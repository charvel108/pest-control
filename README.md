# Checklists

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 10.1.1.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Project structure
In application, we have 3 main layers inside app folder:

- <span>CORE</span>
- FEATURES
- SHARED

<p>Every layer can consist different angular entities. For structuralize the basic parts, try to create a separated folder for every type of entity.</p>
<p>In general, the structure of every layer looks like:</p>

- components/
- containers/
- directives/
- models/
- pipes/
- services/
- *layer.module.ts
- *layer-routing.module.ts

<p>But folders are optional, and depends on the existing of entities inside layer.</p>


## Project architecture
Description of basic project layers

#### Core
<p>Core it's a layer for main server logic, like authentication, server logic(if these methods we use in different modules).</p> 
<p>In core layer, in general we have only module and services, without components. In application we have 2 services:</p>
<ul>
    <li>auth.service.ts - includes logic for user and authentication</li>
    <li>mail.service.ts - includes logic for sending email notifications, uses cloud function</li>
</ul>

#### Features
<p>Features it's layers with separated logic. Every feature it's lazy loaded module, with local scope</p>
<p>We have 2 main modules. AUTH and DASHBOARD.</p>
<ul>
    <li>AUTH - components and routing for auth</li>
    <li>DASHBOARD - main app module, consist basic entities for application, dashboard, forms, services components, etc</li>
</ul>

## Shared
This module includes shared entities, which can be used in different modules.

## Main Flow
<p>Main mission of application, it's creating a different requests, changing the statuses, approve/decline request, creating critical issue from from request and sending email notifications.</p>

**form-config.service.ts** - includes an array of config in *formsConfig* variable, and data for selects

Every field inside form consist from:
<ul>
<li>type (type of dynamic field: input, button, select, date, radiobutton, checkbox)</li>
<li>label (label which should be showed in form field)</li>
<li>name (name = name in database json)</li>
<li>options (field for select and radiobuttons)</li>
<li>validations (array of objects, objects includes: validator name, validator type, validator message)</li>
<li>value(default value of input)</li>
</ul>
All field props are optional, only type is required.

**form-container.ts** - includes stepper, and logic for processing form from every step.

<p>1. General details step it's step with static form fields </p>  
<p>2. Dynamic form uses <b>dynamicField</b> directive which creates dynamic components from json file.</p>
<p>3. Actions list form creating from second step. Only 'No' and 'Not Safe' values from second step form would be passed to the actions list form</p>

After submit an actions list form, all images should be uploaded to firestore, and will retrieve a links for images.
Also, request should be created, and email notification with a link should be sent to approver.

After request has created, approver need to mark all critical actions like duplicate or add action to critical list. After user can approve or decline the request. Also, any user can left the comment for request.
In critical actions list, approver can change status of issue, and add some notes.
