  // // Firebase Config
  // import * as functions from 'firebase-functions';
  // import * as admin from 'firebase-admin';
  // admin.initializeApp(functions.config().firebase);
  // // const db = admin.firestore();
  //
  // // Sendgrid Config
  // import * as sgMail from '@sendgrid/mail';
  //
  // const API_KEY = functions.config().sendgrid.key;
  // const TEMPLATE_ID = functions.config().sendgrid.template;
  // sgMail.setApiKey(API_KEY);
  //
  // // Sends email via HTTP. Can be called from frontend code.
  // export const notificationEmail = functions.https.onCall(async (data, context) => {
  //
  //   if (!context.auth && !context.auth.token.email) {
  //     throw new functions.https.HttpsError('failed-precondition', 'Must be logged with an email address');
  //   }
  //
  //   const msg = {
  //     to: data.recipient,
  //     from: 'noreply@rentokil-initial.com',
  //     templateId: TEMPLATE_ID,
  //     dynamic_template_data: {
  //       firstName: data.firstName,
  //       notificationType: data.notificationType,
  //       link: data.link
  //     }
  //   };
  //
  //   await sgMail.send(msg);
  //
  //   return msg;
  //
  // });
