import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AuthService } from "./services/auth.service";
import { AngularFireModule } from "@angular/fire";
import { environment } from "../../environments/environment";
import { AngularFireAuth, AngularFireAuthModule } from "@angular/fire/auth";
import { AngularFirestore, AngularFirestoreModule } from "@angular/fire/firestore";
import { AngularFireAuthGuardModule } from "@angular/fire/auth-guard";
import { MailService } from "./services/mail.service";
import { AngularFireFunctionsModule } from "@angular/fire/functions";
import { AngularFireStorageModule } from "@angular/fire/storage";
import { AngularFireAnalyticsModule, ScreenTrackingService, UserTrackingService } from "@angular/fire/analytics";

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    AngularFireModule.initializeApp(environment.firebaseConfig),
    AngularFireAuthModule,
    AngularFirestoreModule,
    AngularFireAuthGuardModule,
    AngularFireFunctionsModule,
    AngularFireStorageModule,
    AngularFireAnalyticsModule
  ],
  providers: [
    AuthService,
    AngularFirestore,
    AngularFireAuth,
    MailService,
    ScreenTrackingService,
    UserTrackingService,
  ]
})
export class CoreModule {
}
