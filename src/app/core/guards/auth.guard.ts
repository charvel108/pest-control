import { Injectable, NgZone } from '@angular/core';
import {
  CanActivate,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  UrlTree,
  Router
} from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from "../services/auth.service";
import { AngularFireAuth } from "@angular/fire/auth";
import { map, tap } from "rxjs/operators";

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

  constructor(
      private _authService: AuthService,
      private _ngZone: NgZone,
      private _router: Router,
      private _afAuth: AngularFireAuth
  ) {}

  canActivate(
      route: ActivatedRouteSnapshot,
      state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    return this._afAuth.authState
        .pipe(
            map(user => !!user),
            tap(
                loggedIn => {
                  if (!loggedIn) {
                    if(this.checkLink(state.url)) {
                      this._router.navigate(['auth'],
                          { queryParams: { redirectedLink: state.url } }
                      );
                    } else { this._router.navigate(['auth']) }
                  }
                }
            )
        );
  }

  /**
   * Method for check if router link includes requests and it's not a last path part
   * @param {string} link
   */
  checkLink(link: string) {
    const result = link.split('/');
    const requestsLink = result.indexOf('requests');
    if (requestsLink !== -1) {
      return (result.some(el => el === 'requests')) && (requestsLink !== result.length - 1);
    }
    return false;
  }
}
