import { Injectable, NgZone } from '@angular/core';
import { AngularFireAuth } from "@angular/fire/auth";
import { ActivatedRoute, Router } from "@angular/router";
import { NotificationService } from "../../shared/services/notification.service";
import { BehaviorSubject, Observable } from "rxjs";
import { User } from "../../features/dashboard/models/user.model";
import { DataStorageService } from "../../shared/services/data-storage.service";
import firebase from "firebase/app";
import UserCredential = firebase.auth.UserCredential;
import { map } from "rxjs/operators";
import {CountryName} from "../../shared/enums/shared.enums";
import {ConstantsService} from "../../shared/services/constants.service";


@Injectable()
export class AuthService {

  /**
   * Stream with current logged user
   * @type {BehaviorSubject.<User | null>}
   */
  currentUser$: BehaviorSubject<User> = new BehaviorSubject<User>(null);

  constructor(
      public _notificationService: NotificationService,
      private _afAuth: AngularFireAuth,
      private _router: Router,
      private _dataStorageService: DataStorageService,
      private _activatedRoute: ActivatedRoute,
      private _ngZone: NgZone,
      private _constantsService: ConstantsService
  ) {
    this._notificationService.isLoading = true;
    this._afAuth.authState.subscribe((user) => {
      if (user) {
        this.loadUser(user.uid);
      } else {
        this._notificationService.isLoading = false;
      }
      // if user exists, !!evaluates to true, otherwise it's false
      this._isLoggedIn = !!user;
    }, error => {
      this._notificationService.openCustomMessageSnackbar(error, 'Close');
    });
  }

  private _isLoggedIn: boolean = false;

  /**
   * Getter function for login status
   */
  get isLoggedIn(): boolean {
    return this._isLoggedIn;
  }

  set isLoggedIn(state) {
    this._isLoggedIn = state;
  }

  /**
   * Async handler for Sign In
   */
  async signInGoogle() {
    try {
      this._notificationService.isLoading = true;
      // result => return promise with signIn data
      const userPayload = await this.getSignInPayload();

      // Check if user email domain address matched with DOMAIN_ADDRESS or DOMAIN_ADDRESS_ADDITIONAL
      if (this.isUserEmailValid(userPayload)) {
        if (userPayload.additionalUserInfo.isNewUser || await this.userDocumentIsNotExists(userPayload)) {
          // If user isNew, save user data in firebase
          await this.onNewUser(userPayload);
        } else {
          // If user was existed before, update only provider data, for saving actual info from google account
          const { displayName, email, phoneNumber, photoURL } = userPayload.user.providerData[0];
          await this.updateGoogleSettings(userPayload.user.uid, displayName, email, phoneNumber, photoURL);
        }
        // If we have redirected link in route params, redirect to this link
        if (this._activatedRoute.snapshot.queryParams.redirectedLink) {
          await this._ngZone.run(() => this._router.navigate([this._activatedRoute.snapshot.queryParams.redirectedLink]));
        }
        // Else redirect to the dashboard
        else {
          await this._ngZone.run(() => this._router.navigate(['dashboard']));
        }
        this._notificationService.isLoading = false;
      } else {
        // If user is not from allowed domain area, delete user, and redirect to the auth
        await userPayload.user.delete();
        await this._ngZone.run(() => this._router.navigate(['auth']));
        this._notificationService._snackBar.open(`Please use ${this._constantsService.DOMAIN_ADDRESS} or ${this._constantsService.DOMAIN_ADDRESS_ADDITIONAL} email!`, 'Undo', {
          duration: 3000
        });
        this._notificationService.isLoading = false;
      }
    } catch (error) {
      this._notificationService.openCustomMessageSnackbar(error, 'Close');
      this._notificationService.isLoading = false;
    }
  }

  async userDocumentIsNotExists(userPayload) {
    const isDocumentExists = await this._dataStorageService.isDocumentExists('users', userPayload.user.uid);
    return !isDocumentExists;
  }

  /**
   * Method for signOut to auth page
   */
  async signOut() {
    await this._afAuth.signOut();
    await this.clearUser();
    this.isLoggedIn = false;
    await this._ngZone.run(() => this._router.navigate(['auth']));
  }

  // Get payload if user si
  private async getSignInPayload() {
    return await this._afAuth.signInWithPopup(new firebase.auth.GoogleAuthProvider().setCustomParameters({
      hd: 'rentokil-initial.com', // value for autocomplete user domain
      prompt: 'select_account' // value for open signIn popup every time when user login
    }))
  }

  /**
   *  Method for matching domain area
   * @param {string} email - User email
   * @param {string} domain - Domain area: @google.com for example
   */
  private isDomainMatched(email: string, domain: string) {
    const regex = new RegExp(`^[A-Za-z0-9._%+-]+${domain}\$`);
    return regex.test(email);
  }

  private isUserEmailValid(payload) {
    return this.isDomainMatched(payload.user.email, this._constantsService.DOMAIN_ADDRESS) || this.isDomainMatched(payload.user.email, this._constantsService.DOMAIN_ADDRESS_ADDITIONAL) || payload.user.email === this._constantsService.DEVELOPMENT_EMAIL
  }

  /**
   * Getter for current user
   */
  get currentUser(): User {
    return this.currentUser$.getValue();
  }

  /**
   * Getter for current approver
   */
  get currentApprover(): string {
    return this.currentUser$.getValue()?.preferences?.requestPrefs?.approver;
  }

  /**
   * Getter for current approver
   */
  get currentCountry(): CountryName {
    return this.currentUser$.getValue()?.preferences?.requestPrefs?.countryName;
  }

  /**
   * Getter for user vehicle settings
   */
  get userVehicleDefaults() {
    return this.currentUser$.getValue()?.preferences.vehiclePrefs;
  }

  /**
   * Method for emit current user to stream
   * @param user - User extends interface GoogleData
   */
  setCurrentUser(user: User) {
    this.currentUser$.next(user);
  }

  /**
   * Method for update user settings from provider
   * @param {string} uid
   * @param {string} displayName
   * @param {string} email
   * @param {string} phoneNumber
   * @param {string} photoURL
   */
  async updateGoogleSettings(uid: string, displayName: string, email: string, phoneNumber: string, photoURL: string) {
    await this._dataStorageService.updateGoogleData(uid, displayName, email, phoneNumber, photoURL)
  }

  /**
   * Method for setting new user to database
   * @param { import UserCredential = firebase.auth.UserCredential } userCred
   */
  async onNewUser(userCred: UserCredential) {
    const { displayName, photoURL, email, phoneNumber } = userCred.user.providerData[0];
    const newUser: User = this.newUserGenerator(userCred.user.uid, displayName, email, phoneNumber, photoURL);
    await this._dataStorageService.addUser(newUser);
  }

  /**
   * Method for changing user preferences in database
   * @param {any} updatedPreferences
   */
  async updateUserPreferences(updatedPreferences: any) {
    try {
      await this._dataStorageService.updateUser({ ...this.currentUser, preferences: updatedPreferences });
      this._notificationService.openCustomMessageSnackbar('Vehicle preferences saved.', 'Okay');
    } catch (error) {
      this._notificationService.openCustomMessageSnackbar(error, 'Okay');
    }
  }

  /**
   * Method for getting user from stream
   * @param {string} uid
   */
  loadUser(uid: string) {
    if (!this.currentUser) {
      this._dataStorageService.getUser(uid)
          .subscribe((user) => {
            this.setCurrentUser(user);
            this._notificationService.isLoading = false;
          })
    }
  }

  /**
   * Method for set user to null
   */
  async clearUser() {
    this.setCurrentUser(null);
  }

  /**
   * Get user emails list
   */
  getUsersEmailsList(): Observable<string[]> {
    return this._dataStorageService.getUsersList().pipe(
        map(usersList => usersList.map(user => {
          const { email } = user.payload.doc.data() as User;
          return email;
        }))
    );
  }

  /**
   * Method for async validation, check if user exists in list
   * @param formEmail
   */
  isEmailExistsInList(formEmail: string): Observable<boolean> {
    return this._dataStorageService.getUsersList().pipe(
        map(usersList =>
            usersList
            .map(user => {
              const { email } = user.payload.doc.data() as User;
              return email;
            })
            .some(email=> email===formEmail)
        )
    );
  }

  /**
   * We create new user with info which we get from firestore
   * @param uid {string}
   * @param displayName {string}
   * @param email {string}
   * @param phoneNumber {string}
   * @param photoURL {string}
   * @private
   */
  private newUserGenerator(uid: string, displayName: string, email: string, phoneNumber: string, photoURL: string): User {
    return {
      uid,
      displayName,
      email,
      phoneNumber,
      photoURL,
      preferences: this.userPreferencesGenerator(),
    };
  }

  // Just return new object with default user preferences
  private userPreferencesGenerator() {
    return {
      requestPrefs: {
        approver: null,
        countryName: null
      },
      vehiclePrefs: {
        registration: null,
        makeAndModel: null,
        type: null,
        licenceExpiry: null,
        licenceNumber: null,
      }
    }
  }
}

