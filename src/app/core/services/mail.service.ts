import { Injectable } from '@angular/core';
import { AngularFireFunctions } from "@angular/fire/functions";
import { catchError } from "rxjs/operators";
import { throwError } from "rxjs";

@Injectable()
export class MailService {

  constructor(
      private readonly _fireFunctions: AngularFireFunctions
  ) { }

  /**
   * Method where we trigger cloud function with name "notificationEmail" for sending email
   * @param {string[]} recipient
   * @param {string} firstName
   * @param {string} notificationType
   * @param {string} link
   */
  sendEmail(
      recipient: string[],
      firstName: string,
      notificationType: string,
      link: string
  ) {
    const callable = this._fireFunctions.httpsCallable('notificationEmail');
    callable(
        {
          recipient,
          firstName,
          notificationType,
          link
        }
    ).pipe(catchError(this.handleError)).subscribe(data => console.log(data))
  }

  /**
   * Method for error handling
   * @param error
   */
  private handleError(error) {
    return throwError(error);
  }

}
