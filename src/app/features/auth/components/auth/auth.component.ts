import { Component } from '@angular/core';
import { AuthService } from "../../../../core/services/auth.service";

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.scss']
})
export class AuthComponent {

  /** @type {boolean} */
  isLoading: boolean = false;

  constructor(private _authService: AuthService) {}

  /**
   * Trigger signIn method from auth service
   */
  async onLogin() {
    await this._authService.signInGoogle();
  }
}
