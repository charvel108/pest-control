import {AfterViewInit, ChangeDetectionStrategy, Component, ElementRef, EventEmitter, Input, OnDestroy, Output, ViewChild} from '@angular/core';
import {Comment} from "../../models/comment.model";
import {AuthService} from "../../../../core/services/auth.service";
import {MatDialog, MatDialogConfig} from "@angular/material/dialog";
import {SubmitModalComponent} from "../../../../shared/components/submit-modal/submit-modal.component";
import {Subject} from "rxjs";
import {filter, takeUntil} from "rxjs/operators";
import firebase from "firebase/app";
import Timestamp = firebase.firestore.Timestamp;


@Component({
  selector: 'app-comments',
  templateUrl: './comments.component.html',
  styleUrls: ['./comments.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})

export class CommentsComponent implements AfterViewInit, OnDestroy {

  /** @type {Comment[]} */
  @Input() comments: Comment[];

  /** @type {EventEmitter<Comment>} */
  @Output() newComment = new EventEmitter<Comment>();

  /** @type {ElementRef} */
  @ViewChild('commentBox', { static: true }) commentBox: ElementRef;

  /** @type {ElementRef} */
  @ViewChild('newCommentInput', { static: true }) newCommentInput: ElementRef;

  /** @type {Subject<boolean>} */
  private readonly _destroy$: Subject<boolean> = new Subject<boolean>();

  constructor(
      private readonly _authService: AuthService,
      private _dialog: MatDialog
  ) {
  }

  ngAfterViewInit() {
    this.scrollToBottom();
  }

  ngOnDestroy() {
    this._destroy$.next(true);
    this._destroy$.unsubscribe();
  }

  /**
   * Scroll to bottom after new comment was added
   */
  scrollToBottom() {
    let commentInputEle = this.commentBox.nativeElement.scrollTop;
    if (commentInputEle > 0) {
      commentInputEle.scrollTop = commentInputEle.scrollHeight;
    }
  }

  /**
   * Open submit dialog with some config
   */
  openDialog() {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.autoFocus = true;
    dialogConfig.disableClose = false;
    const dialogRef = this._dialog.open(SubmitModalComponent, dialogConfig);

    dialogRef.afterClosed()
      .pipe(
        takeUntil(this._destroy$),
        filter(value=> value) // Filter stream value for only true
      )
      .subscribe(
      val => {
        this.onNewComment();
      }
    );

  }

  /**
   * Create new comment and emit value to the parent
   */
  onNewComment() {
    this.newComment.emit({
      owner: this._authService.currentUser.email,
      comment: this.newCommentInput.nativeElement.value,
      date: Timestamp.now(),
    });
    this.newCommentInput.nativeElement.value = null;
    // Comment takes ~200-250ms to refresh, scroll set to 350 as a precaution
    setTimeout(() => this.scrollToBottom(), 350);
  }
}
