import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {CriticalActionDto, CriticalActionStatus} from "../../models/action.interface";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {AuthService} from "../../../../core/services/auth.service";
import {FormConfigService} from "../../services/form-config.service";
import {Comment} from "../../models/comment.model";
import {MatDialog, MatDialogConfig} from "@angular/material/dialog";
import {SubmitModalComponent} from "../../../../shared/components/submit-modal/submit-modal.component";
import {filter, takeUntil} from "rxjs/operators";
import {Subject} from "rxjs";
import {MailService} from "../../../../core/services/mail.service";
import {environment} from "../../../../../environments/environment.prod";
import firebase from "firebase/app";
import Timestamp = firebase.firestore.Timestamp;
import {ConstantsService} from "../../../../shared/services/constants.service";
import {ModalChangeApproverComponent} from "../../../../shared/components/modal-change-approver/modal-change-approver.component";
import {DataStorageService} from "../../../../shared/services/data-storage.service";
import {NotificationService} from "../../../../shared/services/notification.service";

@Component({
    selector: 'app-critical-actions-expandable',
    templateUrl: './critical-actions-expandable.component.html',
    styleUrls: ['./critical-actions-expandable.component.scss']
})
export class CriticalActionsExpandableComponent implements OnInit, OnDestroy {

    /** @type {FormGroup} */
    criticalActionsForm: FormGroup;

    /** @type {CriticalActionStatus[]} */
    criticalActionsStatuses: CriticalActionStatus[];

    /** @type {number} */
    currentDateTime: number = new Date().getTime();

    /** @type {boolean} */
    disabledState: boolean = true;

    /** @type {string} */
    readonly defaultImage: string = 'assets/image-placeholder.png';

    /** @type {Subject<boolean>} */
    private readonly _destroy$: Subject<boolean> = new Subject<boolean>();

    /** @type {CriticalActionDto} */
    @Input() actionData: CriticalActionDto;

    constructor(
        private readonly _fb: FormBuilder,
        private readonly _authService: AuthService,
        private readonly _formConfigService: FormConfigService,
        private readonly _dialog: MatDialog,
        private readonly _mailService: MailService,
        private readonly _constantsService: ConstantsService,
        private readonly _dataStorageService: DataStorageService,
        private readonly _notificationService: NotificationService,
    ) {
    }

    ngOnInit(): void {
        // Set default values to critical actions form
        this.criticalActionsForm = this._fb.group({
            criticalActionComment: ['', []],
            criticalActionStatus: [this.actionData.status, [Validators.required]],
        });

        // Set the array of statuses
        this.criticalActionsStatuses = this._formConfigService.criticalActionsStatuses;

    }

    ngOnDestroy() {
        this._destroy$.next(true);
        this._destroy$.unsubscribe();
    }

    /**
     * Method for calculation time, since critical item was created
     * @param {Timestamp} createdOnDate
     */
    calculateDaysDuration(createdOnDate: Timestamp) {
        const daysCount = Math.round((this.currentDateTime - createdOnDate.toDate().getTime()) / (1000 * 3600 * 24));
        return daysCount === 1 ? `${daysCount} day` : `${daysCount} days`;
    }

    /**
     * Return true if it's your request
     * @param {string} approver
     */
    isYourRequest(approver: string): boolean {
        return this._authService?.currentUser?.email === approver;
    }

    /**
     * Return true if you are owner
     * @param {string} owner
     */
    isOwnerItem(owner: string): boolean {
        return this._authService?.currentUser?.email === owner;
    }

    /**
     * If critical action field-select was changed
     */
    isSelectedChanged() {
        this.disabledState = false;
    }

    /**
     * Getter for changing disable state
     */
    get isEnabled() {
        return !!(this.criticalActionsForm.get('criticalActionComment').value) || !this.disabledState
    }

    async onSubmit() {
        if (this.criticalActionsForm.invalid) {
            return;
        } else {
            await this.handleSubmitModal();
        }
    }

    /**
     * Method for submit action form
     * 1. Update status for critical item
     * 2. Send email to owner
     */
    async submitCriticalActionForm() {
        const notes = this.criticalActionsForm.value?.criticalActionComment;
        const status = this.criticalActionsForm.value?.criticalActionStatus;
        const commentMessage: string = notes ?
            `Critical action "${this.actionData.id}" has changed to "${status}" status with message: "${notes}"` :
            `Critical action "${this.actionData.id}" has changed to "${status}" status`

        const comment: Comment = {
            owner: 'System',
            date: Timestamp.now(),
            comment: `Request was changed by ${this.actionData.approver}. ${commentMessage}`
        };

        try {
            await this._dataStorageService.updateCriticalActionStatus(this.actionData.id, status);
            await this._dataStorageService.addNewComment(this.actionData.requestId, comment);
            this.sendNotificationEmail(
                [this.actionData.owner],
                this.actionData.owner,
                commentMessage,
                `${environment.additionalFields.mainDomain}/dashboard/critical-list/${this.actionData.id}`
            );
            this._notificationService.openCustomMessageSnackbar('Critical Action was successfully updated', 'Close');
        } catch (e) {
            this._notificationService.openCustomMessageSnackbar('Something went wrong', 'Close');
        }
    }

    /**
     * Handle submit modal with some settings
     */
    async handleSubmitModal() {
        const dialogConfig = new MatDialogConfig();
        dialogConfig.autoFocus = true;
        dialogConfig.disableClose = false;
        const dialogRef = this._dialog.open(SubmitModalComponent, dialogConfig);

        dialogRef.afterClosed()
            .pipe(
                takeUntil(this._destroy$),
                filter(value => value)
            )
            .subscribe(() => this.submitCriticalActionForm());
    }

    /**
     * Method for change approver modal
     */
    handleChangeApproverModal() {
        const dialogConfig = new MatDialogConfig();
        dialogConfig.autoFocus = true;
        dialogConfig.disableClose = false;
        dialogConfig.data = {approver: this.actionData?.approver};
        dialogConfig.panelClass = 'modal--approver';
        const dialogRef = this._dialog.open(ModalChangeApproverComponent, dialogConfig);

        dialogRef.afterClosed()
            .pipe(
                takeUntil(this._destroy$),
                filter(value => value)
            )
            .subscribe(
                async newApprover => {
                    const comment: Comment = {
                        owner: 'System',
                        comment: `${this.actionData?.owner} has changed approver in critical item "${this.removeNoteMessage(this.actionData?.notes)}" to ${newApprover}`,
                        date: Timestamp.now(),
                    };

                    try {
                        await this._dataStorageService.updateCriticalActionApprover(this.actionData.id, newApprover);
                        await this._dataStorageService.addNewComment(this.actionData.requestId, comment);
                        this.sendNotificationEmail(
                            [newApprover],
                            newApprover,
                            `The ${this.actionData?.owner} has assigned critical action to you!`,
                            `${environment.additionalFields.mainDomain}/dashboard/critical-list/${this.actionData.id}`
                        );
                        this._notificationService.openCustomMessageSnackbar('Approver was successfully updated', 'Close');
                    } catch (e) {
                        this._notificationService.openCustomMessageSnackbar('Something went wrong', 'Close');
                    }
                }
            );
    }

    /**
     * Trigger method for send email notifications from mail service
     * @param recipients {string[]}
     * @param firstName {string}
     * @param notificationType {string}
     * @param link {string}
     */
     sendNotificationEmail(recipients: string[], firstName: string, notificationType: string, link: string) {
        this._mailService.sendEmail(recipients, firstName, notificationType, link)
    }

    /**
     * Regexp for removing note message for
     * @param note
     */
    removeNoteMessage(note: string) {
        return note.replace(/:.*$/gm, "");
    }

}
