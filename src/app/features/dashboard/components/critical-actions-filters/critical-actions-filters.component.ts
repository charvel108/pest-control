import { Component, EventEmitter, Input, OnDestroy, OnInit, Output, ViewChild } from '@angular/core';
import {BranchGroup} from "../../models/form.interface";
import { FormBuilder, FormGroup } from "@angular/forms";
import { Subject } from "rxjs";
import { FormConfigService } from "../../services/form-config.service";
import { CriticalActionStatus } from "../../models/action.interface";
import { MatSort } from "@angular/material/sort";
import { MatPaginator } from "@angular/material/paginator";
import {
  AUTO_STYLE,
  animate,
  state,
  style,
  transition,
  trigger
} from '@angular/animations';
import {BranchName} from "../../../../shared/enums/shared.enums";


@Component({
  selector: 'app-critical-actions-filters',
  templateUrl: './critical-actions-filters.component.html',
  styleUrls: ['./critical-actions-filters.component.scss'],
  animations: [
    trigger('collapse', [
      state('false', style({ height: AUTO_STYLE, visibility: AUTO_STYLE })),
      state('true', style({ height: '0', visibility: 'hidden' })),
      transition('false => true', animate( '300ms ease-in')),
      transition('true => false', animate('300ms ease-out'))
    ])
  ]
})
export class CriticalActionsFiltersComponent implements OnInit, OnDestroy {

  /** @type {CriticalActionStatus[]} */
  statuses: CriticalActionStatus[];

  /** @type {BranchName[]} */
  branches: BranchName[];

  /**@type {string[]} */
  types: string[];

  /**@type {FormGroup} */
  filtersForm: FormGroup;

  /**@type {boolean} */
  collapsed: boolean = false;

  /**@type {BranchGroup[]} */
  branchGroups: BranchGroup[];

  maxDate: Date;

  /**@type {Subject<boolean>} */
  private readonly destroy$: Subject<boolean> = new Subject<boolean>();

  /**@type {EventEmitter<any>} */
  @Output() newFilters = new EventEmitter();

  /**@type {boolean} */
  @Input() hideAdditionalFilters: boolean;

  /**@type {MatSort} */
  @ViewChild(MatSort) sort: MatSort;

  /**@type {MatPaginator} */
  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;

  constructor(
    private _fb: FormBuilder,
    private _formConfigService: FormConfigService,
  ) {
    this.maxDate = new Date();
  }

  ngOnInit() {
    // Set default values for filters form
    this.filtersForm = this._fb.group({
      status: [[], []],
      branch: [[], []],
      approver: ['', []],
      type: ['', []],
      fromDateValue: ['', []],
      toDateValue: ['', []],
      assignedToMe: ['', []],
      registration: "",
    });

    this.statuses = this._formConfigService.criticalActionsStatuses;

    this.types = this._formConfigService.formTypes;

    this.branches = this._formConfigService.branches;

    this.branchGroups = this._formConfigService.branchGroups;

  }

  ngOnDestroy() {
    this.destroy$.next(true);
    this.destroy$.unsubscribe();
  }

  /**
   * Filters emitter
   * @param {any} filter
   */
  filtersEmitter(filter: any) {
    this.newFilters.emit(filter);
  }

  /**
   * Toggle expand panel
   */
  toggle() {
    this.collapsed = !this.collapsed;
  }

  /**
   * Reset filters to default state
   */
  resetFilters() {
    this.newFilters.emit({
      approver: "",
      fromDateValue: "",
      status: [],
      type: "",
      branch: [],
      toDateValue: "",
      assignedToMe: "",
      registration: "",
    })
    this.filtersForm.reset();
  }

  emitFilterValues(filtersFormValue) {
    const filter = {
      ...filtersFormValue,
      approver: filtersFormValue.approver?.trim().toLowerCase() || "",
      fromDateValue: filtersFormValue.fromDateValue ? new Date(filtersFormValue.fromDateValue)?.getTime() : "",
      toDateValue: this.filtersForm.value.toDateValue ? new Date(this.filtersForm.value.toDateValue)?.setHours(23, 59, 59) : "",
      registration: filtersFormValue.registration?.trim().toLowerCase() || "",
      type: filtersFormValue.type || "",
    } as string;
    this.filtersEmitter(filter);
    return filter;
  }

  onApplyFilters() {
    if (this.filtersForm.invalid) {
      return;
    }
    this.emitFilterValues(this.filtersForm.value);
  }

}
