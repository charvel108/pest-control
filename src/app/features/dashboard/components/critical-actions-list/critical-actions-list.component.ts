import { Component, Input, NgZone, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { animate, state, style, transition, trigger } from '@angular/animations';
import { RequestsService } from "../../services/requests.service";
import { MatTableDataSource } from "@angular/material/table";
import {publishReplay, refCount, takeUntil} from "rxjs/operators";
import {Observable, Subject} from "rxjs";
import { MatSort } from "@angular/material/sort";
import { MatPaginator } from "@angular/material/paginator";
import { AuthService } from "../../../../core/services/auth.service";
import { ActivatedRoute, Router } from "@angular/router";
import {CriticalActionDto} from "../../models/action.interface";

@Component({
  selector: 'app-critical-actions-list',
  templateUrl: './critical-actions-list.component.html',
  styleUrls: ['./critical-actions-list.component.scss'],
  animations: [
    trigger('detailExpand', [
      state('collapsed, void', style({ height: '0px' })),
      state('expanded', style({ height: '*' })),
      transition('expanded <=> collapsed',
        animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
      transition('expanded <=> void',
        animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)'))])
  ],
})
export class CriticalActionsListComponent implements OnInit, OnDestroy {

  /**@type {MatTableDataSource<any>} */
  dataSource: MatTableDataSource<any>;

  /**@type {any} */
  expandedElement: any;

  /**@type {boolean} */
  isLoading: boolean = false;

  /**@type {string[]} */
  readonly displayedColumns: string[] = ['createdOn', 'approver', 'branchName', 'status', 'expand'];

  /**@type {Subject<boolean>()} */
  private readonly _destroy$: Subject<boolean> = new Subject<boolean>();

  /**@type {Observable<Array<RequestTableDto} */
  private _cache$: Observable<Array<CriticalActionDto>>;

  /**@type {boolean} */
  @Input() showFilters: boolean = false;

  /**@type {MatSort} */
  @ViewChild(MatSort) sort: MatSort;

  /**@type {MatPaginator} */
  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;

  constructor(
    private _requestsService: RequestsService,
    private _authService: AuthService,
    private _route: ActivatedRoute,
    private readonly _ngZone: NgZone,
    private readonly _router: Router,
  ) {}

  ngOnInit(): void {
    this.isLoading = true;

    /**
     * Get critical list from database
     */
    this.backendCriticalList()
      .subscribe(criticalList => {
        this.dataSource = new MatTableDataSource(criticalList);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
        this.isLoading = false;
        // Create custom filter predicate with some rules data = table row data, filter = filters values
        this.dataSource.filterPredicate = ((data, filter) => {
          const a = !filter.status?.length || (filter.status?.includes(data.status)); // filter by Status
          const b = !filter.branch?.length || (filter.branch?.includes(data.branchName)); // filter by Branch
          const c = !filter.approver || data.approver?.toLowerCase().includes(filter.approver); // filter by Email
          const d = (!filter.fromDateValue || !filter.toDateValue) || ((data.createdOn?.toDate().getTime()) >= filter.fromDateValue) && (data.createdOn?.toDate().getTime() <= filter.toDateValue + 1); // Filter by from and to Date
          const e = !filter.assignedToMe || data.approver === this._authService?.currentUser.email; // filter by approver === my email
          const f = !filter.registration || data.registration?.toLowerCase().includes(filter.registration); // Filter by vehicle registration
          const g = !filter.type || data.formType === filter.type; // filter by Type
          return a && b && c && d && e && f && g;
        }) as (RequestTableDto, string) => boolean;

        /**
         * Get route id, if id consist expand table element with id
         */
        this._route.params
          .pipe(
            takeUntil(this._destroy$),
          ).subscribe(
          async params => {
            if (params.id) {
              const isIdConsist = this.dataSource.data.find(t => t.id == params.id);
              if (isIdConsist) {
                const elemIndex = this.dataSource.sortData(this.dataSource.filteredData, this.dataSource.sort).findIndex(t => t.id == params.id) // Index of element in sort data
                this.paginator.pageIndex = Math.ceil((elemIndex + 1) / this.paginator.pageSize) - 1; // Find number of page with element
                this.expandedElement = isIdConsist;
                this.sort.sort({ id: null, start: 'desc', disableClear: false });
                this.sort._stateChanges.next();
              } else {
                await this._ngZone.run(() => this._router.navigate(['dashboard/critical-list']));
              }
            }
          }
        )
      });
    this.dataSource = new MatTableDataSource([]);


  }

  ngOnDestroy() {
    this._destroy$.next();
    this._destroy$.complete();
  }

  onNewFilters(filters) {
    this.expandedElement = null;
    this.dataSource.filter = filters;
  }

  get criticalList() {
    if (!this._cache$) {
      this._cache$ = this.backendCriticalList().pipe(
          publishReplay(1),
          refCount()
      );
    }

    return this._cache$;
  }

  private backendCriticalList() {
    return this._requestsService.getCriticalList();
  }

}
