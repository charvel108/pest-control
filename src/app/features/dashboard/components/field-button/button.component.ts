import { Component, OnInit } from '@angular/core';
import { FieldConfig } from "../../models/field.interface";
import { FormGroup } from "@angular/forms";

@Component({
  selector: 'app-button',
  template: `
    <div class="" [formGroup]="group">
      <button type="submit" mat-raised-button color="primary">{{field.label}}</button>
    </div>
  `,
  styles: [
    ':host { display:block; margin-top: 15px; }'
  ]
})
export class ButtonComponent implements OnInit {

  field: FieldConfig;
  group: FormGroup;

  constructor() {
  }

  ngOnInit(): void {
  }

}
