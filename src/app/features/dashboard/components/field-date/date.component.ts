import { Component, OnInit } from '@angular/core';
import { FieldConfig } from "../../models/field.interface";
import { FormGroup } from "@angular/forms";

@Component({
  selector: 'app-date',
  template: `
    <mat-form-field class="fullwidth" appearance="outline" [formGroup]="group" >
      <mat-label>{{field.label}}</mat-label>
      <input matInput [matDatepicker]="picker" [formControlName]="field.name">
      <mat-datepicker-toggle matSuffix [for]="picker"></mat-datepicker-toggle>
      <mat-datepicker #picker></mat-datepicker>
      <mat-hint></mat-hint>
      <ng-container *ngFor="let validation of field.validations;" ngProjectAs="mat-error">
        <mat-error *ngIf="group.get(field.name).hasError(validation.name)">{{validation.message}}</mat-error>
      </ng-container>
    </mat-form-field>
  `,
  styles: [
    ':host { display:block; }'
  ]
})
export class DateComponent implements OnInit {

  field: FieldConfig;
  group: FormGroup;

  constructor() {
  }

  ngOnInit(): void {
  }

}
