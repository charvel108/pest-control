import { Component, OnInit } from '@angular/core';
import { FieldConfig } from "../../models/field.interface";
import { FormGroup } from "@angular/forms";

@Component({
  selector: 'app-input',
  template: `
    <mat-form-field class="fullwidth" [formGroup]="group" appearance="outline"> 
      <mat-label>{{field.label}}:</mat-label>
      <input matInput [formControlName]="field.name" [type]="field.inputType">
      <ng-container *ngFor="let validation of field.validations;" ngProjectAs="mat-error">
        <mat-error *ngIf="group.get(field.name).hasError(validation.name)">{{validation.message}}</mat-error>
      </ng-container>
    </mat-form-field>
  `,
  styles: [
      ':host { display:block; }'
  ]
})
export class InputComponent implements OnInit {

  field: FieldConfig;
  category: string;
  group: FormGroup;

  constructor() {
  }

  ngOnInit(): void {
  }

}
