import { Component, OnInit } from '@angular/core';
import { FieldConfig } from "../../models/field.interface";
import { FormGroup } from "@angular/forms";

@Component({
  selector: 'app-radio-button',
  template: `
    <div class="radio-button__wrapper" [formGroup]="group">
      <mat-label>{{field.label}}:</mat-label>
      <mat-radio-group [formControlName]="field.name" [attr.data-category]="category" [attr.data-label]="field.label">
        <ng-container *ngFor="let validation of field.validations;" ngProjectAs="mat-error">
          <mat-error *ngIf="group.get(field.name).hasError(validation.name)">{{validation.message}}</mat-error>
        </ng-container>
        <mat-radio-button *ngFor="let item of field.options" [value]="item">{{item}}</mat-radio-button>
      </mat-radio-group>
    </div>
  `,
  styleUrls: ['./radio-button.component.scss']
})
export class RadioButtonComponent implements OnInit {

  field: FieldConfig;
  category: string;
  group: FormGroup;

  constructor() {
  }

  ngOnInit(): void {
  }

}
