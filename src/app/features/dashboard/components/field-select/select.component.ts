import { Component, OnInit } from '@angular/core';
import { FieldConfig } from "../../models/field.interface";
import { FormGroup } from "@angular/forms";

@Component({
  selector: 'app-select',
  template: `
    <mat-form-field appearance="outline" class="fullwidth" [formGroup]="group">
      <mat-label>{{field.label}}</mat-label>
      <mat-select [placeholder]="field.label" [formControlName]="field.name">
        <mat-option *ngFor="let item of field.options" [value]="item">{{item}}</mat-option>
      </mat-select>
      <ng-container *ngFor="let validation of field.validations;" ngProjectAs="mat-error">
        <mat-error *ngIf="group.get(field.name).hasError(validation.name)">{{validation.message}}</mat-error>
      </ng-container>
    </mat-form-field>
  `,
  styles: [
    ':host { display:block; }'
  ]
})
export class SelectComponent implements OnInit {

  field: FieldConfig;
  group: FormGroup;

  constructor() {
  }

  ngOnInit(): void {
  }

}
