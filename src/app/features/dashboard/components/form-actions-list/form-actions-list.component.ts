import { Component, EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { Observable, Subject } from "rxjs";
import { ActionDto, ActionRequestDto, ActionStatus } from "../../models/action.interface";
import { FormBuilder, FormControl, FormGroup, Validators } from "@angular/forms";
import { filter, takeUntil } from "rxjs/operators";
import { AngularFireStorage } from "@angular/fire/storage";
import { NotificationService } from "../../../../shared/services/notification.service";
import { MatDialog, MatDialogConfig } from "@angular/material/dialog";
import { SubmitModalComponent } from "../../../../shared/components/submit-modal/submit-modal.component";

@Component({
  selector: 'app-form-actions-list',
  templateUrl: './form-actions-list.component.html',
  styleUrls: ['./form-actions-list.component.scss']
})
export class FormActionsListComponent implements OnInit, OnDestroy {

  /**@type {FormGroup} */
  actionsForm: FormGroup;

  /**@type {ActionDto[]} */
  actionsList: ActionDto[];

  /**@type {boolean} */
  isLoading: boolean = false;

  /**@type {boolean} */
  isButtonDisabled: boolean;

  /**@type {Subject<boolean>} */
  private readonly _destroy$: Subject<boolean> = new Subject<boolean>();

  /**@type {ActionDto[]} */
  @Input() actionsList$: Observable<ActionDto[]>;

  /**@type {EventEmitter<any>()} */
  @Output() goToMainFieldsStep: EventEmitter<any> = new EventEmitter<any>();

  /**@type {EventEmitter<any>()} */
  @Output() actionsListEmitter: EventEmitter<any> = new EventEmitter<any>();


  constructor(
      private readonly _fb: FormBuilder,
      private readonly _fireStorage: AngularFireStorage,
      private readonly _notificationService: NotificationService,
      private readonly _dialog: MatDialog
  ) {
  }

  ngOnInit(): void {
    this.isButtonDisabled = false;

    // Get actions list from observable and bind form groups and form fields in model
    this.actionsList$
        .pipe(takeUntil(this._destroy$))
        .subscribe(actionsList => {
              this.actionsList = actionsList;
              this.isActionsListEmpty(this.actionsList);
              let group = {};
              actionsList?.forEach(input => {
                group[input.name] = new FormGroup({
                  notes: new FormControl('', [Validators.required]),
                  image: new FormControl('', [])
                })
              });
              this.actionsForm = new FormGroup(group);
            }
        )
  }

  ngOnDestroy() {
    this._destroy$.next();
    this._destroy$.complete();
  }

  /**
   * Prepare actions form values for emitting to parent
   * @param actionsFormValues
   */
  prepareActionsForm(actionsFormValues) {
    return this.actionsList.map(action => {
      return {
        ...actionsFormValues[action.name],
        ...action
      }
    })
  }

  /**
   * Trigger prev step
   */
  emitPrev() {
    this.goToMainFieldsStep.emit();
  }

  /**
   * Check if is actions list not consist
   * @param actionsList {ActionDto[]}
   */
  isActionsListEmpty(actionsList: ActionDto[]) {
    return Array.isArray(actionsList) && actionsList.length;
  }

  /**
   * Method for upload images to firestore. Return new item with returned link
   * @param formData {formData: Array<any>}
   */
  async upLoadImages(formData: Array<any>) {
    let result: ActionRequestDto[] = [];
    for await (const item of formData) {
      const { image, ...itemData } = item;
      const id = `${new Date().getTime()}${Math.round((Math.random() * 36 ** 12)).toString(36)}`;
      if (image) {

        // The storage path
        const path = `issues/${Date.now()}_${item.image?._fileNames}`;

        // The main task
        const task: ActionRequestDto = await this._fireStorage.upload(path, item.image._files[0]).then(uploadSnapshot =>
            uploadSnapshot.ref.getDownloadURL()
                .then((downloadUrl) => ( {
                  actionStatus: ActionStatus.New,
                  imageUrl: downloadUrl,
                  id,
                  criticalListId : null,
                  ...itemData } ))
        );

        result.push(task);

      }

     else {
       // Else, we don't have image, we push item
        result.push( {
          actionStatus: ActionStatus.New,
          imageUrl: null,
          id,
          criticalListId : null,
          ...itemData} );
      }
    }
    return result;
  }

  /**
   * Handle modal dialog, and if submit prepare data and upload images
   */
  async handleSubmitModal() {
    if (this.actionsForm.invalid) {
      return;
    } else {
      const dialogConfig = new MatDialogConfig();
      dialogConfig.autoFocus = true;
      dialogConfig.disableClose = false;
      const dialogRef = this._dialog.open(SubmitModalComponent, dialogConfig);
      dialogRef.afterClosed()
          .pipe(
              takeUntil(this._destroy$),
              filter(value => value)
          )
          .subscribe(
              async val => {
                this.isButtonDisabled = true;
                this._notificationService.isLoadingLocal = true;
                const formData = this.prepareActionsForm(this.actionsForm.value);
                this.actionsListEmitter.emit(await this.upLoadImages(formData));
                this._notificationService.isLoadingLocal = false;
              }
          );
    }
  }
}
