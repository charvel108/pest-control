import {AfterViewInit, Component, NgZone, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute, Params, Router} from "@angular/router";
import {FormConfigService} from "../../services/form-config.service";
import {combineLatest, Observable, of, Subject} from "rxjs";
import {map, switchMap, takeUntil} from "rxjs/operators";
import {MatStepper} from "@angular/material/stepper";
import {FieldGroup, formDynamic} from "../../models/form.interface";
import {RequestDto, RequestTableDto} from '../../models/request.model';
import {AuthService} from "../../../../core/services/auth.service";
import {MailService} from "../../../../core/services/mail.service";
import {RequestsService} from "../../services/requests.service";
import {FormStoreService} from "../../services/form-store.service";
import {ActionDto, ActionRequestDto} from "../../models/action.interface";
import {NotificationService} from "../../../../shared/services/notification.service";
import {environment} from "../../../../../environments/environment.prod";
import firebase from "firebase/app";
import {
  CountryName,
  FormAnswerClassic,
  FormAnswerSecondary,
  FormClass,
  RequestStatus
} from "../../../../shared/enums/shared.enums";
import {GeneralDetailsNewDto} from "../../models/general-details-new.model";
import {FormDynamicComponent} from "../form-dynamic/form-dynamic.component";
import {DynamicFieldType} from "../../models/field.interface";
import Timestamp = firebase.firestore.Timestamp;
import {VEHICLE_DEFAULTS} from "../../../../shared/dto/constants";

@Component({
  selector: 'app-form-container',
  templateUrl: './form-container.component.html',
  styleUrls: ['./form-container.component.scss']
})
export class FormContainerComponent implements OnInit, OnDestroy, AfterViewInit {

  /**@type {Observable<formDynamic>} */
  currentForm$: Observable<formDynamic>;

  /**@type {Observable<any>} */
  newForm$: Observable<any>;

  /**@type {Observable<GeneralDetailsNewDto>} */
  generalDetails$: Observable<GeneralDetailsNewDto>;

  /**@type {Observable<FieldGroup[]>} */
  mainFields$: Observable<FieldGroup[]>;

  /**@type {Observable<ActionDto[]>} */
  actionsList$: Observable<ActionDto[]>;

  /**@type {string} */
  private _formType: string;

  /**@type {string} */
  private _formClass: FormClass;

  /**@type {string} */
   countryName: CountryName;

  /**@type {Subject<boolean>} */
  private readonly _destroy$: Subject<boolean> = new Subject<boolean>();

  /**@type {FormDynamicComponent} */
  @ViewChild(FormDynamicComponent) form: FormDynamicComponent;

  /**@type {MatStepper} */
  @ViewChild('stepper') private formStepper: MatStepper;

  constructor(
      private readonly _route: ActivatedRoute,
      private readonly _formConfigService: FormConfigService,
      private readonly _formStoreService: FormStoreService,
      private readonly _authService: AuthService,
      private readonly _mailService: MailService,
      private readonly _requestsService: RequestsService,
      private readonly _ngZone: NgZone,
      private readonly _router: Router,
      public notificationService: NotificationService
  ) {
    this.generalDetails$ = this._formStoreService.generalDetails$;
    this.mainFields$ = this._formStoreService.mainFields$;
    this.actionsList$ = this._formStoreService.actionsList$;
  }

  ngOnInit(): void {
    // Get form type from form config service
    this.currentForm$ = this._route.params
        .pipe(
            takeUntil(this._destroy$),
            switchMap((params: Params) => {
              this.countryName = params.id;
              this._formType = this._formConfigService.getFormTypeById(params.formId);
              this._formClass = this._formConfigService.getFormClassById(params.formId);
              return of(this._formConfigService.getFormConfigByIdAndCountry(params.formId, params.id))
            })
        );

    // Combine 3 observables for every step form values
    this.newForm$ = combineLatest([
      this.generalDetails$,
      this.mainFields$,
      this.actionsList$
    ]).pipe(
        takeUntil(this._destroy$),
        map(data => (
            {
              generalDetails: data[0],
              mainFields: data[1],
              actionsList: data[2]
            }
        ))
    );
  }

  ngAfterViewInit(): void {
    // Set numbers for indicators in stepper
    this.formStepper._getIndicatorType = () => 'number';
  }

  ngOnDestroy() {
    this._destroy$.next(true);
    this._destroy$.unsubscribe();
  }

  /**
   * Method for saving request in database and create separated table request view
   * @param criticalList {ActionRequestDto[]}
   */
  async setFormToDatabase(criticalList: ActionRequestDto[]) {
    const request: RequestDto = {
      comments: [this.prepareRequestComment()],
      requestDetails: this.prepareRequestDetails(),
      formDetails: {
        generalDetails: {...this._formStoreService.generalDetailsValue, inspectionDate: new Date()},
        mainFields: this._formStoreService.mainFieldsValue,
      },
      criticalActions: criticalList,
      status: RequestStatus.New,
    };

    const isSuperVisedInspection = this._formStoreService.generalDetailsValue?.supervisedInspection;

    const reqResponse = await this._requestsService.createRequest(request);

    await this._requestsService.updateRequestId(reqResponse.id);

    const tableRequest: RequestTableDto = await this.prepareTableRequest(isSuperVisedInspection, reqResponse.id);

    await this._requestsService.createTableRequest(
      tableRequest,
      reqResponse.id
    );

    return reqResponse.id;
  }

  /**
   * Handle submit modal and trigger saving in database, also send email with request link
   * @param criticalList
   */
  async handleSubmit(criticalList: ActionRequestDto[]) {
    const criticalListResult = criticalList.map(criticalItem =>  {
      return {
        ...criticalItem,
        notes: `${criticalItem.category} - ${criticalItem.label} : ${criticalItem.notes}`
      }
    });
    const reqId = await this.setFormToDatabase(criticalListResult);
    await this._ngZone.run(() => this._router.navigate(['dashboard/requests', reqId]));
    this._mailService.sendEmail(
        [this._formStoreService.generalDetailsValue?.approver,],
        this._formStoreService.generalDetailsValue?.approver,
        'The new request was created',
        `${environment.additionalFields.mainDomain}/dashboard/requests/${reqId}`
    );
  }

  /**
   *  Trigger prev step
   */
  goPrev() {
    this.formStepper.previous();
  }

  /**
   *  Trigger next step
   */
  goNext() {
    this.formStepper.next();
  }

  /**
   * Method for getting email division from user email
   * @param userEmail {string}
   */
  getDivision(userEmail: string) {
    return userEmail.split('@')[1].split('.')[0];
  }

  /**
   * Handler when generalDetails emitted
   * @param generalDetails {GeneralDetailsNewDto}
   */
  handleGeneralDetails(generalDetails: GeneralDetailsNewDto) {
    const fullGeneralDetails: GeneralDetailsNewDto = { ...generalDetails, formType: this._formType };
    console.log(fullGeneralDetails);
    this._formStoreService.setGeneralDetails(fullGeneralDetails);
    this.goNext();
  }

  /**
   * Handler when mainFields emitted, and creating a actions list form
   * @param mainFields
   */
  handleMainFields(mainFields: FieldGroup[]) {
    this._formStoreService.setMainFields(mainFields);
    if(this._formClass === FormClass.VehicleForm) {
      this.prepareActionsListForVehicleForm(mainFields);
    } else {
      this._formStoreService.setActionsList(this.prepareActionsList(this._formStoreService.mainFieldsValue));
    }
    this.goNext();
  }

  // For special cross field actions in Vehicle form, we add specific checking
  private prepareActionsListForVehicleForm(mainFields: FieldGroup[]) {
    if(this.isOdometerPotentialCriticalAction(mainFields) && this.isServiceDuePotentialCriticalAction(mainFields)) {
      this._formStoreService.setActionsList(this.prepareActionsListWithRegDueAndOdometerCriticalActions());
    }
    else if(this.isOdometerPotentialCriticalAction(mainFields)) {
      this._formStoreService.setActionsList(this.prepareActionsListWithOdometerCriticalAction());
    }
    else if(this.isServiceDuePotentialCriticalAction(mainFields)) {
      this._formStoreService.setActionsList(this.prepareActionsListWithRegDueCriticalAction());
    } else {
      this._formStoreService.setActionsList(this.prepareActionsList(this._formStoreService.mainFieldsValue));
    }
  }

  /**
   * Prepare actions list forms for rendering
   * @param mainFields {FieldGroup[]}
   */
  prepareActionsList(mainFields: FieldGroup[]): ActionDto[] {
    return mainFields.map(category => {
      return category.values
          .filter(field => (field.value === FormAnswerClassic.No || field.value === FormAnswerSecondary.NotSafe))
          .map(field => {
            return { ...field, category: category.category } as ActionDto;
          })
    }).filter(array => array.length).flat(1);
  }

  /**
   * If it's supervised inspection we add field supervisedInspection
   * @param supervisedInspection {boolean}
   * @param responseId {string}
   * @private
   */
  private async prepareTableRequest(supervisedInspection, responseId: string) {
    if(this._formClass === FormClass.VehicleForm) {

      return supervisedInspection ?
          {
            createdOn: new Date(),
            status: RequestStatus.New,
            owner: this._authService.currentUser.email,
            approver: this._formStoreService.generalDetailsValue?.approver,
            branchName: this._formStoreService.generalDetailsValue?.branchName,
            division: this.getDivision(this._authService.currentUser.email),
            formType: this._formStoreService.generalDetailsValue?.formType,
            id: responseId,
            registration: this.getRegistrationField(),
            supervisedInspection
          } :
          {
            createdOn: new Date(),
            status: RequestStatus.New,
            owner: this._authService.currentUser.email,
            approver: this._formStoreService.generalDetailsValue?.approver,
            branchName: this._formStoreService.generalDetailsValue?.branchName,
            division: this.getDivision(this._authService.currentUser.email),
            formType: this._formStoreService.generalDetailsValue?.formType,
            id: responseId,
            registration: this.getRegistrationField(),
          }
      } else {

        return {
          createdOn: new Date(),
          status: RequestStatus.New,
          owner: this._authService.currentUser.email,
          approver: this._formStoreService.generalDetailsValue?.approver,
          branchName: this._formStoreService.generalDetailsValue?.branchName,
          division: this.getDivision(this._authService.currentUser.email),
          formType: this._formStoreService.generalDetailsValue?.formType,
          id: responseId,
        }
      }
    }

  // Get registration field from mainFields stream
  private getRegistrationField() {
    return this._formStoreService.mainFieldsValue.find(group => group.category === VEHICLE_DEFAULTS)?.values.find(field => field.name === 'registration')?.value;
  }

  // Create first comment it request
  private prepareRequestComment() {
    return  {
      owner: 'System',
      date: Timestamp.now(),
      comment: `Request was created by ${this._authService.currentUser.email}`
    }
  }

  // Create payload for request details
  private prepareRequestDetails() {
    return {
      formType: this._formType,
      formClass: this._formClass,
      owner: this._authService.currentUser.email,
      ownerId: this._authService.currentUser.uid,
      approver: this._formStoreService.generalDetailsValue?.approver,
      createdOn: new Date(),
      division: this.getDivision(this._authService.currentUser.email),
      branchName: this._formStoreService.generalDetailsValue?.branchName,
      countryName: this.countryName
    }
  }

  /**
   * Here we combine checking - odometer value and service due exist and first greater then second
   * @param mainFields {string}
   * @private
   */
  private isOdometerPotentialCriticalAction(mainFields: FieldGroup[]) {
    const vehicleDefaults = mainFields.find(field => field.category === 'Vehicle Defaults');
    const odometerValue = this.getVehicleDefaultsField(mainFields, 'odometer');
    const serviceDueValue = this.getVehicleDefaultsField(mainFields, 'serviceDue');
    const isOdometerAndServiceDueExist = vehicleDefaults?.values?.some(field => field.name === 'odometer') && vehicleDefaults?.values?.some(field => field.name === 'serviceDue');

    return isOdometerAndServiceDueExist && this.isOdometerGreaterThanServiceDue(odometerValue, serviceDueValue);
  }

  private isServiceDuePotentialCriticalAction(mainFields: FieldGroup[]) {
    const regDueDate = this.getVehicleDefaultsField(mainFields, 'regDue');
    return this.countryName === CountryName.NewZealand.toLowerCase() && this.isRegDueBeforeThanInspectionDate(new Date(), regDueDate);
  }

  // Get field value by name from vehicle defaults category
  private getVehicleDefaultsField(mainFields: FieldGroup[], fieldName: string) {
    return mainFields.find(field => field.category === 'Vehicle Defaults')?.values?.find(field => field.name === fieldName)?.value;
  }

  /**
   * We check if odometerValue greater then serviceDue
   * @param odometerValue {string}
   * @param serviceDueValue {string}
   * @private
   */
  private isOdometerGreaterThanServiceDue(odometerValue: string, serviceDueValue: string) {
    return parseInt(odometerValue) >= parseInt(serviceDueValue);
  }

  /**
   *
   * @param inspectionDate {Date}
   * @param regDueDate {Date}
   * @private
   */
  private isRegDueBeforeThanInspectionDate(inspectionDate: Date, regDueDate: Date) {
    return inspectionDate.getTime() >= regDueDate.getTime();
  }

  /**
   * If we have odometer critical action, just add to list another object
   * @private
   */
  private prepareActionsListWithOdometerCriticalAction() {
    return [
      ...this.prepareActionsList(this._formStoreService.mainFieldsValue),
      {
        category: "Vehicle Defaults",
        label: "Odometer greater than service due",
        name: "odometerGreaterThanServiceDue",
        type: DynamicFieldType.Input,
        value: "Yes"
      },
    ]
  }

  /**
   * If we have reg due critical action, just add to list another object
   * @private
   */
  private prepareActionsListWithRegDueCriticalAction() {
    return [
      ...this.prepareActionsList(this._formStoreService.mainFieldsValue),
      {
        category: "Vehicle Defaults",
        label: "Registration Overdue",
        name: "inspectionDateGreaterThenServiceDue",
        type: DynamicFieldType.Input,
        value: "Yes"
      },
    ]
  }

  /**
   * If we have reg due critical action, just add to list another object
   * @private
   */
  private prepareActionsListWithRegDueAndOdometerCriticalActions() {
    return [
      ...this.prepareActionsList(this._formStoreService.mainFieldsValue),
      {
        category: "Vehicle Defaults",
        label: "Odometer greater than service due",
        name: "odometerGreaterThanServiceDue",
        type: DynamicFieldType.Input,
        value: "Yes"
      },
      {
        category: "Vehicle Defaults",
        label: "Registration Overdue",
        name: "inspectionDateGreaterThenServiceDue",
        type: DynamicFieldType.Input,
        value: "Yes"
      },
    ]
  }
}
