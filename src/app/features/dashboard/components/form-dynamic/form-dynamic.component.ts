import {Component, EventEmitter, Input, OnInit, Output, ViewChild} from "@angular/core";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {FieldGroup} from "../../models/form.interface";
import {MatAccordion} from "@angular/material/expansion";
import {FormConfigService} from "../../services/form-config.service";


@Component({
    exportAs: "formDynamic",
    selector: "form-dynamic",
    template: `
        <h6 class="text--secondary" *ngIf="showValidationMessage">Please fill all required fields*</h6>
        <form class="form-dynamic" [formGroup]="form" (submit)="onSubmit($event)">
            <mat-accordion multi>
                <mat-expansion-panel *ngFor="let category of formConfig">
                    <mat-expansion-panel-header
                            *ngIf="category.category">
                        <span class="text--semi-bold">{{category.category}}</span>
                    </mat-expansion-panel-header>
                    <div class="form-dynamic__content">
                        <ng-container
                                *ngFor="let value of category.values;"
                                dynamicField
                                [field]="value"
                                [category]="category.category"
                                [group]="form"
                        >
                        </ng-container>
                    </div>
                </mat-expansion-panel>
            </mat-accordion>

            <div class="row justify-content-between">
                <div class="col-4">
                    <button color="primary" type="button" mat-flat-button (click)="emitPrev()">
                        Back
                    </button>
                </div>
                <div class="col-4 text--right">
                    <button color="primary" mat-flat-button type="submit">
                        Next
                    </button>
                </div>
            </div>
        </form>
    `,
    styleUrls: ['./form-dynamic.component.scss']
})
export class FormDynamicComponent implements OnInit {

    /**@type {boolean} */
    showValidationMessage: boolean = false;

    /**@type {FieldGroup[]} */
    @Input() formConfig: FieldGroup[] = [];

    /**@type {MatAccordion} */
    @ViewChild(MatAccordion) accordion: MatAccordion;

    /**@type {EventEmitter<any>} */
    @Output() mainFieldsEmitter: EventEmitter<any> = new EventEmitter<any>();

    /**@type {EventEmitter<any>} */
    @Output() goToGeneralDetailsStep: EventEmitter<any> = new EventEmitter<any>();

    /**@type {FormGroup} */
    form: FormGroup;

    /**
     * Getter for form values
     */
    get value() {
        return this.form.value;
    }

    constructor(
        private readonly _fb: FormBuilder,
        private readonly _formConfigService: FormConfigService,
    ) {
    }

    ngOnInit() {
        this.form = this.createControl();
    }

    /**
     * Method for go to prev step
     */
    emitPrev() {
        this.goToGeneralDetailsStep.emit();
    }

    /**
     * Method where on submit we disable default behaviour, if form valid we emit prepared data to the parent, else open all accordions
     * @param event
     */
    onSubmit(event: Event) {
        event.preventDefault();
        event.stopPropagation();
        if (this.form.valid) {
            this.showValidationMessage = false;
            this.mainFieldsEmitter.emit(this.prepareFormData(this.formConfig, this.form.value));
        } else {
            this.validateAllFormFields(this.form);
            this.accordion.openAll();
            this.showValidationMessage = true;
        }
        // this.mainFieldsEmitter.emit(this.prepareFormData(this.formConfig, this.form.value));
    }

    /**
     * Method for aggregate data, after we submit form, we don't have label and category for every field, that's why we compare object and array from config, and aggregate them to one array
     * @param formConfig - json which we set up in form config service
     * @param formValues - values which we get after submit
     */
    private prepareFormData(formConfig: FieldGroup[], formValues: Object) {
        const preparedConfig = FormDynamicComponent.prepareConfigData(formConfig);
        let formData = [];
        Object.entries(formValues).forEach(currentItem => {
            const [key, value] = currentItem;
            formData = preparedConfig.map(item => {
                if (item.values.find(field => field.name === key)) {
                    item.values.find(singleInput => singleInput.name === key).value = value
                }
                return item
            });
        });
        return formData;
    }

    /**
     * Method for prepared config data for aggregating
     * @param config {FieldGroup[]}
     */
    private static prepareConfigData(config: FieldGroup[]) {
        return config.reduce((acc, item) => {
            const fields = item.values.map(single => ({
                name: single.name,
                label: single.label,
                type: single.type
            }));
            acc.push({
                category: item.category,
                values: fields
            });
            return acc
        }, [])
    }

    /**
     * Create every control in model, bind validation and add it to the form group
     */
    private createControl() {
        const group = this._fb.group({});
        this.formConfig.forEach(category => {
            category.values.forEach(field => {
                if (field.type === "button") return;
                const control = this._fb.control(
                    field.value,
                    this.bindValidations(field.validations || [])
                );
                group.addControl(field.name, control);
            });
        });
        return group;
    }

    /**
     * Bind validations to the field
     * @param validations {any}
     */
    private bindValidations(validations: any) {
        if (validations.length > 0) {
            const validList = [];
            validations.forEach(valid => {
                validList.push(valid.validator);
            });
            return Validators.compose(validList);
        }
        return null;
    }

    /**
     * If form invalid - trigger validation for every field
     * @param formGroup - {FormGroup}
     */
    private validateAllFormFields(formGroup: FormGroup) {
        Object.keys(formGroup.controls).forEach(field => {
            const control = formGroup.get(field);
            control.markAsTouched({onlySelf: true});
        });
    }
}
