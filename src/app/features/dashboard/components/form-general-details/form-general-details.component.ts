import {Component, EventEmitter, Input, OnDestroy, OnInit, Output} from '@angular/core';
import {AbstractControl, FormBuilder, FormGroup, ValidatorFn, Validators} from "@angular/forms";
import {FormConfigService} from "../../services/form-config.service";
import {AuthService} from "../../../../core/services/auth.service";
import {CountryName, FormClass, VehicleType} from "../../../../shared/enums/shared.enums";
import {GeneralDetailsNewDto} from "../../models/general-details-new.model";
import {takeUntil} from "rxjs/operators";
import {ActivatedRoute} from "@angular/router";
import {Subject} from "rxjs";
import {ConstantsService} from "../../../../shared/services/constants.service";
import {ThemePalette} from '@angular/material/core';
import {NotificationService} from "../../../../shared/services/notification.service";

@Component({
    selector: 'app-form-general-details',
    templateUrl: './form-general-details.component.html',
    styles: [
        `
            mat-form-field {
                width: 100%;
                font-size: 14px;
            }

            mat-form-field + mat-form-field {
                margin-top: 17px;
            }
        `
    ]
})

export class FormGeneralDetailsComponent implements OnInit, OnDestroy {

    /**@type {FormGroup} */
    generalDetailsForm: FormGroup;

    /**@type {string[]} */
    branches: string[];

    /**@type {VehicleType[]} */
    vehicleTypes: VehicleType[];

    /**@type {boolean} */
    requestsFormSubmit: boolean = true;

    /**
     *  Max field-date allowed for Inspection Date Picker
     *  @type {Date}
     */
    maxDate: Date = new Date();

    /**@type {CountryName} */
    countryName: CountryName;

    /**@type {Subject<boolean>} */
    private readonly _destroy$: Subject<boolean> = new Subject<boolean>();

    /**@type {EventEmitter<any>} */
    @Output() generalDetailsEmitter = new EventEmitter();

    /**@type {FormClass} */
    @Input() formClass: FormClass;

    constructor(
        private _formConfigService: FormConfigService,
        private _fb: FormBuilder,
        private _authService: AuthService,
        private _route: ActivatedRoute,
        private _constantsService: ConstantsService,
        private _notificationService: NotificationService,
    ) {
        this.vehicleTypes = _formConfigService.vehicleTypes;
        this.generalDetailsForm = this._fb.group({
                branchName: [null, Validators.required],
                driver: [_authService.currentUser.displayName, Validators.required],
                inspectionDate: [new Date(), Validators.required],
                approver: [this._authService?.currentApprover, [
                    Validators.required,
                    Validators.pattern(new RegExp(`(^[A-Za-z0-9._%+-]+${this._constantsService.DOMAIN_ADDRESS})|(^[A-Za-z0-9._%+-]+${this._constantsService.DOMAIN_ADDRESS_ADDITIONAL})`)),
                    this.restrictPersonalUserEmailValidator()
                ]],
                supervisedInspection: [false, []],
            }
        )
        this._notificationService.isLoadingLocal = false;
    }

    ngOnInit(): void {
        this._route.params
            .pipe(
                takeUntil(this._destroy$),
            ).subscribe(data => {
            this.countryName = data.id;
            this.branches = this._formConfigService.getBranchesByCountry(this.countryName);
        });
    }

    ngOnDestroy() {
        this._destroy$.next(true);
        this._destroy$.unsubscribe();
    }

    /**
     * Method, when form is valid, emit form values to parent
     */
    onSubmit() {
        if (this.generalDetailsForm.invalid) {
            return;
        }
        this.emitGeneralDetails(this.generalDetailsForm.value);
    }

    /**
     * Here we check that our formClass should includes supervised inspection
     */
    isFormWithSupervisedInspection() {
        return this.formClass === FormClass.VehicleForm;
    }

    /**
     * We check if form does not have supervised inspection, we emit value without this field
     * @param formValues
     */
    emitGeneralDetails(formValues) {
        console.log(formValues);
        if(!this.isFormWithSupervisedInspection()) {
            const {supervisedInspection, ...newFormValues} = formValues;

            this.generalDetailsEmitter.emit(new GeneralDetailsNewDto(newFormValues))
        } else {
            this.generalDetailsEmitter.emit(new GeneralDetailsNewDto(formValues))
        }
    }

    // Custom validator for exclude user email as approver
    private restrictPersonalUserEmailValidator(): ValidatorFn {
        return (control: AbstractControl): { [key: string]: any } | null => {
            return control.value.toLowerCase() === this._authService?.currentUser?.email ? { restrictPersonalUserEmailValidator: control.value } : null;
        }
    }
}
