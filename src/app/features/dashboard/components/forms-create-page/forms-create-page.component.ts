import { Component } from '@angular/core';
import { AuthService } from "../../../../core/services/auth.service";

@Component({
  selector: 'app-dashboard',
  templateUrl: './forms-create-page.component.html',
  styleUrls: ['./forms-create-page.component.scss']
})
export class FormsCreatePageComponent {

  constructor(public authService: AuthService) {
  }

}
