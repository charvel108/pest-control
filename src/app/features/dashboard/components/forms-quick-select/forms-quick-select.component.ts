import {Component, OnInit} from '@angular/core';
import {AuthService} from "../../../../core/services/auth.service";
import {FormConfigService} from "../../services/form-config.service";
import {formDynamic} from "../../models/form.interface";
import {NotificationService} from "../../../../shared/services/notification.service";
import {ActivatedRoute} from "@angular/router";
import {Subject} from "rxjs";
import {CountryName} from "../../../../shared/enums/shared.enums";

@Component({
    selector: 'app-forms-quick-select',
    templateUrl: './forms-quick-select.component.html',
    styleUrls: ['./forms-quick-select.component.scss']
})
export class FormsQuickSelectComponent implements OnInit {

    /**@type {formDynamic[]} */
    forms: formDynamic[];

    /**@type {formDynamic[]} */
    countryName: CountryName;

    /**@type {Subject<boolean>} */
    private readonly _destroy$: Subject<boolean> = new Subject<boolean>();

    constructor(
        public authService: AuthService,
        private readonly _formConfigService: FormConfigService,
        private readonly _notificationService: NotificationService,
        private readonly _route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.countryName = this.authService.currentCountry;
        this.forms = this._formConfigService.getFormsByCountry(this.countryName?.toLowerCase());
    }

    ngOnDestroy() {
        this._destroy$.next();
        this._destroy$.complete();
    }

}
