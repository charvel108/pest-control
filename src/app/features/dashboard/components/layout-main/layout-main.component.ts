import { Component, ElementRef, ViewChild } from '@angular/core';
import { AuthService } from "../../../../core/services/auth.service";
import { NotificationService } from "../../../../shared/services/notification.service";

@Component({
  selector: 'app-layout-main',
  templateUrl: './layout-main.component.html',
  styleUrls: ['./layout-main-component.scss']
})
export class LayoutMainComponent {

  /**@type {boolean} */
  isOpen: boolean = true;

  /**@type {ElementRef} */
  @ViewChild('start') start: ElementRef;

  constructor(
      public authService: AuthService,
      public notificationService: NotificationService
  ) { }

  /**
   * When menu is open
   */
  onMenuUpdate() {
    this.isOpen = !this.isOpen;
  }

}
