import { Component, EventEmitter, Input, Output } from '@angular/core';
import { ActionRequestDto, ActionStatus } from "../../models/action.interface";

@Component({
  selector: 'app-non-compliant-answer',
  templateUrl: './non-compliant-answer.component.html',
  styleUrls: ['./non-compliant-answer.component.scss']
})
export class NonCompliantAnswerComponent {

  /**@type {string} */
  readonly defaultImage: string = 'assets/image-placeholder.png';

  /**@type {ActionRequestDto} */
  @Input() actionData: ActionRequestDto;

  /**@type {ActionRequestDto} */
  @Input() requestId: string;

  /**@type {boolean} */
  @Input() isYourRequest: boolean;

  /**@type {string} */
  @Input() owner: string;

  /**@type {EventEmitter<ActionRequestDto>} */
  @Output() duplicateHandler = new EventEmitter<ActionRequestDto>();

  /**@type {EventEmitter<ActionRequestDto>} */
  @Output() criticalHandler = new EventEmitter<ActionRequestDto>();

  /**
   * Method for marking issue as duplicate
   */
  markAsDuplicate() {
    this.duplicateHandler.emit({ ...this.actionData, actionStatus: ActionStatus.Duplicate } as ActionRequestDto);
  }

  /**
   * Method for marking issue as critical
   */
  markAsCritical() {
    this.criticalHandler.emit({ ...this.actionData, actionStatus: ActionStatus.Critical } as ActionRequestDto);
  }

  /**
   * Method for check if action consist in critical list
   */
  isInActionsList() {
    return this.actionData.criticalListId
  }

  /**
   * Method for check if action duplicate
   */
  isDuplicate() {
    return this.actionData.actionStatus === ActionStatus.Duplicate
  }

  /**
   * Method for check if answer in disabled state
   */
  isDisabledState() {
    return this.actionData.actionStatus !== ActionStatus.New || !this.isYourRequest;
  }

}
