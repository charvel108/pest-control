import { Component, OnInit } from '@angular/core';
import {
  AbstractControl,
  FormBuilder,
  FormGroup, ValidatorFn,
  Validators
} from "@angular/forms";
import 'firebase/firestore';
import { AuthService } from "../../../../core/services/auth.service";
import {CountryName, VehicleType} from "../../../../shared/enums/shared.enums";
import {ConstantsService} from "../../../../shared/services/constants.service";


@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {

  /**@type {string[]} */
  usersEmails: string[];

  /**@type {FormGroup} */
  requestPrefsForm: FormGroup;

  /**@type {FormGroup} */
  vehiclePrefsForm: FormGroup;

  /**@type {VehicleType[]} */
  vehicleTypes: VehicleType[];

  /**@type {CountryName[]} */
  countries: CountryName[];

  constructor(
      public readonly _authService: AuthService,
      private readonly _fb: FormBuilder,
      private readonly _constantsService: ConstantsService
  ) {

    this.vehiclePrefsForm = this._fb.group({
      registration: [_authService.userVehicleDefaults.registration, [Validators.pattern(/^[a-z0-9]+$/i)]],
      makeAndModel: [_authService.userVehicleDefaults.makeAndModel, []],
      type: [_authService.userVehicleDefaults.type, []],
      licenceExpiry: [this.convertOnDate(_authService?.userVehicleDefaults?.licenceExpiry), []]
    })
  }

  ngOnInit(): void {

    console.log(this._authService.currentUser)

    this.countries = this._constantsService.countriesList;

    this.vehicleTypes = this._constantsService.vehicleTypes;

    this.requestPrefsForm = this._fb.group({
      approver: [ this._authService?.currentApprover, [
            Validators.pattern(new RegExp(`(^[A-Za-z0-9._%+-]+${this._constantsService.DOMAIN_ADDRESS})|(^[A-Za-z0-9._%+-]+${this._constantsService.DOMAIN_ADDRESS_ADDITIONAL})`)),
            this.restrictPersonalUserEmailValidator()
        ]],
      countryName: [ this._authService?.currentCountry, []]
    });

  }

  /**
   * Update preferences form
   */
  updateRequestPrefsForm() {
    if (this.requestPrefsForm.invalid) {
      return;
    }
    const newApprover = this.requestPrefsForm.value.approver?.trim().toLowerCase() ?? "";
    const countryName = this.requestPrefsForm.value.countryName || null ;
    this.requestPrefsForm.patchValue({ approver: newApprover })
    this._authService.updateUserPreferences(
        { requestPrefs: { approver: newApprover, countryName }
        });
  }

  /**
   * Update vehicle form
   */
  vehiclePrefsFormUpdate() {
    if (this.vehiclePrefsForm.invalid) {
      return;
    }
    let values = {
      vehiclePrefs: {
        ...this.vehiclePrefsForm.value,
        type: this.vehiclePrefsForm.value.type ? this.vehiclePrefsForm.value.type : null
      }
    };
    this._authService.updateUserPreferences(values);
  }

  // Method for converting on field-date
  convertOnDate(timestamp) {
    return timestamp ? timestamp.toDate() : null;
  }

  // Custom validator for exclude user email as approver
  private restrictPersonalUserEmailValidator(): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } | null => {
      return control.value?.toLowerCase() === this._authService?.currentUser?.email ? { restrictPersonalUserEmailValidator: control.value } : null;
    }
  }


}
