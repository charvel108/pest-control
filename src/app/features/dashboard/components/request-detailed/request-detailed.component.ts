import {Component, NgZone, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {RequestsService} from "../../services/requests.service";
import {Subject} from "rxjs";
import {Comment} from "../../models/comment.model";
import {AuthService} from "../../../../core/services/auth.service";
import {RequestDto} from "../../models/request.model";
import {filter, takeUntil} from "rxjs/operators";
import {ActionRequestDto, ActionStatus, CriticalActionDto, CriticalActionStatus} from "../../models/action.interface";
import {AngularFirestore} from "@angular/fire/firestore";
import {MatDialog, MatDialogConfig} from "@angular/material/dialog";
import {SubmitModalComponent} from "../../../../shared/components/submit-modal/submit-modal.component";
import {NotificationService} from "../../../../shared/services/notification.service";
import {MailService} from "../../../../core/services/mail.service";
import {environment} from "../../../../../environments/environment.prod";
import {FormBuilder, FormGroup} from '@angular/forms';
import firebase from 'firebase/app';
import {FormClass, RequestStatus} from "../../../../shared/enums/shared.enums";
import {ConstantsService} from "../../../../shared/services/constants.service";
import {ModalChangeApproverComponent} from "../../../../shared/components/modal-change-approver/modal-change-approver.component";
import Timestamp = firebase.firestore.Timestamp;
import {VEHICLE_DEFAULTS} from "../../../../shared/dto/constants";
import {DataStorageService} from "../../../../shared/services/data-storage.service";

@Component({
    selector: 'app-request-detailed',
    templateUrl: './request-detailed.component.html',
    styleUrls: ['./request-detailed.component.scss']
})
export class RequestDetailedComponent implements OnInit, OnDestroy {

    /**@type {RequestDto} */
    selectedRequest: RequestDto;

    /**@type {FormGroup} */
    changeApproverState: boolean = false;

    /**@type {Subject<boolean>} */
    private readonly _destroy$: Subject<boolean> = new Subject<boolean>();

    constructor(
        private readonly _route: ActivatedRoute,
        private readonly _requestsService: RequestsService,
        private readonly _afs: AngularFirestore,
        private readonly _dialog: MatDialog,
        private readonly _ngZone: NgZone,
        private readonly _router: Router,
        private readonly _formBuilder: FormBuilder,
        private _notificationService: NotificationService,
        private _mailService: MailService,
        private _authService: AuthService,
        private _constantsService: ConstantsService,
        private _dataStorageService: DataStorageService,
    ) {}


    ngOnInit() {
        this._notificationService.isLoadingLocal = true;
        // Get request by id
        this._requestsService.getRequest(this._route.snapshot.params['id'])
            .pipe(takeUntil(this._destroy$))
            .subscribe(async request => {
                if (request) {
                    this.selectedRequest = request;
                    console.log(this.selectedRequest);
                } else {
                    await this._ngZone.run(() => this._router.navigate(['dashboard/requests']));
                }
            }
        );
    }

    ngOnDestroy() {
        this._destroy$.next(true);
        this._destroy$.unsubscribe();
    }

    /**
     * Trigger method for send email notifications from mail service
     * @param recipients {string[]}
     * @param firstName {string}
     * @param notificationType {string}
     * @param link {string}
     */
    sendNotificationEmail(recipients: string[], firstName: string, notificationType: string, link: string) {
        this._mailService.sendEmail(recipients, firstName, notificationType, link)
    }

    /**
     * Method for trigger approve modal
     */
    handleApproveModal() {
        const dialogConfig = new MatDialogConfig();
        dialogConfig.autoFocus = true;
        dialogConfig.disableClose = false;
        const dialogRef = this._dialog.open(SubmitModalComponent, dialogConfig);

        dialogRef.afterClosed()
            .pipe(
                takeUntil(this._destroy$),
                filter(value => value)
            )
            .subscribe(async () => {
                    try {
                        await Promise.all([
                            await this._dataStorageService.approveRequest(this.selectedRequest.id),
                            await this._dataStorageService.approveTableRequest(this.selectedRequest.id)
                        ]);

                        await this._dataStorageService.addNewComment(
                            this.selectedRequest.id,
                            this.generateComment(`Request was approved by ${this._authService.currentUser.email}.`)
                        );

                        this.sendNotificationEmail(
                            [this.selectedRequest.requestDetails?.owner],
                            this.selectedRequest.requestDetails?.owner,
                            'The request was approved.',
                            `${environment.additionalFields.mainDomain}/dashboard/requests/${this.selectedRequest.id}`
                        );

                        this._notificationService.openCustomMessageSnackbar('Request successfully approved.', 'Close');
                    } catch (e) {
                        this._notificationService.openCustomMessageSnackbar('Something went wrong.', 'Close');
                    }
                }
            );
    }

    /**
     * Method for trigger decline modal
     */
    handleDeclineModal() {
        const dialogConfig = new MatDialogConfig();
        dialogConfig.autoFocus = true;
        dialogConfig.disableClose = false;
        const dialogRef = this._dialog.open(SubmitModalComponent, dialogConfig);

        dialogRef.afterClosed()
            .pipe(
                takeUntil(this._destroy$),
                filter(value => value)
            )
            .subscribe(async () => {
                    try {
                        await Promise.all([
                            await this._dataStorageService.declineRequest(this.selectedRequest.id),
                            await this._dataStorageService.declineTableRequest(this.selectedRequest.id)
                        ]);

                        await this._dataStorageService.addNewComment(
                            this.selectedRequest.id,
                            this.generateComment(`Request was declined by ${this._authService.currentUser.email}.`)
                        );

                        this.sendNotificationEmail(
                            [this.selectedRequest.requestDetails?.owner],
                            this.selectedRequest.requestDetails?.owner,
                            'The request was declined.',
                            `${environment.additionalFields.mainDomain}/dashboard/requests/${this.selectedRequest.id}`
                        );

                        this._notificationService.openCustomMessageSnackbar('Request successfully declined.', 'Close');
                    } catch (e) {
                        this._notificationService.openCustomMessageSnackbar('Something went wrong.', 'Close');
                    }
                }
            );
    }

    /**
     * Method for change approver modal
     */
    handleChangeApproverModal() {
        const dialogConfig = new MatDialogConfig();
        dialogConfig.autoFocus = true;
        dialogConfig.disableClose = false;
        dialogConfig.data = {approver: this.selectedRequest?.requestDetails?.approver};
        dialogConfig.panelClass = 'modal--approver';
        const dialogRef = this._dialog.open(ModalChangeApproverComponent, dialogConfig);

        dialogRef.afterClosed()
            .pipe(
                takeUntil(this._destroy$),
                filter(value => value )
            )
            .subscribe(async newApprover => {
                    try {
                        this._notificationService.isLoadingLocal = true;
                        await Promise.all([
                            await this._dataStorageService.updateRequestApprover(this.selectedRequest.id, newApprover),
                            await this._dataStorageService.updateTableRequestApprover(this.selectedRequest.id, newApprover)
                        ]);

                        await this._dataStorageService.addNewComment(
                            this.selectedRequest.id,
                            this.generateComment(`${this.selectedRequest?.requestDetails?.owner} has changed approver to ${newApprover}`)
                        );
                        this.sendNotificationEmail(
                            [newApprover],
                            newApprover,
                            `The ${this.selectedRequest?.requestDetails?.owner} has assigned request to you!`,
                            `${environment.additionalFields.mainDomain}/dashboard/requests/${this.selectedRequest.id}`
                        );

                        this._notificationService.openCustomMessageSnackbar('Approver was successfully updated', 'Close');
                        this._notificationService.isLoadingLocal = false;
                        this.changeApproverState = false;
                    } catch (e) {
                        this._notificationService.openCustomMessageSnackbar('Something went wrong.', 'Close');
                    }
                }
            );
    }

    private generateComment(comment: string): Comment {
        return {
            owner: 'System',
            comment,
            date: Timestamp.now(),
        }
    };

    /**
     * When new comment added, save in database and send email
     * @param comment {Comment}
     */
    async onNewComment(comment: Comment) {
        const newCommentsList: Comment[] = [...this.selectedRequest.comments.slice(), comment];
        await this._requestsService.addComment(this.selectedRequest.id, newCommentsList);
        this.sendNotificationEmail(
            this.setRecipients(),
            this.setRecipients().join(', '),
            `The user ${this._authService.currentUser.email} has added a comment: "${comment.comment}".`,
            `${environment.additionalFields.mainDomain}/dashboard/requests/${this.selectedRequest.id}`
        )
    }

    /**
     * Check if request approver is equal to your email
     * @param requestApprover {string}
     */
    isYourRequest(requestApprover: string) {
        return requestApprover === this._authService.currentUser?.email;
    }

    /**
     * Check if request is New
     * @param requestStatus {RequestStatus}
     */
    isNewRequest(requestStatus: RequestStatus) {
        return requestStatus === RequestStatus.New;
    }

    private setRecipients() {
        const {
            approver,
            owner
        } = this.selectedRequest.requestDetails;
        const recipients = new Set();
        if ((this._authService.currentUser?.email !== owner)) {
            recipients.add(owner);
        }
        if (this._authService.currentUser?.email !== approver) {
            recipients.add(approver);
        }
        if (!recipients.size) {
            recipients.add(this._authService.currentUser?.email);
        }

        return [...recipients] as string[];

    }

    isAllowedToChangeStatus(requestStatus: RequestStatus, requestApprover: string) {
        return requestStatus !== RequestStatus.New || requestApprover !== this._authService.currentUser?.email
    }

    handleDuplicateModal(action: ActionRequestDto) {
        const dialogConfig = new MatDialogConfig();
        dialogConfig.autoFocus = true;
        dialogConfig.disableClose = false;
        const dialogRef = this._dialog.open(SubmitModalComponent, dialogConfig);

        dialogRef.afterClosed()
            .pipe(
                takeUntil(this._destroy$),
                filter(value => value)
            )
            .subscribe(
                async () => {
                    try {
                        this._notificationService.isLoadingLocal = true;
                        await this.addToCriticalList(action, CriticalActionStatus.Duplicate);
                        this._notificationService.openCustomMessageSnackbar('Was added to list as duplicated', 'Close');
                        this._notificationService.isLoadingLocal = false;
                    } catch (e) {
                        this._notificationService.openCustomMessageSnackbar('Something went wrong', 'Close');
                    }
                }
            );
    }

    async addToCriticalList(action: ActionRequestDto, status: CriticalActionStatus) {

        const id = this._afs.createId();

        const criticalAction: CriticalActionDto = this.prepareCriticalAction(action, status, id);

        const newData = this.selectedRequest.criticalActions
            .map(item => item.id === action.id ? {...action, criticalListId: id} : item
        );

        await this._dataStorageService.addToCriticalList(id, criticalAction);
        await this._dataStorageService.updateActionsList(this.selectedRequest.id, action.id, newData);
    }

    handleAddingToCriticalList(action: ActionRequestDto) {
        const dialogConfig = new MatDialogConfig();
        dialogConfig.autoFocus = true;
        dialogConfig.disableClose = false;
        const dialogRef = this._dialog.open(SubmitModalComponent, dialogConfig);

        dialogRef.afterClosed()
            .pipe(
                takeUntil(this._destroy$),
                filter(value => value)
            )
            .subscribe(
                async val => {
                    try {
                        this._notificationService.isLoadingLocal = true;
                        await this.addToCriticalList(action, CriticalActionStatus.Open);
                        this._notificationService.openCustomMessageSnackbar('Was successfully added to critical list', 'Close');
                        this._notificationService.isLoadingLocal = false;
                    } catch (e) {
                        this._notificationService.openCustomMessageSnackbar('Something went wrong.', 'Close');
                    }
                }
            );
    }

    isAllActionsChecked(actionsList: ActionRequestDto[]) {
        return actionsList.every(action => action.actionStatus !== ActionStatus.New)
    }

    /**
     * Return true if request status is equal to New
     */
    isNewStatus() {
        return this.selectedRequest?.status === RequestStatus.New;
    }

    /**
     * Return true if user is owner of request
     */
    isOwnerRequest() {
        return this.selectedRequest.requestDetails.owner === this._authService.currentUser?.email;
    }

    /**
     * New status + user is owner
     */
    isAllowedToChangeApprover() {
        return this.isNewStatus() && this.isOwnerRequest();
    }

    private isVehicleFormRequest() {
        return this.selectedRequest.requestDetails.formClass === FormClass.VehicleForm;
    }

    // Get registration field from mainFields stream
    private getRegistrationField() {
        return this.selectedRequest?.formDetails?.mainFields?.find(group => group.category === VEHICLE_DEFAULTS)?.values.find(field => field.name === 'registration')?.value;
    }

    private prepareCriticalAction(action: ActionRequestDto, status: CriticalActionStatus, id: string) {
        return this.isVehicleFormRequest ?
            {
                id,
                createdOn: new Date(),
                approver: this.selectedRequest?.requestDetails?.approver,
                owner: this.selectedRequest?.requestDetails?.owner,
                branchName: this.selectedRequest?.requestDetails?.branchName,
                status,
                imageUrl: action.imageUrl,
                notes: action.notes,
                requestId: this.selectedRequest.id,
                formType: this.selectedRequest.requestDetails.formType,
                registration: this.getRegistrationField()
            } :
            {
                id,
                createdOn: new Date(),
                approver: this.selectedRequest?.requestDetails?.approver,
                owner: this.selectedRequest?.requestDetails?.owner,
                branchName: this.selectedRequest?.requestDetails?.branchName,
                status,
                imageUrl: action.imageUrl,
                notes: action.notes,
                requestId: this.selectedRequest.id,
                formType: this.selectedRequest.requestDetails.formType
            }
    }
}
