import {Component, EventEmitter, OnDestroy, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup} from "@angular/forms";
import {Subject} from "rxjs";
import {FormConfigService} from "../../services/form-config.service";
import {BranchGroup} from "../../models/form.interface";
import {animate, AUTO_STYLE, state, style, transition, trigger} from "@angular/animations";
import {RequestStatus, StorageKeys} from "../../../../shared/enums/shared.enums";
import {StorageService} from "../../../../shared/services/storage.service";

@Component({
    selector: 'app-request-filters',
    templateUrl: './request-filters.component.html',
    styleUrls: ['./request-filters.component.scss'],
    animations: [
        trigger('collapse', [
            state('false', style({height: AUTO_STYLE, visibility: AUTO_STYLE})),
            state('true', style({height: '0', visibility: 'hidden'})),
            transition('false => true', animate('300ms ease-in')),
            transition('true => false', animate('300ms ease-out'))
        ])
    ]
})

export class RequestFiltersComponent implements OnInit, OnDestroy {

    statuses: RequestStatus[];
    types: string[];
    filtersForm: FormGroup;
    collapsed: boolean = false;
    maxDate: Date;
    private readonly destroy$: Subject<boolean> = new Subject<boolean>();

    /**@type {BranchGroup[]} */
    branchGroups: BranchGroup[];

    @Output() newFilters = new EventEmitter();

    constructor(
        private _fb: FormBuilder,
        private _formConfigService: FormConfigService,
        private _storageService: StorageService
    ) {
        this.maxDate = new Date();
    }

    ngOnInit() {
        if (this._storageService.getItem(StorageKeys.RequestFilters)) {
            const filtersFormGroup: any = this.transformFiltersToFromGroup(this._storageService.getItem(StorageKeys.RequestFilters));
            this.filtersForm = this._fb.group(filtersFormGroup);
        } else {
            this.filtersForm = this._fb.group({
                status: [[], []],
                branch: [[], []],
                approver: ['', []],
                type: ['', []],
                myForms: ['', []],
                fromDateValue: ['', []],
                toDateValue: ['', []],
                registration: ['', []]
            });
        }

        this.types = this._formConfigService.formTypes;

        this.statuses = this._formConfigService.requestStatuses;

        this.branchGroups = this._formConfigService.branchGroups;

    }

    ngOnDestroy() {
        this.destroy$.next(true);
        this.destroy$.unsubscribe();
    }

    transformFiltersToFromGroup(formObject) {
        return Object.entries(formObject)
            .map(([key, value] : [key: string, value: any])=> {
                if((key === 'fromDateValue') || (key === 'toDateValue')  ) {
                    return [key, value ? new Date(value) : '']
                }
                return [key, value];
            })
            .reduce((acc, iterable) => {
                const [key, value] = iterable;
                acc[key] = [value, []]
                return acc
            }, {});
    }

    filtersEmitter(filter: any) {
        this.newFilters.emit(filter);
    }

    toggle() {
        this.collapsed = !this.collapsed;
    }

    expand() {
        this.collapsed = false;
    }

    collapse() {
        this.collapsed = true;
    }

    resetFilters() {
        this.newFilters.emit({
            approver: "",
            fromDateValue: "",
            myForms: "",
            status: [],
            branch: [],
            toDateValue: "",
            type: "",
            registration: ""
        })
        this.filtersForm.reset();
        this._storageService.removeItem(StorageKeys.RequestFilters);
    }

    emitFilterValues(filtersFormValue) {
        const filter = {
            ...filtersFormValue,
            approver: filtersFormValue.approver?.trim().toLowerCase() || "",
            fromDateValue: filtersFormValue.fromDateValue ? new Date(filtersFormValue.fromDateValue)?.getTime() : "",
            toDateValue: this.filtersForm.value.toDateValue ? new Date(this.filtersForm.value.toDateValue)?.setHours(23, 59, 59) : "",
            registration: filtersFormValue.registration?.trim().toLowerCase() || "",
            type: filtersFormValue.type || "",
        } as string;
        this.filtersEmitter(filter);
        return filter;
    }

    onApplyFilters() {
        if (this.filtersForm.invalid) {
            return;
        }
        this.emitFilterValues(this.filtersForm.value);
        this._storageService.setItem(StorageKeys.RequestFilters, this.emitFilterValues(this.filtersForm.value));
    }
}
