import {ChangeDetectionStrategy, Component, Input, OnDestroy, ViewChild} from '@angular/core';
import {FormInstance} from "../../models/form.interface";
import {MatAccordion} from "@angular/material/expansion";
import {BreakpointObserver} from "@angular/cdk/layout";
import {map, takeUntil} from "rxjs/operators";
import {Observable, Subject} from "rxjs";

@Component({
  selector: 'app-request-form-view',
  templateUrl: './request-form-view.component.html',
  styleUrls: ['./request-form-view.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})

export class RequestFormViewComponent implements OnDestroy {

  /**@type {boolean} */
  accordionState: boolean = false;

  /**@type {Subject<boolean>} */
  private readonly _destroy$: Subject<boolean> = new Subject<boolean>();

  /**@type {FormGroup} */
  private mediaStream$: Observable<boolean>;

  /**@type {FormGroup} */
  @Input() formDetails: FormInstance;

  /**@type {FormGroup} */
  @ViewChild(MatAccordion) accordion: MatAccordion;

  constructor(
      private readonly _breakpoint: BreakpointObserver
  ) {
    this.mediaStream$ = this._breakpoint
        .observe(`(min-width: 768px)`)
        .pipe(
            takeUntil(this._destroy$),
            map(({ matches }) => matches)
        );
  }

  ngOnDestroy(): void {
    this._destroy$.next(true);
    this._destroy$.unsubscribe();
  }

  /**
   * Method for toggle all accordions
   */
  toggleAccordion() {
    this.accordionState = !this.accordionState;
    this.accordionState ? this.accordion.openAll() : this.accordion.closeAll()
  }


}
