import {AfterViewInit, Component, Input, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {RequestsService} from '../../services/requests.service';
import {Observable, Subject} from "rxjs";
import {MatTableDataSource} from "@angular/material/table";
import {MatSort} from "@angular/material/sort";
import {MatPaginator} from "@angular/material/paginator";
import {publishReplay, refCount, takeUntil} from "rxjs/operators";
import {AuthService} from "../../../../core/services/auth.service";
import {RequestTableDto} from "../../models/request.model";
import {AngularFirestore} from "@angular/fire/firestore";
import {FormConfigService} from "../../services/form-config.service";
import {StorageService} from "../../../../shared/services/storage.service";
import {StorageKeys} from "../../../../shared/enums/shared.enums";

@Component({
    selector: 'app-request-table',
    templateUrl: './request-table.component.html',
    styleUrls: ['./request-table.component.scss']
})
export class RequestTableComponent implements OnInit, OnDestroy, AfterViewInit {

    isLoading: boolean = false;
    dataSource: MatTableDataSource<RequestTableDto>;
    displayedColumns: string[] = ['createdOn', 'status', 'owner', 'approver', 'branchName', 'division', 'formType'];
    destroy$: Subject<boolean> = new Subject<boolean>();

    private _cache$: Observable<Array<RequestTableDto>>;

    @Input() showFilters: boolean = false;
    @ViewChild(MatSort) sort: MatSort;
    @ViewChild(MatPaginator, {static: false}) paginator: MatPaginator;

    constructor(
        private _requestsService: RequestsService,
        private _authService: AuthService,
        private _firestore: AngularFirestore,
        private _formConfigService: FormConfigService,
        private _storageService: StorageService,
    ) {
    }

    ngOnInit() {
        this.isLoading = true;
        this.backendTableRequests()
            .subscribe((requests) => {
                this.dataSource = new MatTableDataSource(requests);
                this.dataSource.sort = this.sort;
                this.dataSource.paginator = this.paginator;
                this.isLoading = false;
                this.dataSource.filterPredicate = ((data, filter) => {
                    const a = !filter.status?.length || (filter.status?.includes(data.status)); // filter by Status
                    const b = !filter.branch?.length || (filter.branch?.includes(data.branchName)); // filter by Branch
                    const c = !filter.approver || data.approver?.toLowerCase().includes(filter.approver); // filter by Email
                    const d = !filter.type || data.formType === filter.type; // filter by Type
                    const e = !filter.myForms || data.approver === this._authService?.currentUser.email; // filter by Forms For me
                    const f = (!filter.fromDateValue || !filter.toDateValue) || ((data.createdOn?.toDate().getTime()) >= filter.fromDateValue) && (data.createdOn?.toDate().getTime() <= filter.toDateValue + 1); // Filter by from and to Date
                    const g = !filter.registration || data.registration?.toLowerCase().includes(filter.registration); // Filter by vehicle registration
                    return a && b && c && d && e && f && g;
                }) as (RequestTableDto, string) => boolean;

                if (this._storageService.getItem(StorageKeys.RequestFilters)) {
                    this.dataSource.filter = this._storageService.getItem(StorageKeys.RequestFilters);
                }
            });
        this.dataSource = new MatTableDataSource([]);
    }

    ngAfterViewInit() {
        this.dataSource.paginator = this.paginator;
    }

    ngOnDestroy() {
        this.destroy$.next(true);
        this.destroy$.unsubscribe();
    }

    onNewFilters(filters) {
        this.dataSource.filter = filters;
    }

    get tableRequests() {
        if (!this._cache$) {
            this._cache$ = this.backendTableRequests().pipe(
                publishReplay(1),
                refCount()
            );
        }

        return this._cache$;
    }

    private backendTableRequests() {
        return this._requestsService.getTableRequests();
    }

}
