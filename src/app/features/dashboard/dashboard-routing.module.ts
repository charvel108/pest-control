import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RequestsLayoutComponent } from "./components/requests-layout/requests-layout.component";
import { RequestDetailedComponent } from "./components/request-detailed/request-detailed.component";
import { ProfileComponent } from "./components/profile/profile.component";
import { LayoutMainComponent } from "./components/layout-main/layout-main.component";
import { FormContainerComponent } from "./components/form-container/form-container.component";
import { CriticalActionsListComponent } from "./components/critical-actions-list/critical-actions-list.component";
import {FormsCreatePageComponent} from "./components/forms-create-page/forms-create-page.component";

const routes: Routes = [
  {
    path: '',
    component: LayoutMainComponent,
    children: [
      {
        path: '',
        redirectTo: 'forms'
      },
      {
        path: 'requests',
        component: RequestsLayoutComponent
      },
      {
        path: 'requests/:id',
        component: RequestDetailedComponent
      },
      {
        path: 'critical-list',
        component: CriticalActionsListComponent
      },
      {
        path: 'critical-list/:id',
        component: CriticalActionsListComponent
      },
      {
        path: 'profile',
        component: ProfileComponent,
      },
      {
        path: 'forms',
        component: FormsCreatePageComponent
      },
      {
        path: 'forms/create/:id/:formId',
        component: FormContainerComponent
      },
    ]
  },

  {
    path: '**', redirectTo: ''
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})

export class DashboardRoutingModule {
}
