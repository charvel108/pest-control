import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {DashboardRoutingModule} from "./dashboard-routing.module";
import {FormsCreatePageComponent} from "./components/forms-create-page/forms-create-page.component";
import {FormsQuickSelectComponent} from "./components/forms-quick-select/forms-quick-select.component";
import {SharedModule} from "../../shared/shared.module";
import {RequestsService} from "./services/requests.service";
import {RequestFormViewComponent} from "./components/request-form-view/request-form-view.component";
import {RequestsLayoutComponent} from "./components/requests-layout/requests-layout.component";
import {RequestTableComponent} from "./components/request-table/request-table.component";
import {RequestFiltersComponent} from "./components/request-filters/request-filters.component";
import {RequestDetailedComponent} from "./components/request-detailed/request-detailed.component";
import {CommentsComponent} from "./components/comments/comments.component";
import {ProfileComponent} from "./components/profile/profile.component";
import {InputComponent} from './components/field-input/input.component';
import {SelectComponent} from './components/field-select/select.component';
import {CheckboxComponent} from './components/field-checkbox/checkbox.component';
import {ButtonComponent} from './components/field-button/button.component';
import {RadioButtonComponent} from './components/field-radio-button/radio-button.component';
import {DynamicFieldDirective} from "./directives/dynamic-field.directive";
import {DateComponent} from "./components/field-date/date.component";
import {LayoutMainComponent} from "./components/layout-main/layout-main.component";
import {FormConfigService} from "./services/form-config.service";
import {FormContainerComponent} from "./components/form-container/form-container.component";
import {FormGeneralDetailsComponent} from './components/form-general-details/form-general-details.component';
import {FormStoreService} from "./services/form-store.service";
import {FormActionsListComponent} from "./components/form-actions-list/form-actions-list.component";
import {NonCompliantAnswerComponent} from "./components/non-compliant-answer/non-compliant-answer.component";
import {CriticalActionsListComponent} from './components/critical-actions-list/critical-actions-list.component';
import {CriticalActionsExpandableComponent} from './components/critical-actions-expandable/critical-actions-expandable.component';
import {CriticalActionsFiltersComponent} from './components/critical-actions-filters/critical-actions-filters.component';
import {FormDynamicComponent} from "./components/form-dynamic/form-dynamic.component";


@NgModule({
    declarations: [
        FormsQuickSelectComponent,
        FormsCreatePageComponent,
        LayoutMainComponent,
        RequestFormViewComponent,
        RequestsLayoutComponent,
        RequestTableComponent,
        RequestFiltersComponent,
        RequestDetailedComponent,
        CommentsComponent,
        ProfileComponent,
        InputComponent,
        SelectComponent,
        CheckboxComponent,
        DateComponent,
        ButtonComponent,
        RadioButtonComponent,
        DynamicFieldDirective,
        FormDynamicComponent,
        FormContainerComponent,
        FormGeneralDetailsComponent,
        FormActionsListComponent,
        NonCompliantAnswerComponent,
        CriticalActionsListComponent,
        CriticalActionsExpandableComponent,
        CriticalActionsFiltersComponent
    ],
    imports: [
        CommonModule,
        SharedModule,
        DashboardRoutingModule,
    ],
    providers: [
        RequestsService,
        FormConfigService,
        FormStoreService
    ],
    entryComponents: [
        InputComponent,
        ButtonComponent,
        SelectComponent,
        DateComponent,
        RadioButtonComponent,
        CheckboxComponent
    ]
})
export class DashboardModule {
}
