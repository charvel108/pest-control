import {
  ComponentFactoryResolver,
  Directive,
  Input,
  OnInit,
  ViewContainerRef
} from "@angular/core";
import { FormGroup } from "@angular/forms";
import { InputComponent } from "../components/field-input/input.component";
import { ButtonComponent } from "../components/field-button/button.component";
import { SelectComponent } from "../components/field-select/select.component";
import { RadioButtonComponent } from "../components/field-radio-button/radio-button.component";
import { CheckboxComponent } from "../components/field-checkbox/checkbox.component";
import { FieldConfig } from "../models/field.interface";
import { DateComponent } from "../components/field-date/date.component";

// Create the types of components which we can to add to the dynamic form
const componentMapper = {
  input: InputComponent,
  button: ButtonComponent,
  select: SelectComponent,
  date: DateComponent,
  radiobutton: RadioButtonComponent,
  checkbox: CheckboxComponent
};

@Directive({
  selector: '[dynamicField]'
})
export class DynamicFieldDirective implements OnInit {


  /**@type {any} */
  componentRef: any;

  /**@type {FieldConfig} */
  @Input() field: FieldConfig;

  /**@type {FormGroup} */
  @Input() group: FormGroup;

  /**@type {string} */
  @Input() category: string;

  /**@type {string} */
  @Input() label: string;

  /**@type {boolean} */
  @Input() isShow?: boolean;

  constructor(
      private readonly _resolver: ComponentFactoryResolver,
      private readonly _container: ViewContainerRef
  ) {
  }

  ngOnInit() {

    // Resolver form create dynamic fields
    const factory = this._resolver.resolveComponentFactory(
        componentMapper[this.field.type]
    );

    this.componentRef = this._container.createComponent(factory);

    this.componentRef.instance.field = this.field;

    this.componentRef.instance.group = this.group;

    this.componentRef.instance.category = this.category;

    this.componentRef.instance.isShow = this.isShow;
  }

}
