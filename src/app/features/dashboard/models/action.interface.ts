import firebase from "firebase/app";
import Timestamp = firebase.firestore.Timestamp;
import {DynamicFieldType} from "./field.interface";

export enum ActionStatus {
  New = "New",
  Duplicate = "Duplicate",
  Critical = "Critical"
}

export enum CriticalActionStatus {
  Open = "Open",
  Resolved = "Resolved",
  InProgress = "In Progress",
  Duplicate = "Duplicate"
}

export interface ActionDto {
  name : string;
  label : string;
  value : string;
  category : string;
  type ? : DynamicFieldType
}

export interface ActionRequestDto extends ActionDto {
  id: string;
  imageUrl: string;
  notes: string;
  actionStatus: ActionStatus;
  criticalListId: string;
}

export interface CriticalActionDto {
  id: string
  createdOn: Date | Timestamp;
  approver: string;
  branchName: string;
  status: CriticalActionStatus;
  imageUrl: string;
  notes: string;
  requestId: string;
  owner: string;
  formType?: string;
  registration?: string;
}
