import firebase from "firebase/app";
import Timestamp = firebase.firestore.Timestamp;

export class Comment {
  owner: string;
  date: Timestamp;
  comment: string;
}
