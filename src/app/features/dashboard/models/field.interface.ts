export interface Validator {
    name: string;
    validator: any;
    message: string;
}

export interface FieldConfig {
    label?: string;
    name?: string;
    inputType?: string;
    options?: string[];
    collections?: any;
    type: DynamicFieldType;
    value?: any;
    validations?: Validator[];
}

export enum RadioButtonTrueFalse {
    'Yes',
    'No'
}

export enum DynamicFieldType {
    Input = 'input',
    Button = 'button',
    Select = 'select',
    Date = 'date',
    Radiobutton = 'radiobutton',
    Checkbox = 'checkbox'
}
