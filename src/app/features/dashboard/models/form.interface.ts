import { FieldConfig } from "./field.interface";
import {BranchName, CountryName, FormClass} from "../../../shared/enums/shared.enums";
import {GeneralDetailsNewDto} from "./general-details-new.model";

export interface BranchGroup {
  disabled?: boolean;
  branchCategory: CountryName;
  branches: BranchName[];
}

export interface FieldGroup {
  category: string,
  values: FieldConfig[]
}

export interface formDynamic {
  formType: string;
  formClass: FormClass;
  formId: string;
  config: FieldGroup[];
  countryName: CountryName
}

export interface FormInstance {
  generalDetails: GeneralDetailsNewDto,
  mainFields: FieldGroup[];
}