import {BranchName} from "../../../shared/enums/shared.enums";

export class GeneralDetailsNewDto {
    formType?: string;
    branchName: BranchName;
    driver: string;
    inspectionDate: Date;
    approver: string;
    supervisedInspection?: boolean;

    constructor(generalDetailsData: GeneralDetailsNewDto) {
        this.formType = generalDetailsData.formType;
        this.branchName = generalDetailsData.branchName;
        this.driver = generalDetailsData.driver;
        this.inspectionDate = generalDetailsData.inspectionDate;
        this.approver = generalDetailsData?.approver.trim().toLowerCase();
        if(generalDetailsData.supervisedInspection) {
            this.supervisedInspection = generalDetailsData.supervisedInspection;
        }
    }
}
