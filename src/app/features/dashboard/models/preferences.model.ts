import firebase from "firebase/app";
import Timestamp = firebase.firestore.Timestamp;
import {CountryName} from "../../../shared/enums/shared.enums";

export class Preferences {
  requestPrefs: {
    approver: string;
    countryName: CountryName;
  };
  vehiclePrefs: {
    registration: string;
    makeAndModel: string;
    type: string;
    licenceExpiry: Timestamp;
    licenceNumber: string;
  };
}
