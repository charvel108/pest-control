import {Comment} from "./comment.model";
import {FormInstance} from "./form.interface";
import { ActionRequestDto } from "./action.interface";
import firebase from "firebase/app";
import Timestamp = firebase.firestore.Timestamp;
import {BranchName, CountryName, FormClass, RequestStatus} from "../../../shared/enums/shared.enums";

export class RequestDetails {
  formType: string;
  owner: string;
  ownerId?: string;
  approver: string;
  createdOn: Timestamp | Date;
  branchName: string;
  division: string;
  countryName?: CountryName;
  formClass?: FormClass;
}

export class RequestDto {
  comments: Comment[];
  requestDetails: RequestDetails;
  formDetails: FormInstance;
  criticalActions: ActionRequestDto[];
  id?: string;
  status: RequestStatus;
}


export interface RequestTableDto {
  createdOn: Timestamp | Date;
  status: RequestStatus;
  owner: string;
  approver: string;
  branchName: BranchName;
  division: string;
  formType: string;
  id: string;
  supervisedInspection?: boolean;
}
