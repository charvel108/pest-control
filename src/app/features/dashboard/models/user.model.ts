import { Preferences } from "./preferences.model";

export interface GoogleData  {
  uid: string;
  displayName: string;
  email: string;
  phoneNumber: string;
  photoURL: string;
}

export interface User extends GoogleData{
  preferences: Preferences;
}

