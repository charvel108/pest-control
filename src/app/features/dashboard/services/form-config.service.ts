import {Injectable} from '@angular/core';
import {BranchGroup, formDynamic,} from "../models/form.interface";
import {Validators} from "@angular/forms";
import {CriticalActionStatus} from "../models/action.interface";
import {AuthService} from "../../../core/services/auth.service";
import {
    BranchName,
    CountryName,
    FormAnswerClassic,
    FormAnswerSecondary,
    FormClass,
    RequestStatus,
    VehicleType
} from "../../../shared/enums/shared.enums";
import {DynamicFieldType} from "../models/field.interface";

@Injectable()
export class FormConfigService {

    /**@type {string[]} */
    formTypes: string[];

    /**@type {RequestStatus[] } */
    readonly requestStatuses: RequestStatus[] = [
        RequestStatus.New,
        RequestStatus.Approved,
        RequestStatus.Declined
    ];

    /**@type {CriticalActionStatus[]} */
    readonly criticalActionsStatuses: CriticalActionStatus[] = [
        CriticalActionStatus.Open,
        CriticalActionStatus.InProgress,
        CriticalActionStatus.Resolved,
        CriticalActionStatus.Duplicate
    ];

    /**@type { VehicleType[] } */
    readonly vehicleTypes: VehicleType[] = [
        VehicleType.Van,
        VehicleType.Truck,
        VehicleType.Ute,
        VehicleType.Car,
        VehicleType.Other
    ];

    readonly australianBranches: BranchName[] = [
        BranchName.Adelaide,
        BranchName.Albury,
        BranchName.Brisbane,
        BranchName.Canberra,
        BranchName.DarlingDowns,
        BranchName.Darwin,
        BranchName.Marulan,
        BranchName.Melbourne,
        BranchName.Newcastle,
        BranchName.NorthQueensland,
        BranchName.Perth,
        BranchName.Sydney,
        BranchName.Tasmania,
    ];

    readonly newZealandBranches: BranchName[] = [
        BranchName.Albany,
        BranchName.Auckland,
        BranchName.Blenheim,
        BranchName.Christchurch,
        BranchName.Cromwell,
        BranchName.Dunedin,
        BranchName.Hamilton,
        BranchName.Queenstown,
        BranchName.Napier,
        BranchName.Nelson,
        BranchName.NewPlymouth,
        BranchName.PalmerstonNorth,
        BranchName.Rotorua,
        BranchName.Timaru,
        BranchName.Wellington,
        BranchName.Whangarei,
    ];

    readonly fijiBranches: BranchName[] = [
        BranchName.Suva,
        BranchName.Lautoka
    ];

    /**@type {BranchName[]} */
    readonly branches: BranchName[] = [
        ...this.australianBranches,
        ...this.newZealandBranches,
        ...this.fijiBranches

    ];

    /**@type {BranchGroup[]} */
    readonly branchGroups: BranchGroup[] = [
        {
            branchCategory: CountryName.Australia,
            branches: [...this.australianBranches]
        },
        {
            branchCategory: CountryName.NewZealand,
            branches: [...this.newZealandBranches]
        },
        {
            branchCategory: CountryName.Fiji,
            branches: [...this.fijiBranches]
        },
    ];

    /**
     * Form config for all types of forms
     * @type {formDynamic[]}
     */
    readonly formsConfig: formDynamic[] = [

        // Australian Forms
        {
            formType: 'Ambius Service Form',
            formClass: FormClass.VehicleForm,
            formId: 'v-ambi-form',
            countryName: CountryName.Australia,
            config: [
                {
                    category: 'Vehicle Defaults',
                    values: [
                        {
                            type: DynamicFieldType.Input,
                            label: "Odometer Reading",
                            name: "odometer",
                            validations: [
                                {
                                    name: 'required',
                                    validator: Validators.required,
                                    message: 'This field is required!'
                                },
                                {
                                    name: 'pattern',
                                    validator: Validators.pattern(/^[0-9]+$/),
                                    message: 'Allow only numbers'
                                },
                            ],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Input,
                            label: "Service Due (KM)",
                            name: "serviceDue",
                            validations: [
                                {
                                    name: 'required',
                                    validator: Validators.required,
                                    message: 'This field is required!'
                                },
                                {
                                    name: 'pattern',
                                    validator: Validators.pattern(/^[0-9]+$/),
                                    message: 'Allow only numbers'
                                },
                            ],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Input,
                            label: "Make and Model",
                            name: "makeAndModel",
                            validations: [
                                {
                                    name: 'required',
                                    validator: Validators.required,
                                    message: 'This field is required!'
                                },
                            ],
                            value: this._authService.userVehicleDefaults?.makeAndModel
                        },
                        {
                            type: DynamicFieldType.Select,
                            label: "Vehicle Type",
                            name: "type",
                            options: [...this.vehicleTypes],
                            validations: [
                                {
                                    name: 'required',
                                    validator: Validators.required,
                                    message: 'This field is required!'
                                },
                            ],
                            value: this._authService.userVehicleDefaults?.type
                        },
                        {
                            type: DynamicFieldType.Select,
                            label: "Do you have a current driver licence?",
                            name: "isLicenseConsist",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [
                                {
                                    name: 'required',
                                    validator: Validators.required,
                                    message: 'This field is required!'
                                },
                            ],
                            value: ''
                        },
                        {
                            type: DynamicFieldType.Date,
                            label: "Drivers Licence Expiry Date",
                            name: "driversLicenceExpiryDate",
                            validations: [
                                {
                                    name: 'required',
                                    validator: Validators.required,
                                    message: 'This field is required!'
                                },
                            ],
                            value: this._authService.userVehicleDefaults?.licenceExpiry?.toDate()
                        },
                        {
                            type: DynamicFieldType.Input,
                            label: "Vehicle Registration",
                            name: "registration",
                            validations: [
                                {
                                    name: 'required',
                                    validator: Validators.required,
                                    message: 'This field is required!'
                                },
                                {
                                    name: 'pattern',
                                    validator: Validators.pattern(/^[a-z0-9]+$/i),
                                    message: 'Registration can only consist from letters or numbers'
                                },
                            ],
                            value: this._authService.userVehicleDefaults?.registration
                        },
                    ]
                },
                {
                    category: 'Tyres',
                    values: [
                        {
                            type: DynamicFieldType.Select,
                            label: "Tyre Pressure",
                            name: "tyrePressure",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            label: "Tread Depth",
                            name: "treadDepth",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "goodCondition",
                            label: "Tyres in Good Condition",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "spareServiceable",
                            label: "Spare Serviceable",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                    ]
                },
                {
                    category: 'Brakes',
                    values: [
                        {
                            type: DynamicFieldType.Select,
                            name: "brakingPower",
                            label: "Braking Power",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "handBrake",
                            label: "Hand Brake",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        }
                    ]
                },
                {
                    category: 'Seat Belts',
                    values: [
                        {
                            type: DynamicFieldType.Select,
                            name: "tensionerFunctions",
                            label: "Belt Tensioner Functions",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "beltsWork",
                            label: "Belts work and are in good condition",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        }
                    ]
                },
                {
                    category: 'Wipers',
                    values: [
                        {
                            type: DynamicFieldType.Select,
                            name: "wipersFunction",
                            label: "Wipers Function",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "washerFluidLevel",
                            label: "Washer Fluid Level is High",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        }
                    ]
                },
                {
                    category: 'Windows & Windscreen',
                    values: [
                        {
                            type: DynamicFieldType.Select,
                            name: "windscreen",
                            label: "Windscreen",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "windows",
                            label: "Windows",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                    ]
                },
                {
                    category: 'Lights',
                    values: [
                        {
                            type: DynamicFieldType.Select,
                            name: "numberPlate",
                            label: "Number Plate",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "highBeam",
                            label: "High Beam",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "lowBeam",
                            label: "Low Beam",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "parking",
                            label: "Parking",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "reverse",
                            label: "Reverse",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "brakes",
                            label: "Brakes",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                    ]
                },
                {
                    category: 'Indicators',
                    values: [
                        {
                            type: DynamicFieldType.Select,
                            name: "indicatorFront",
                            label: "Front",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "indicatorSide",
                            label: "Side",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "indicatorRear",
                            label: "Rear",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "indicatorHazards",
                            label: "Hazards",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                    ]
                },
                {
                    category: 'Mirrors',
                    values: [
                        {
                            type: DynamicFieldType.Select,
                            name: "rearSideMirror",
                            label: "Rear/Side Mirrors",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "fishEyeSpotter",
                            label: 'Fish Eye Spotter',
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        }
                    ]
                },
                {
                    category: 'Maintenance',
                    values: [
                        {
                            type: DynamicFieldType.Select,
                            name: "engineStarts",
                            label: 'Engine Starts 1st Time',
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "oilLevel",
                            label: 'Oil Levels',
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "waterLevel",
                            label: 'Water Levels',
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "brakeFluidLevels",
                            label: 'Brake Fluid Levels',
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "airConditioning",
                            label: 'Air Conditioning',
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "gpsSticker",
                            label: 'GPS Sticker',
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "heightLevel",
                            label: 'Height Label',
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "doorFunction",
                            label: 'Doors Function Properly',
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "strutsRams",
                            label: 'Struts and Rams',
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "reverseBeeper",
                            label: 'Reverse Beeper',
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "reverseCameraScreen",
                            label: 'Reverse Camera Screen',
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "reverseCameraCameraLens",
                            label: 'Reverse Camera Lens',
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "horn",
                            label: 'Horn',
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "isVehicleClean",
                            label: 'Vehicle Clean',
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "accidentDamage",
                            label: 'Is the vehicle free from damage?',
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "decals",
                            label: 'Decals',
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        }
                    ]
                },
                {
                    category: 'Other',
                    values: [
                        {
                            type: DynamicFieldType.Select,
                            name: "tailgate",
                            label: 'Tailgate',
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                    ]
                },
                {
                    category: 'General Items',
                    values: [
                        {
                            type: DynamicFieldType.Select,
                            name: "stickers",
                            label: "Have Take 5 Stickers",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "manualHandbook",
                            label: "Manual Handbook",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "fireExtinguisher",
                            label: "Fire Extinguisher",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "extinguisherServiced",
                            label: "Extinguisher Serviced",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "extinguisherAccessible",
                            label: "Extinguisher Accessible",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "firstAidKit",
                            label: "First Aid Kit",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "handSanitiser",
                            label: "Hand Sanitiser",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "sdsFolder",
                            label: "SDS Folder",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        }
                    ]
                },
                {
                    category: 'Spill Kit',
                    values: [
                        {
                            type: DynamicFieldType.Select,
                            name: "absorbentGranules",
                            label: "Absorbent Granules",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "glovesAndGlassesAndMask",
                            label: "PPE - Gloves, Safety Glasses, Dust Mask",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "dustPanAndBrush",
                            label: "Dust Pan & Brush",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "tongs",
                            label: "Tongs",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "bucketWithLid",
                            label: "Bucket with lid and heavy duty plastic bag",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "sharpsCleanUpKit",
                            label: "Sharps clean up kit",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "mop",
                            label: "Mop",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        }
                    ]
                },
                {
                    category: 'Other Equipment',
                    values: [
                        {
                            type: DynamicFieldType.Select,
                            name: "trolley",
                            label: "Trolley",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "stepLadder",
                            label: "Step Ladder (1.8m Industrial Grade)",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "technicalKitBag",
                            label: "Technician Kit Bag",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "bucketAndWateringCan",
                            label: "Bucket and Watering Can",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "secateursAndLeatherSheaf",
                            label: "Secateurs and Leather Sheaf",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "cleaningCloths",
                            label: "Cleaning Cloths (Sponges and Chux)",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "sprayBottles",
                            label: "Spray Bottles (2 x 1L)",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "chemicalsLabelled",
                            label: "All Chemicals Labelled with contents",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "soilMoisture",
                            label: "Soil Moisture Testing Probe",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "waterPipette",
                            label: "Water Measuring Pipette",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "wireTie",
                            label: "Wire Tie",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "heavyDutyPlasticBug",
                            label: "Heavy Duty Plastic Bag",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "yellowWarningSun",
                            label: "Yellow Warning Sign",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        }
                    ]
                },
            ]
        },

        {
            formType: 'Fumigation Service Form',
            formClass: FormClass.VehicleForm,
            formId: 'v-fumi-form',
            countryName: CountryName.Australia,
            config: [
                {
                    category: 'Vehicle Defaults',
                    values: [
                        {
                            type: DynamicFieldType.Input,
                            label: "Odometer Reading",
                            name: "odometer",
                            validations: [
                                {
                                    name: 'required',
                                    validator: Validators.required,
                                    message: 'This field is required!'
                                },
                                {
                                    name: 'pattern',
                                    validator: Validators.pattern(/^[0-9]+$/),
                                    message: 'Allow only numbers'
                                },
                            ],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Input,
                            label: "Service Due (KM)",
                            name: "serviceDue",
                            validations: [
                                {
                                    name: 'required',
                                    validator: Validators.required,
                                    message: 'This field is required!'
                                },
                                {
                                    name: 'pattern',
                                    validator: Validators.pattern(/^[0-9]+$/),
                                    message: 'Allow only numbers'
                                },
                            ],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Input,
                            label: "Make and Model",
                            name: "makeAndModel",
                            validations: [
                                {
                                    name: 'required',
                                    validator: Validators.required,
                                    message: 'This field is required!'
                                },
                            ],
                            value: this._authService.userVehicleDefaults?.makeAndModel
                        },
                        {
                            type: DynamicFieldType.Select,
                            label: "Vehicle Type",
                            name: "type",
                            options: [...this.vehicleTypes],
                            validations: [
                                {
                                    name: 'required',
                                    validator: Validators.required,
                                    message: 'This field is required!'
                                },
                            ],
                            value: this._authService.userVehicleDefaults?.type
                        },
                        {
                            type: DynamicFieldType.Select,
                            label: "Do you have a current driver licence?",
                            name: "isLicenseConsist",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [
                                {
                                    name: 'required',
                                    validator: Validators.required,
                                    message: 'This field is required!'
                                },
                            ],
                            value: ''
                        },
                        {
                            type: DynamicFieldType.Date,
                            label: "Drivers Licence Expiry Date",
                            name: "driversLicenceExpiryDate",
                            validations: [
                                {
                                    name: 'required',
                                    validator: Validators.required,
                                    message: 'This field is required!'
                                },
                            ],
                            value: this._authService.userVehicleDefaults?.licenceExpiry?.toDate()
                        },
                        {
                            type: DynamicFieldType.Input,
                            label: "Vehicle Registration",
                            name: "registration",
                            validations: [
                                {
                                    name: 'required',
                                    validator: Validators.required,
                                    message: 'This field is required!'
                                },
                                {
                                    name: 'pattern',
                                    validator: Validators.pattern(/^[a-z0-9]+$/i),
                                    message: 'Registration can only consist from letters or numbers'
                                },
                            ],
                            value: this._authService.userVehicleDefaults?.registration
                        },
                    ]
                },
                {
                    category: 'Tyres',
                    values: [
                        {
                            type: DynamicFieldType.Select,
                            label: "Tyre pressure",
                            name: "tyrePressure",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            label: "Tread depth",
                            name: "treadDepth",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "goodCondition",
                            label: "Tyres in good condition",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "spareServiceable",
                            label: "Spare serviceable",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                    ]
                },
                {
                    category: 'Brakes',
                    values: [
                        {
                            type: DynamicFieldType.Select,
                            name: "brakingPower",
                            label: "Braking Power",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "handBrake",
                            label: "Hand Brake",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        }
                    ]
                },
                {
                    category: 'Seat Belts',
                    values: [
                        {
                            type: DynamicFieldType.Select,
                            name: "tensionerFunctions",
                            label: "Tensioner Functions",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "beltsWork",
                            label: "Belts work and are in good condition",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        }
                    ]
                },
                {
                    category: 'Wipers',
                    values: [
                        {
                            type: DynamicFieldType.Select,
                            name: "wipersFunction",
                            label: "Wipers function",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "washerFluidLevel",
                            label: "Washer fluid level",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        }
                    ]
                },
                {
                    category: 'Windows & Windscreen',
                    values: [
                        {
                            type: DynamicFieldType.Select,
                            name: "windscreen",
                            label: "Windscreen",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "windows",
                            label: "Windows",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                    ]
                },
                {
                    category: 'Lights',
                    values: [
                        {
                            type: DynamicFieldType.Select,
                            name: "numberPlate",
                            label: "Number Plate",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "highBeam",
                            label: "High Beam",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "lowBeam",
                            label: "Low Beam",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "parking",
                            label: "Parking",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "reverse",
                            label: "Reverse",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "brakes",
                            label: "Brakes",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                    ]
                },
                {
                    category: 'Indicators',
                    values: [
                        {
                            type: DynamicFieldType.Select,
                            name: "indicatorFront",
                            label: "Front",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "indicatorSide",
                            label: "Side",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "indicatorRear",
                            label: "Rear",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "indicatorHazards",
                            label: "Hazards",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                    ]
                },
                {
                    category: 'Mirrors',
                    values: [
                        {
                            type: DynamicFieldType.Select,
                            name: "rearSideMirror",
                            label: "Rear/Side Mirrors",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "fishEyeSpotter",
                            label: 'Fish Eye Spotter',
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        }
                    ]
                },
                {
                    category: 'Maintenance',
                    values: [
                        {
                            type: DynamicFieldType.Select,
                            name: "engineStarts",
                            label: 'Engine Starts 1st Time',
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "oilLevel",
                            label: 'Oil Levels',
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "waterLevel",
                            label: 'Water Levels',
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "brakeFluidLevels",
                            label: 'Brake Fluid Levels',
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "airConditioning",
                            label: 'Air Conditioning',
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "gpsSticker",
                            label: 'GPS Sticker',
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "heightLevel",
                            label: 'Height Label',
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "doorFunction",
                            label: 'Door Functioning',
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "strutsRams",
                            label: 'Struts and Rams',
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "horn",
                            label: 'Horn',
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "reverseCameraScreen",
                            label: 'Reverse Camera Screen',
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "reverseCameraCameraLens",
                            label: 'Reverse Camera Lens',
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "reverseBeeper",
                            label: 'Reverse Beeper',
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "isVehicleClean",
                            label: 'Vehicle Clean',
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "vehicleBodyCondition",
                            label: 'Vehicle Body Condition',
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "rearCompartment",
                            label: 'Rear Compartment',
                            options: ["Safe", "Not Safe"],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "decals",
                            label: 'Decals',
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "trayCover",
                            label: 'Tray Cover',
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "chassisRail",
                            label: 'Chassis Rail',
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        }
                    ]
                },
                {
                    category: 'General Items',
                    values: [
                        {
                            type: DynamicFieldType.Select,
                            name: "stickers",
                            label: "Take 5 Stickers",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "fireExtinguisher",
                            label: "Fire Extinguisher",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "extinguisherServiced",
                            label: "Extinguisher Serviced",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "extinguisherAccessible",
                            label: "Extinguisher Accessible",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "firstAidKit",
                            label: "First Aid Kit (Missing Items Listed in Comments)",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "sdsFolder",
                            label: "Safety Data Sheet (SDS) Folder",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "manualHandbook",
                            label: "Vehicle Manual & Drivers Handbook",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "handSanitiser",
                            label: "Hand Sanitiser",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "soapDispenser",
                            label: "Soap Dispenser (complete with spare soap)",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "fanHeaters",
                            label: "Fan & Heaters",
                            options: ["Yes", "No", "N/A"],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "extensionCord",
                            label: "Extension Cord",
                            options: ["Safe", "Not Safe"],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        }
                    ]
                },
                {
                    category: 'Manuals/Required Information',
                    values: [
                        {
                            type: DynamicFieldType.Select,
                            name: "fumigationTechnicalFolder",
                            label: "Fumigation Technical Folder",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "emergencyResponseInformation",
                            label: "Emergency Response Information (TeamCards) for chemicals being carried",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        }
                    ]
                },
                {
                    category: 'General Vehicle Equipment',
                    values: [
                        {
                            type: DynamicFieldType.Select,
                            name: "insectRepellent",
                            label: "Insect Repellent",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "approvedFumigationTape",
                            label: "Approved Fumigation Tape",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "fumigationWarningSigns",
                            label: "Fumigation Warning Signs",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "bollardsOrBunting",
                            label: "Bollards / Bunting",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "cableTies",
                            label: "Cable Ties",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "sunscreen",
                            label: "Sunscreen",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "waterBottle",
                            label: "20L Water Bottle (Isuzu Models only)",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                    ]
                },
                {
                    category: 'Dangerous Goods',
                    values: [
                        {
                            type: DynamicFieldType.Select,
                            name: "dangerousGoods",
                            label: "Dangerous Goods stored correctly",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "spillKit",
                            label: "Spill Kit",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        }
                    ]
                },
                {
                    category: 'Equipment',
                    values: [
                        {
                            type: DynamicFieldType.Select,
                            name: "marineHotWaterSystem",
                            label: "Marine Hot Water System",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "bottleRacks",
                            label: "Bottle Racks",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "tempRecordingDevices",
                            label: "Temp. Recording Devices",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "shiftingSpanner",
                            label: "Shifting Spanner",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "profumeSpanner",
                            label: "Profume Spanner",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "gasMonitoringLines",
                            label: "Gas / Monitoring Lines",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "safetyGlasses",
                            label: "SafetyGlasses",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "pvsMonitorExtensionTubes",
                            label: "PVC Monitor Extension Tubes",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "methylBromideDispenser",
                            label: "Methyl Bromide Dispenser",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "ispmStamps",
                            label: "ISPM Stamps",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "personalDetectionDevice",
                            label: "Personal Detection Device",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "ladder",
                            label: "Ladder",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        }
                    ]
                },
                {
                    category: 'Digital Equipment Case',
                    values: [
                        {
                            type: DynamicFieldType.Select,
                            name: "digitalMonitoringUnit",
                            label: "Digital Monitoring Unit",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "digitalLeakCheckingUnit",
                            label: "Digital Leak Checking Unit",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "digitalClearanceDevice",
                            label: "Digital Clearance Device",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "dryWipeBrush",
                            label: "Dry Wipe Brush",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "alcoholFreeWipes",
                            label: "Alcohol Free Wipes",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        }
                    ]
                },
                {
                    category: 'PPE',
                    values: [
                        {
                            type: DynamicFieldType.Select,
                            name: "steelCapBoots",
                            label: "Steel Cap Boots",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "hiVisShirt",
                            label: "Hi-Vis Shirt & Pants",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "gloves",
                            label: "Gloves",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "fullFaceRespirator",
                            label: "Full Face Respirator",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "respiratorCartridges",
                            label: "Respirator Cartridges",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "hat",
                            label: "Hat",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "wetWeatherRaincoat",
                            label: "Wet Weather Raincoat and Pants",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        }
                    ]
                },
            ]
        },

        {
            formType: 'Hygiene Service Form',
            formClass: FormClass.VehicleForm,
            formId: 'v-init-form',
            countryName: CountryName.Australia,
            config: [
                {
                    category: 'Vehicle Defaults',
                    values: [
                        {
                            type: DynamicFieldType.Input,
                            label: "Odometer Reading",
                            name: "odometer",
                            validations: [
                                {
                                    name: 'required',
                                    validator: Validators.required,
                                    message: 'This field is required!'
                                },
                                {
                                    name: 'pattern',
                                    validator: Validators.pattern(/^[0-9]+$/),
                                    message: 'Allow only numbers'
                                },
                            ],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Input,
                            label: "Service Due (KM)",
                            name: "serviceDue",
                            validations: [
                                {
                                    name: 'required',
                                    validator: Validators.required,
                                    message: 'This field is required!'
                                },
                                {
                                    name: 'pattern',
                                    validator: Validators.pattern(/^[0-9]+$/),
                                    message: 'Allow only numbers'
                                },
                            ],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Input,
                            label: "Make and Model",
                            name: "makeAndModel",
                            validations: [
                                {
                                    name: 'required',
                                    validator: Validators.required,
                                    message: 'This field is required!'
                                },
                            ],
                            value: this._authService.userVehicleDefaults?.makeAndModel
                        },
                        {
                            type: DynamicFieldType.Select,
                            label: "Vehicle Type",
                            name: "type",
                            options: [...this.vehicleTypes],
                            validations: [
                                {
                                    name: 'required',
                                    validator: Validators.required,
                                    message: 'This field is required!'
                                },
                            ],
                            value: this._authService.userVehicleDefaults?.type
                        },
                        {
                            type: DynamicFieldType.Select,
                            label: "Do you have a current driver licence?",
                            name: "isLicenseConsist",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [
                                {
                                    name: 'required',
                                    validator: Validators.required,
                                    message: 'This field is required!'
                                },
                            ],
                            value: ''
                        },
                        {
                            type: DynamicFieldType.Date,
                            label: "Drivers Licence Expiry Date",
                            name: "driversLicenceExpiryDate",
                            validations: [
                                {
                                    name: 'required',
                                    validator: Validators.required,
                                    message: 'This field is required!'
                                },
                            ],
                            value: this._authService.userVehicleDefaults?.licenceExpiry?.toDate()
                        },
                        {
                            type: DynamicFieldType.Input,
                            label: "Vehicle Registration",
                            name: "registration",
                            validations: [
                                {
                                    name: 'required',
                                    validator: Validators.required,
                                    message: 'This field is required!'
                                },
                                {
                                    name: 'pattern',
                                    validator: Validators.pattern(/^[a-z0-9]+$/i),
                                    message: 'Registration can only consist from letters or numbers'
                                },
                            ],
                            value: this._authService.userVehicleDefaults?.registration
                        },
                    ]
                },
                {
                    category: 'Tyres',
                    values: [
                        {
                            type: DynamicFieldType.Select,
                            label: "Tyre pressure",
                            name: "tyrePressure",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            label: "Tread depth",
                            name: "treadDepth",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "goodCondition",
                            label: "Tyres in good condition",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "spareServiceable",
                            label: "Spare serviceable",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                    ]
                },
                {
                    category: 'Brakes',
                    values: [
                        {
                            type: DynamicFieldType.Select,
                            name: "brakingPower",
                            label: "Braking Power",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "handBrake",
                            label: "Hand Brake",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        }
                    ]
                },
                {
                    category: 'Seat Belts',
                    values: [
                        {
                            type: DynamicFieldType.Select,
                            name: "tensionerFunctions",
                            label: "Tensioner Functions",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "beltsWork",
                            label: "Belts work and are in good condition",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        }
                    ]
                },
                {
                    category: 'Wipers',
                    values: [
                        {
                            type: DynamicFieldType.Select,
                            name: "wipersFunction",
                            label: "Wipers function",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "washerFluidLevel",
                            label: "Washer fluid level",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        }
                    ]
                },
                {
                    category: 'Windows & Windscreen',
                    values: [
                        {
                            type: DynamicFieldType.Select,
                            name: "windscreen",
                            label: "Windscreen",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "windows",
                            label: "Windows",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                    ]
                },
                {
                    category: 'Lights',
                    values: [
                        {
                            type: DynamicFieldType.Select,
                            name: "numberPlate",
                            label: "Number Plate",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "highBeam",
                            label: "High Beam",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "lowBeam",
                            label: "Low Beam",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "parking",
                            label: "Parking",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "reverse",
                            label: "Reverse",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "brakes",
                            label: "Brakes",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                    ]
                },
                {
                    category: 'Indicators',
                    values: [
                        {
                            type: DynamicFieldType.Select,
                            name: "indicatorFront",
                            label: "Front",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "indicatorSide",
                            label: "Side",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "indicatorRear",
                            label: "Rear",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "indicatorHazards",
                            label: "Hazards",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                    ]
                },
                {
                    category: 'Mirrors',
                    values: [
                        {
                            type: DynamicFieldType.Select,
                            name: "rearSideMirror",
                            label: "Rear/Side Mirrors",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "fishEyeSpotter",
                            label: 'Fish Eye Spotter',
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        }
                    ]
                },
                {
                    category: 'Maintenance',
                    values: [
                        {
                            type: DynamicFieldType.Select,
                            name: "engineStarts",
                            label: 'Engine Starts 1st Time',
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "oilLevel",
                            label: 'Oil Levels',
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "waterLevel",
                            label: 'Water Levels',
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "brakeFluidLevels",
                            label: 'Brake Fluid Levels',
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "airConditioning",
                            label: 'Air Conditioning',
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "gpsSticker",
                            label: 'GPS Sticker',
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "heightLevel",
                            label: 'Height Label',
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "doorFunction",
                            label: 'Door Functioning',
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "strutsRams",
                            label: 'Struts and Rams',
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "reverseBeeper",
                            label: 'Reverse Beeper',
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "reverseCameraScreen",
                            label: 'Reverse Camera Screen',
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "reverseCameraCameraLens",
                            label: 'Reverse Camera Lens',
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "horn",
                            label: 'Horn',
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "isVehicleClean",
                            label: 'Vehicle Clean',
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "accidentDamage",
                            label: 'Is the vehicle free from damage?',
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "decals",
                            label: 'Decals',
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        }
                    ]
                },
                {
                    category: 'Other',
                    values: [
                        {
                            type: DynamicFieldType.Select,
                            name: "tailgate",
                            label: 'Tailgate',
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                    ]
                },
                {
                    category: 'General Items',
                    values: [
                        {
                            type: DynamicFieldType.Select,
                            name: "stickers",
                            label: "Take 5 Stickers",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "manualHandbook",
                            label: "Manual Handbook",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "fireExtinguisher",
                            label: "Fire Extinguisher",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "extinguisherServiced",
                            label: "Extinguisher Serviced",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "extinguisherAccessible",
                            label: "Extinguisher Accessible",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "firstAidKit",
                            label: "First Aid Kit (Missing Items Listed in Comments)",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "handSanitiser",
                            label: "Hand Sanitiser",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "sdsFolder",
                            label: "SDS Folder",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        }
                    ]
                },
                {
                    category: 'PPE',
                    values: [
                        {
                            type: DynamicFieldType.Select,
                            name: "disposableGloves",
                            label: "Disposable Gloves",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "dustMask",
                            label: "Dust Mask",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "earMuffs",
                            label: "Ear Muffs",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "safetyBoots",
                            label: "Safety Boots",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "safetyVest",
                            label: "Safety Vest",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "hardHat",
                            label: "Hardhat",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "spareContainer",
                            label: "SpareContainer",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "resistantGloves",
                            label: "Needle Resistant Gloves",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "safetyGlasses",
                            label: "Safety Glasses / Goggles",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "Kneepads",
                            label: "Kneepads",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "spillKit",
                            label: "Spill Kit Stocked",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                    ]
                },
                {
                    category: 'Equipment',
                    values: [
                        {
                            type: DynamicFieldType.Select,
                            name: "serviceTrolley",
                            label: "Service Trolley",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "largeLadder",
                            label: "Large Ladder",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "cordlessHammerdrill",
                            label: "Cordless Hammerdrill",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "extensionCord",
                            label: "Extension Cord",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "currentSaferySwitch",
                            label: "Current Safety Switch and RCD Unit",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "toolTray",
                            label: "Tool Tray / Tool Box",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "studFinder",
                            label: "Stud Finder Or Metal Detector",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "serviceKeys",
                            label: "Service Keys",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "vehicleRamps",
                            label: "Vehicle Ramps",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "binSupportBrace",
                            label: "Bin Support Brace",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "smallServiceTrolleyBag",
                            label: "Small Service Trolley Bag",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "whelieAndSulo",
                            label: "Wheelie And Sulo Bin",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        }
                    ]
                },
            ]
        },

        {
            formType: 'Non Service Form',
            formClass: FormClass.VehicleForm,
            formId: 'v-ns-form',
            countryName: CountryName.Australia,
            config: [
                {
                    category: 'Vehicle Defaults',
                    values: [
                        {
                            type: DynamicFieldType.Input,
                            label: "Odometer Reading",
                            name: "odometer",
                            validations: [
                                {
                                    name: 'required',
                                    validator: Validators.required,
                                    message: 'This field is required!'
                                },
                                {
                                    name: 'pattern',
                                    validator: Validators.pattern(/^[0-9]+$/),
                                    message: 'Allow only numbers'
                                },
                            ],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Input,
                            label: "Service Due (KM)",
                            name: "serviceDue",
                            validations: [
                                {
                                    name: 'required',
                                    validator: Validators.required,
                                    message: 'This field is required!'
                                },
                                {
                                    name: 'pattern',
                                    validator: Validators.pattern(/^[0-9]+$/),
                                    message: 'Allow only numbers'
                                },
                            ],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Input,
                            label: "Make and Model",
                            name: "makeAndModel",
                            validations: [
                                {
                                    name: 'required',
                                    validator: Validators.required,
                                    message: 'This field is required!'
                                },
                            ],
                            value: this._authService.userVehicleDefaults?.makeAndModel
                        },
                        {
                            type: DynamicFieldType.Select,
                            label: "Vehicle Type",
                            name: "type",
                            options: [...this.vehicleTypes],
                            validations: [
                                {
                                    name: 'required',
                                    validator: Validators.required,
                                    message: 'This field is required!'
                                },
                            ],
                            value: this._authService.userVehicleDefaults?.type
                        },
                        {
                            type: DynamicFieldType.Select,
                            label: "Do you have a current driver licence?",
                            name: "isLicenseConsist",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [
                                {
                                    name: 'required',
                                    validator: Validators.required,
                                    message: 'This field is required!'
                                },
                            ],
                            value: ''
                        },
                        {
                            type: DynamicFieldType.Date,
                            label: "Drivers Licence Expiry Date",
                            name: "driversLicenceExpiryDate",
                            validations: [
                                {
                                    name: 'required',
                                    validator: Validators.required,
                                    message: 'This field is required!'
                                },
                            ],
                            value: this._authService.userVehicleDefaults?.licenceExpiry?.toDate()
                        },
                        {
                            type: DynamicFieldType.Input,
                            label: "Vehicle Registration",
                            name: "registration",
                            validations: [
                                {
                                    name: 'required',
                                    validator: Validators.required,
                                    message: 'This field is required!'
                                },
                                {
                                    name: 'pattern',
                                    validator: Validators.pattern(/^[a-z0-9]+$/i),
                                    message: 'Registration can only consist from letters or numbers'
                                },
                            ],
                            value: this._authService.userVehicleDefaults?.registration
                        },
                    ]
                },
                {
                    category: 'Tyres',
                    values: [
                        {
                            type: DynamicFieldType.Select,
                            label: "Tyre pressure",
                            name: "tyrePressure",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            label: "Tread depth",
                            name: "treadDepth",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "goodCondition",
                            label: "Tyres in good condition",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "spareServiceable",
                            label: "Spare serviceable",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                    ]
                },
                {
                    category: 'Brakes',
                    values: [
                        {
                            type: DynamicFieldType.Select,
                            name: "brakingPower",
                            label: "Braking Power",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "handBrake",
                            label: "Hand Brake",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        }
                    ]
                },
                {
                    category: 'Seat Belts',
                    values: [
                        {
                            type: DynamicFieldType.Select,
                            name: "tensionerFunctions",
                            label: "Tensioner Functions",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "beltsWork",
                            label: "Belts work and are in good condition",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        }
                    ]
                },
                {
                    category: 'Wipers',
                    values: [
                        {
                            type: DynamicFieldType.Select,
                            name: "wipersFunction",
                            label: "Wipers function",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "washerFluidLevel",
                            label: "Washer fluid level",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        }
                    ]
                },
                {
                    category: 'Windows & Windscreen',
                    values: [
                        {
                            type: DynamicFieldType.Select,
                            name: "windscreen",
                            label: "Windscreen",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "windows",
                            label: "Windows",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                    ]
                },
                {
                    category: 'Lights',
                    values: [
                        {
                            type: DynamicFieldType.Select,
                            name: "numberPlate",
                            label: "Number Plate",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "highBeam",
                            label: "High Beam",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "lowBeam",
                            label: "Low Beam",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "parking",
                            label: "Parking",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "reverse",
                            label: "Reverse",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "brakes",
                            label: "Brakes",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                    ]
                },
                {
                    category: 'Indicators',
                    values: [
                        {
                            type: DynamicFieldType.Select,
                            name: "indicatorFront",
                            label: "Front",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "indicatorSide",
                            label: "Side",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "indicatorRear",
                            label: "Rear",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "indicatorHazards",
                            label: "Hazards",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                    ]
                },
                {
                    category: 'Mirrors',
                    values: [
                        {
                            type: DynamicFieldType.Select,
                            name: "rearSideMirror",
                            label: "Rear/Side Mirrors",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        }
                    ]
                },
                {
                    category: 'Maintenance',
                    values: [
                        {
                            type: DynamicFieldType.Select,
                            name: "engineStarts",
                            label: 'Engine Starts 1st Time',
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "oilLevel",
                            label: 'Oil Levels',
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "waterLevel",
                            label: 'Water Levels',
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "brakeFluidLevels",
                            label: 'Brake Fluid Levels',
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "airConditioning",
                            label: 'Air Conditioning',
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "doorFunction",
                            label: 'Door Functioning',
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "strutsRams",
                            label: 'Struts and Rams',
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "reverseBeeper",
                            label: 'Reverse Beeper',
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "reverseCameraScreen",
                            label: 'Reverse Camera Screen',
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "reverseCameraCameraLens",
                            label: 'Reverse Camera Lens',
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "horn",
                            label: 'Horn',
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "isVehicleClean",
                            label: 'Vehicle Clean',
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "accidentDamage",
                            label: 'Is the vehicle free from damage?',
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "decals",
                            label: 'Decals',
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No, FormAnswerClassic.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        }
                    ]
                },
                {
                    category: 'General Items',
                    values: [
                        {
                            type: DynamicFieldType.Select,
                            name: "stickers",
                            label: "Take 5 Stickers",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "manualHandbook",
                            label: "Manual Handbook",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "firstAidKit",
                            label: "First Aid Kit (Missing Items Listed in Comments)",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "handSanitiser",
                            label: "Hand Sanitiser",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "sdsFolder",
                            label: "SDS Folder",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                    ]
                }
            ]
        },

        {
            formType: 'Pest Service Form',
            formClass: FormClass.VehicleForm,
            formId: 'v-pest-form',
            countryName: CountryName.Australia,
            config: [
                {
                    category: 'Vehicle Defaults',
                    values: [
                        {
                            type: DynamicFieldType.Input,
                            label: "Odometer Reading",
                            name: "odometer",
                            validations: [
                                {
                                    name: 'required',
                                    validator: Validators.required,
                                    message: 'This field is required!'
                                },
                                {
                                    name: 'pattern',
                                    validator: Validators.pattern(/^[0-9]+$/),
                                    message: 'Allow only numbers'
                                },
                            ],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Input,
                            label: "Service Due (KM)",
                            name: "serviceDue",
                            validations: [
                                {
                                    name: 'required',
                                    validator: Validators.required,
                                    message: 'This field is required!'
                                },
                                {
                                    name: 'pattern',
                                    validator: Validators.pattern(/^[0-9]+$/),
                                    message: 'Allow only numbers'
                                },
                            ],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Input,
                            label: "Make and Model",
                            name: "makeAndModel",
                            validations: [
                                {
                                    name: 'required',
                                    validator: Validators.required,
                                    message: 'This field is required!'
                                },
                            ],
                            value: this._authService.userVehicleDefaults?.makeAndModel
                        },
                        {
                            type: DynamicFieldType.Select,
                            label: "Vehicle Type",
                            name: "type",
                            options: [...this.vehicleTypes],
                            validations: [
                                {
                                    name: 'required',
                                    validator: Validators.required,
                                    message: 'This field is required!'
                                },
                            ],
                            value: this._authService.userVehicleDefaults?.type
                        },
                        {
                            type: DynamicFieldType.Select,
                            label: "Do you have a current driver licence?",
                            name: "isLicenseConsist",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [
                                {
                                    name: 'required',
                                    validator: Validators.required,
                                    message: 'This field is required!'
                                },
                            ],
                            value: ''
                        },
                        {
                            type: DynamicFieldType.Date,
                            label: "Drivers Licence Expiry Date",
                            name: "driversLicenceExpiryDate",
                            validations: [
                                {
                                    name: 'required',
                                    validator: Validators.required,
                                    message: 'This field is required!'
                                },
                            ],
                            value: this._authService.userVehicleDefaults?.licenceExpiry?.toDate()
                        },
                        {
                            type: DynamicFieldType.Input,
                            label: "Vehicle Registration",
                            name: "registration",
                            validations: [
                                {
                                    name: 'required',
                                    validator: Validators.required,
                                    message: 'This field is required!'
                                },
                                {
                                    name: 'pattern',
                                    validator: Validators.pattern(/^[a-z0-9]+$/i),
                                    message: 'Registration can only consist from letters or numbers'
                                },
                            ],
                            value: this._authService.userVehicleDefaults?.registration
                        },
                    ]
                },
                {
                    category: 'Tyres',
                    values: [
                        {
                            type: DynamicFieldType.Select,
                            label: "Tyre pressure",
                            name: "tyrePressure",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            label: "Tread depth",
                            name: "treadDepth",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "goodCondition",
                            label: "Tyres in good condition",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "spareServiceable",
                            label: "Spare serviceable",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                    ]
                },
                {
                    category: 'Brakes',
                    values: [
                        {
                            type: DynamicFieldType.Select,
                            name: "brakingPower",
                            label: "Braking Power",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "handBrake",
                            label: "Hand Brake",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        }
                    ]
                },
                {
                    category: 'Seat Belts',
                    values: [
                        {
                            type: DynamicFieldType.Select,
                            name: "tensionerFunctions",
                            label: "Tensioner Functions",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "beltsWork",
                            label: "Belts work and are in good condition",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        }
                    ]
                },
                {
                    category: 'Wipers',
                    values: [
                        {
                            type: DynamicFieldType.Select,
                            name: "wipersFunction",
                            label: "Wipers function",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "washerFluidLevel",
                            label: "Washer fluid level",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        }
                    ]
                },
                {
                    category: 'Windows & Windscreen',
                    values: [
                        {
                            type: DynamicFieldType.Select,
                            name: "windscreen",
                            label: "Windscreen",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "windows",
                            label: "Windows",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                    ]
                },
                {
                    category: 'Lights',
                    values: [
                        {
                            type: DynamicFieldType.Select,
                            name: "numberPlate",
                            label: "Number Plate",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "highBeam",
                            label: "High Beam",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "lowBeam",
                            label: "Low Beam",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "parking",
                            label: "Parking",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "reverse",
                            label: "Reverse",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "brakes",
                            label: "Brakes",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                    ]
                },
                {
                    category: 'Indicators',
                    values: [
                        {
                            type: DynamicFieldType.Select,
                            name: "indicatorFront",
                            label: "Front",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "indicatorSide",
                            label: "Side",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "indicatorRear",
                            label: "Rear",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "indicatorHazards",
                            label: "Hazards",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                    ]
                },
                {
                    category: 'Mirrors',
                    values: [
                        {
                            type: DynamicFieldType.Select,
                            name: "rearSideMirror",
                            label: "Rear/Side Mirrors",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        }
                    ]
                },
                {
                    category: 'Maintenance',
                    values: [
                        {
                            type: DynamicFieldType.Select,
                            name: "engineStarts",
                            label: 'Engine Starts 1st Time',
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "oilLevel",
                            label: 'Oil Levels',
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "waterLevel",
                            label: 'Water Levels',
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "brakeFluidLevels",
                            label: 'Brake Fluid Levels',
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "airConditioning",
                            label: 'Air Conditioning',
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "gpsSticker",
                            label: 'GPS Sticker',
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "heightLevel",
                            label: 'Height Label',
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "doorFunction",
                            label: 'Door Functioning',
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "strutsRams",
                            label: 'Struts and Rams',
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "reverseBeeper",
                            label: 'Reverse Beeper',
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "reverseCameraScreen",
                            label: 'Reverse Camera Screen',
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "reverseCameraCameraLens",
                            label: 'Reverse Camera Lens',
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "horn",
                            label: 'Horn',
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "isVehicleClean",
                            label: 'Vehicle Clean',
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "accidentDamage",
                            label: 'Is the vehicle free from damage?',
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "decals",
                            label: 'Decals',
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        }
                    ]
                },
                {
                    category: 'Other',
                    values: [
                        {
                            type: DynamicFieldType.Select,
                            name: "tankUnitFixed",
                            label: 'Tank Unit Fixed',
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "spillTrayFitted",
                            label: 'Spill Tray Fitted',
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "hoseConnectorsSecure",
                            label: 'Hose Connectors Secure',
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                    ]
                },
                {
                    category: 'General Items',
                    values: [
                        {
                            type: DynamicFieldType.Select,
                            name: "stickers",
                            label: "Take 5 Stickers",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "manualHandbook",
                            label: "Manual Handbook",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "fireExtinguisher",
                            label: "Fire Extinguisher",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "extinguisherServiced",
                            label: "Extinguisher Serviced",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "extinguisherAccessible",
                            label: "Extinguisher Accessible",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "firstAidKit",
                            label: "First Aid Kit (Missing Items Listed in Comments)",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "handSanitiser",
                            label: "Hand Sanitiser",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "sdsFolder",
                            label: "SDS Folder",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        }
                    ]
                },
                {
                    category: 'PPE',
                    values: [
                        {
                            type: DynamicFieldType.Select,
                            name: "pvcGloves",
                            label: "PVC Gloves",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "latexGloves",
                            label: "Latex Gloves",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "earMuffs",
                            label: "Ear Muffs ",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "safetyGlasses",
                            label: "Safety Glasses",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "faceShield",
                            label: "Face Shield",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "safetyBoots",
                            label: "Safety Boots",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "safetyVest",
                            label: "Safety Vest",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "hardHat",
                            label: "Hard Hat / Bump Cap",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "spareUniform",
                            label: "Spare Uniform",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "beeHood",
                            label: "Bee Hood",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "insectRepellent",
                            label: "Insect Repellent",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "overalls",
                            label: "Overalls (Two Pairs)",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "kneePads",
                            label: "Knee Pads",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "subfloorEntrySign",
                            label: "Subfloor Entry Sign",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "paperTowelDispensor",
                            label: "Paper Towel Dispensor ",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "sunscreen",
                            label: "Sunscreen 30+",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "spareSafetyBoots",
                            label: "Spare Safety Boots",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "faceMaskAndRespirator",
                            label: "Face Mask And Respirator",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "faceMaskCartridges",
                            label: "Face Mask Cartridges",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        }
                    ]
                },
                {
                    category: 'Spill Kit',
                    values: [
                        {
                            type: DynamicFieldType.Select,
                            name: "spillKitStocked",
                            label: 'Spill Kit Stocked',
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        }
                    ]
                },
                {
                    category: 'Dangerous Goods',
                    values: [
                        {
                            type: DynamicFieldType.Select,
                            name: "dangerousGoodsStoredCorrectly",
                            label: 'Dangerous Goods Stored Correctly',
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "chemicalTreatmentSigns",
                            label: 'Chemical Treatment Signs',
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                    ]
                },
                {
                    category: 'Other',
                    values: [
                        {
                            type: DynamicFieldType.Select,
                            name: "extensionLadder",
                            label: 'Extension Ladder 2.4m',
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "ladder",
                            label: 'Ladder 1.8m',
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "rcdSafetySwitch",
                            label: 'RCD Safety Switch',
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "extensionCord",
                            label: 'Extension Cord',
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "waterBottle",
                            label: '20L Water Bottle',
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "soapDispenser",
                            label: 'Soap Dispenser',
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                    ]
                },
            ]
        },

        // New Zealand Forms

        {
            formType: 'Ambius Service Form',
            formClass: FormClass.VehicleForm,
            formId: 'v-ambi-form',
            countryName: CountryName.NewZealand,
            config: [
                {
                    category: 'Vehicle Defaults',
                    values: [
                        {
                            type: DynamicFieldType.Input,
                            label: "Odometer Reading",
                            name: "odometer",
                            validations: [
                                {
                                    name: 'required',
                                    validator: Validators.required,
                                    message: 'This field is required!'
                                },
                                {
                                    name: 'pattern',
                                    validator: Validators.pattern(/^[0-9]+$/),
                                    message: 'Allow only numbers'
                                },
                            ],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Input,
                            label: "Service Due (KM)",
                            name: "serviceDue",
                            validations: [
                                {
                                    name: 'required',
                                    validator: Validators.required,
                                    message: 'This field is required!'
                                },
                                {
                                    name: 'pattern',
                                    validator: Validators.pattern(/^[0-9]+$/),
                                    message: 'Allow only numbers'
                                },
                            ],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Input,
                            label: "Make and Model",
                            name: "makeAndModel",
                            validations: [
                                {
                                    name: 'required',
                                    validator: Validators.required,
                                    message: 'This field is required!'
                                },
                            ],
                            value: this._authService.userVehicleDefaults?.makeAndModel
                        },
                        {
                            type: DynamicFieldType.Select,
                            label: "Vehicle Type",
                            name: "type",
                            options: [...this.vehicleTypes],
                            validations: [
                                {
                                    name: 'required',
                                    validator: Validators.required,
                                    message: 'This field is required!'
                                },
                            ],
                            value: this._authService.userVehicleDefaults?.type
                        },
                        {
                            type: DynamicFieldType.Select,
                            label: "Do you have a current driver licence?",
                            name: "isLicenseConsist",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [
                                {
                                    name: 'required',
                                    validator: Validators.required,
                                    message: 'This field is required!'
                                },
                            ],
                            value: ''
                        },
                        {
                            type: DynamicFieldType.Date,
                            label: "Drivers Licence Expiry Date",
                            name: "driversLicenceExpiryDate",
                            validations: [
                                {
                                    name: 'required',
                                    validator: Validators.required,
                                    message: 'This field is required!'
                                },
                            ],
                            value: this._authService.userVehicleDefaults?.licenceExpiry?.toDate()
                        },
                        {
                            type: DynamicFieldType.Input,
                            label: "Vehicle Registration",
                            name: "registration",
                            validations: [
                                {
                                    name: 'required',
                                    validator: Validators.required,
                                    message: 'This field is required!'
                                },
                                {
                                    name: 'pattern',
                                    validator: Validators.pattern(/^[a-z0-9]+$/i),
                                    message: 'Registration can only consist from letters or numbers'
                                },
                            ],
                            value: this._authService.userVehicleDefaults?.registration
                        },
                        {
                            type: DynamicFieldType.Date,
                            label: "WOF/COF Due",
                            name: "wofCofDue",
                            validations: [
                                {
                                    name: 'required',
                                    validator: Validators.required,
                                    message: 'This field is required!'
                                },
                            ],
                            value: ''
                        },
                        {
                            type: DynamicFieldType.Date,
                            label: "Registration Due",
                            name: "regDue",
                            validations: [
                                {
                                    name: 'required',
                                    validator: Validators.required,
                                    message: 'Registrtion Due is required!'
                                },
                            ],
                            value: ''
                        },
                        {
                            type: DynamicFieldType.Input,
                            label: "RUC/Road User Charges",
                            name: "rucRoadUserCharges",
                            validations: [
                                {
                                    name: 'required',
                                    validator: Validators.required,
                                    message: 'Road user charges is required!'
                                },
                                {
                                    name: 'pattern',
                                    validator: Validators.pattern(/^[a-z0-9]+$/i),
                                    message: 'Road user charges allow only numbers from 1 to 6 digits'
                                },
                            ],
                            value: ''
                        },
                    ]
                },
                {
                    category: 'Tyres',
                    values: [
                        {
                            type: DynamicFieldType.Select,
                            label: "Tyre Pressure",
                            name: "tyrePressure",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            label: "Tread Depth",
                            name: "treadDepth",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "goodCondition",
                            label: "Tyres in Good Condition",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "spareServiceable",
                            label: "Spare Serviceable",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                    ]
                },
                {
                    category: 'Brakes',
                    values: [
                        {
                            type: DynamicFieldType.Select,
                            name: "brakingPower",
                            label: "Braking Power",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "handBrake",
                            label: "Hand Brake",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        }
                    ]
                },
                {
                    category: 'Seat Belts',
                    values: [
                        {
                            type: DynamicFieldType.Select,
                            name: "tensionerFunctions",
                            label: "Belt Tensioner Functions",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "beltsWork",
                            label: "Belts work and are in good condition",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        }
                    ]
                },
                {
                    category: 'Wipers',
                    values: [
                        {
                            type: DynamicFieldType.Select,
                            name: "wipersFunction",
                            label: "Wipers Function",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "washerFluidLevel",
                            label: "Washer Fluid Level is High",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        }
                    ]
                },
                {
                    category: 'Windows & Windscreen',
                    values: [
                        {
                            type: DynamicFieldType.Select,
                            name: "windscreen",
                            label: "Windscreen",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "windows",
                            label: "Windows",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                    ]
                },
                {
                    category: 'Lights',
                    values: [
                        {
                            type: DynamicFieldType.Select,
                            name: "numberPlate",
                            label: "Number Plate",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "highBeam",
                            label: "High Beam",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "lowBeam",
                            label: "Low Beam",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "parking",
                            label: "Parking",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "reverse",
                            label: "Reverse",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "brakes",
                            label: "Brakes",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                    ]
                },
                {
                    category: 'Indicators',
                    values: [
                        {
                            type: DynamicFieldType.Select,
                            name: "indicatorFront",
                            label: "Front",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "indicatorSide",
                            label: "Side",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "indicatorRear",
                            label: "Rear",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "indicatorHazards",
                            label: "Hazards",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                    ]
                },
                {
                    category: 'Mirrors',
                    values: [
                        {
                            type: DynamicFieldType.Select,
                            name: "rearSideMirror",
                            label: "Rear/Side Mirrors",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "fishEyeSpotter",
                            label: 'Fish Eye Spotter',
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        }
                    ]
                },
                {
                    category: 'Maintenance',
                    values: [
                        {
                            type: DynamicFieldType.Select,
                            name: "engineStarts",
                            label: 'Engine Starts 1st Time',
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "oilLevel",
                            label: 'Oil Levels',
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "waterLevel",
                            label: 'Water Levels',
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "brakeFluidLevels",
                            label: 'Brake Fluid Levels',
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "airConditioning",
                            label: 'Air Conditioning',
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "gpsSticker",
                            label: 'GPS Sticker',
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "heightLevel",
                            label: 'Height Label',
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "doorFunction",
                            label: 'Doors Function Properly',
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "strutsRams",
                            label: 'Struts and Rams',
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "reverseBeeper",
                            label: 'Reverse Beeper',
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "reverseCameraScreen",
                            label: 'Reverse Camera Screen',
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "reverseCameraCameraLens",
                            label: 'Reverse Camera Lens',
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "horn",
                            label: 'Horn',
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "isVehicleClean",
                            label: 'Vehicle Clean',
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "accidentDamage",
                            label: 'Is the vehicle free from damage?',
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "decals",
                            label: 'Decals',
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        }
                    ]
                },
                {
                    category: 'Other',
                    values: [
                        {
                            type: DynamicFieldType.Select,
                            name: "tailgate",
                            label: 'Tailgate',
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                    ]
                },
                {
                    category: 'General Items',
                    values: [
                        {
                            type: DynamicFieldType.Select,
                            name: "stickers",
                            label: "Have Take 5 Stickers",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "manualHandbook",
                            label: "Manual Handbook",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "fireExtinguisher",
                            label: "Fire Extinguisher",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "extinguisherServiced",
                            label: "Extinguisher Serviced",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "extinguisherAccessible",
                            label: "Extinguisher Accessible",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "firstAidKit",
                            label: "First Aid Kit",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "handSanitiser",
                            label: "Hand Sanitiser",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "sdsFolder",
                            label: "SDS Folder",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        }
                    ]
                },
                {
                    category: 'Spill Kit',
                    values: [
                        {
                            type: DynamicFieldType.Select,
                            name: "absorbentGranules",
                            label: "Absorbent Granules",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "glovesAndGlassesAndMask",
                            label: "PPE - Gloves, Safety Glasses, Dust Mask",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "dustPanAndBrush",
                            label: "Dust Pan & Brush",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "tongs",
                            label: "Tongs",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "bucketWithLid",
                            label: "Bucket with lid and heavy duty plastic bag",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "sharpsCleanUpKit",
                            label: "Sharps clean up kit",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "mop",
                            label: "Mop",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        }
                    ]
                },
                {
                    category: 'Other Equipment',
                    values: [
                        {
                            type: DynamicFieldType.Select,
                            name: "trolley",
                            label: "Trolley",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "stepLadder",
                            label: "Step Ladder (1.8m Industrial Grade)",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "technicalKitBag",
                            label: "Technician Kit Bag",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "bucketAndWateringCan",
                            label: "Bucket and Watering Can",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "secateursAndLeatherSheaf",
                            label: "Secateurs and Leather Sheaf",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "cleaningCloths",
                            label: "Cleaning Cloths (Sponges and Chux)",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "sprayBottles",
                            label: "Spray Bottles (2 x 1L)",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "chemicalsLabelled",
                            label: "All Chemicals Labelled with contents",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "soilMoisture",
                            label: "Soil Moisture Testing Probe",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "waterPipette",
                            label: "Water Measuring Pipette",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "wireTie",
                            label: "Wire Tie",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "heavyDutyPlasticBug",
                            label: "Heavy Duty Plastic Bag",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "yellowWarningSun",
                            label: "Yellow Warning Sign",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        }
                    ]
                },
            ]
        },

        {
            formType: 'Fumigation Service Form',
            formClass: FormClass.VehicleForm,
            formId: 'v-fumi-form',
            countryName: CountryName.NewZealand,
            config: [
                {
                    category: 'Vehicle Defaults',
                    values: [
                        {
                            type: DynamicFieldType.Input,
                            label: "Odometer Reading",
                            name: "odometer",
                            validations: [
                                {
                                    name: 'required',
                                    validator: Validators.required,
                                    message: 'This field is required!'
                                },
                                {
                                    name: 'pattern',
                                    validator: Validators.pattern(/^[0-9]+$/),
                                    message: 'Allow only numbers'
                                },
                            ],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Input,
                            label: "Service Due (KM)",
                            name: "serviceDue",
                            validations: [
                                {
                                    name: 'required',
                                    validator: Validators.required,
                                    message: 'This field is required!'
                                },
                                {
                                    name: 'pattern',
                                    validator: Validators.pattern(/^[0-9]+$/),
                                    message: 'Allow only numbers'
                                },
                            ],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Input,
                            label: "Make and Model",
                            name: "makeAndModel",
                            validations: [
                                {
                                    name: 'required',
                                    validator: Validators.required,
                                    message: 'This field is required!'
                                },
                            ],
                            value: this._authService.userVehicleDefaults?.makeAndModel
                        },
                        {
                            type: DynamicFieldType.Select,
                            label: "Vehicle Type",
                            name: "type",
                            options: [...this.vehicleTypes],
                            validations: [
                                {
                                    name: 'required',
                                    validator: Validators.required,
                                    message: 'This field is required!'
                                },
                            ],
                            value: this._authService.userVehicleDefaults?.type
                        },
                        {
                            type: DynamicFieldType.Select,
                            label: "Do you have a current driver licence?",
                            name: "isLicenseConsist",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [
                                {
                                    name: 'required',
                                    validator: Validators.required,
                                    message: 'This field is required!'
                                },
                            ],
                            value: ''
                        },
                        {
                            type: DynamicFieldType.Date,
                            label: "Drivers Licence Expiry Date",
                            name: "driversLicenceExpiryDate",
                            validations: [
                                {
                                    name: 'required',
                                    validator: Validators.required,
                                    message: 'This field is required!'
                                },
                            ],
                            value: this._authService.userVehicleDefaults?.licenceExpiry?.toDate()
                        },
                        {
                            type: DynamicFieldType.Input,
                            label: "Vehicle Registration",
                            name: "registration",
                            validations: [
                                {
                                    name: 'required',
                                    validator: Validators.required,
                                    message: 'This field is required!'
                                },
                                {
                                    name: 'pattern',
                                    validator: Validators.pattern(/^[a-z0-9]+$/i),
                                    message: 'Registration can only consist from letters or numbers'
                                },
                            ],
                            value: this._authService.userVehicleDefaults?.registration
                        },
                        {
                            type: DynamicFieldType.Date,
                            label: "WOF/COF Due",
                            name: "wofCofDue",
                            validations: [
                                {
                                    name: 'required',
                                    validator: Validators.required,
                                    message: 'This field is required!'
                                },
                            ],
                            value: ''
                        },
                        {
                            type: DynamicFieldType.Date,
                            label: "Registration Due",
                            name: "regDue",
                            validations: [
                                {
                                    name: 'required',
                                    validator: Validators.required,
                                    message: 'Registrtion Due is required!'
                                },
                            ],
                            value: ''
                        },
                        {
                            type: DynamicFieldType.Input,
                            label: "RUC/Road User Charges",
                            name: "rucRoadUserCharges",
                            validations: [
                                {
                                    name: 'required',
                                    validator: Validators.required,
                                    message: 'Road user charges is required!'
                                },
                                {
                                    name: 'pattern',
                                    validator: Validators.pattern(/^[a-z0-9]+$/i),
                                    message: 'Road user charges allow only numbers from 1 to 6 digits'
                                },
                            ],
                            value: ''
                        },
                    ]
                },
                {
                    category: 'Tyres',
                    values: [
                        {
                            type: DynamicFieldType.Select,
                            label: "Tyre pressure",
                            name: "tyrePressure",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            label: "Tread depth",
                            name: "treadDepth",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "goodCondition",
                            label: "Tyres in good condition",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "spareServiceable",
                            label: "Spare serviceable",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                    ]
                },
                {
                    category: 'Brakes',
                    values: [
                        {
                            type: DynamicFieldType.Select,
                            name: "brakingPower",
                            label: "Braking Power",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "handBrake",
                            label: "Hand Brake",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        }
                    ]
                },
                {
                    category: 'Seat Belts',
                    values: [
                        {
                            type: DynamicFieldType.Select,
                            name: "tensionerFunctions",
                            label: "Tensioner Functions",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "beltsWork",
                            label: "Belts work and are in good condition",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        }
                    ]
                },
                {
                    category: 'Wipers',
                    values: [
                        {
                            type: DynamicFieldType.Select,
                            name: "wipersFunction",
                            label: "Wipers function",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "washerFluidLevel",
                            label: "Washer fluid level",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        }
                    ]
                },
                {
                    category: 'Windows & Windscreen',
                    values: [
                        {
                            type: DynamicFieldType.Select,
                            name: "windscreen",
                            label: "Windscreen",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "windows",
                            label: "Windows",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                    ]
                },
                {
                    category: 'Lights',
                    values: [
                        {
                            type: DynamicFieldType.Select,
                            name: "numberPlate",
                            label: "Number Plate",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "highBeam",
                            label: "High Beam",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "lowBeam",
                            label: "Low Beam",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "parking",
                            label: "Parking",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "reverse",
                            label: "Reverse",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "brakes",
                            label: "Brakes",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                    ]
                },
                {
                    category: 'Indicators',
                    values: [
                        {
                            type: DynamicFieldType.Select,
                            name: "indicatorFront",
                            label: "Front",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "indicatorSide",
                            label: "Side",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "indicatorRear",
                            label: "Rear",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "indicatorHazards",
                            label: "Hazards",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                    ]
                },
                {
                    category: 'Mirrors',
                    values: [
                        {
                            type: DynamicFieldType.Select,
                            name: "rearSideMirror",
                            label: "Rear/Side Mirrors",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "fishEyeSpotter",
                            label: 'Fish Eye Spotter',
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        }
                    ]
                },
                {
                    category: 'Maintenance',
                    values: [
                        {
                            type: DynamicFieldType.Select,
                            name: "engineStarts",
                            label: 'Engine Starts 1st Time',
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "oilLevel",
                            label: 'Oil Levels',
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "waterLevel",
                            label: 'Water Levels',
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "brakeFluidLevels",
                            label: 'Brake Fluid Levels',
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "airConditioning",
                            label: 'Air Conditioning',
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "gpsSticker",
                            label: 'GPS Sticker',
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "heightLevel",
                            label: 'Height Label',
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "doorFunction",
                            label: 'Door Functioning',
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "strutsRams",
                            label: 'Struts and Rams',
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "horn",
                            label: 'Horn',
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "reverseCameraScreen",
                            label: 'Reverse Camera Screen',
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "reverseCameraCameraLens",
                            label: 'Reverse Camera Lens',
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "reverseBeeper",
                            label: 'Reverse Beeper',
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "isVehicleClean",
                            label: 'Vehicle Clean',
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "vehicleBodyCondition",
                            label: 'Vehicle Body Condition',
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "rearCompartment",
                            label: 'Rear Compartment',
                            options: ["Safe", "Not Safe"],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "decals",
                            label: 'Decals',
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "trayCover",
                            label: 'Tray Cover',
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "chassisRail",
                            label: 'Chassis Rail',
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        }
                    ]
                },
                {
                    category: 'General Items',
                    values: [
                        {
                            type: DynamicFieldType.Select,
                            name: "stickers",
                            label: "Take 5 Stickers",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "fireExtinguisher",
                            label: "Fire Extinguisher",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "extinguisherServiced",
                            label: "Extinguisher Serviced",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "extinguisherAccessible",
                            label: "Extinguisher Accessible",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "firstAidKit",
                            label: "First Aid Kit (Missing Items Listed in Comments)",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "sdsFolder",
                            label: "Safety Data Sheet (SDS) Folder",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "manualHandbook",
                            label: "Vehicle Manual & Drivers Handbook",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "handSanitiser",
                            label: "Hand Sanitiser",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "soapDispenser",
                            label: "Soap Dispenser (complete with spare soap)",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "fanHeaters",
                            label: "Fan & Heaters",
                            options: ["Yes", "No", "N/A"],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "extensionCord",
                            label: "Extension Cord",
                            options: ["Safe", "Not Safe"],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        }
                    ]
                },
                {
                    category: 'Manuals/Required Information',
                    values: [
                        {
                            type: DynamicFieldType.Select,
                            name: "fumigationTechnicalFolder",
                            label: "Fumigation Technical Folder",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "emergencyResponseInformation",
                            label: "Emergency Response Information (TeamCards) for chemicals being carried",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        }
                    ]
                },
                {
                    category: 'General Vehicle Equipment',
                    values: [
                        {
                            type: DynamicFieldType.Select,
                            name: "insectRepellent",
                            label: "Insect Repellent",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "approvedFumigationTape",
                            label: "Approved Fumigation Tape",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "fumigationWarningSigns",
                            label: "Fumigation Warning Signs",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "bollardsOrBunting",
                            label: "Bollards / Bunting",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "cableTies",
                            label: "Cable Ties",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "sunscreen",
                            label: "Sunscreen",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "waterBottle",
                            label: "20L Water Bottle (Isuzu Models only)",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                    ]
                },
                {
                    category: 'Dangerous Goods',
                    values: [
                        {
                            type: DynamicFieldType.Select,
                            name: "dangerousGoods",
                            label: "Dangerous Goods stored correctly",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "spillKit",
                            label: "Spill Kit",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        }
                    ]
                },
                {
                    category: 'Equipment',
                    values: [
                        {
                            type: DynamicFieldType.Select,
                            name: "marineHotWaterSystem",
                            label: "Marine Hot Water System",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "bottleRacks",
                            label: "Bottle Racks",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "tempRecordingDevices",
                            label: "Temp. Recording Devices",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "shiftingSpanner",
                            label: "Shifting Spanner",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "profumeSpanner",
                            label: "Profume Spanner",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "gasMonitoringLines",
                            label: "Gas / Monitoring Lines",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "safetyGlasses",
                            label: "SafetyGlasses",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "pvsMonitorExtensionTubes",
                            label: "PVC Monitor Extension Tubes",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "methylBromideDispenser",
                            label: "Methyl Bromide Dispenser",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "ispmStamps",
                            label: "ISPM Stamps",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "personalDetectionDevice",
                            label: "Personal Detection Device",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "ladder",
                            label: "Ladder",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        }
                    ]
                },
                {
                    category: 'Digital Equipment Case',
                    values: [
                        {
                            type: DynamicFieldType.Select,
                            name: "digitalMonitoringUnit",
                            label: "Digital Monitoring Unit",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "digitalLeakCheckingUnit",
                            label: "Digital Leak Checking Unit",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "digitalClearanceDevice",
                            label: "Digital Clearance Device",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "dryWipeBrush",
                            label: "Dry Wipe Brush",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "alcoholFreeWipes",
                            label: "Alcohol Free Wipes",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        }
                    ]
                },
                {
                    category: 'PPE',
                    values: [
                        {
                            type: DynamicFieldType.Select,
                            name: "steelCapBoots",
                            label: "Steel Cap Boots",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "hiVisShirt",
                            label: "Hi-Vis Shirt & Pants",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "gloves",
                            label: "Gloves",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "fullFaceRespirator",
                            label: "Full Face Respirator",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "respiratorCartridges",
                            label: "Respirator Cartridges",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "hat",
                            label: "Hat",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "wetWeatherRaincoat",
                            label: "Wet Weather Raincoat and Pants",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        }
                    ]
                },
            ]
        },

        {
            formType: 'Hygiene Service Form',
            formClass: FormClass.VehicleForm,
            formId: 'v-init-form',
            countryName: CountryName.NewZealand,
            config: [
                {
                    category: 'Vehicle Defaults',
                    values: [
                        {
                            type: DynamicFieldType.Input,
                            label: "Odometer Reading",
                            name: "odometer",
                            validations: [
                                {
                                    name: 'required',
                                    validator: Validators.required,
                                    message: 'This field is required!'
                                },
                                {
                                    name: 'pattern',
                                    validator: Validators.pattern(/^[0-9]+$/),
                                    message: 'Allow only numbers'
                                },
                            ],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Input,
                            label: "Service Due (KM)",
                            name: "serviceDue",
                            validations: [
                                {
                                    name: 'required',
                                    validator: Validators.required,
                                    message: 'This field is required!'
                                },
                                {
                                    name: 'pattern',
                                    validator: Validators.pattern(/^[0-9]+$/),
                                    message: 'Allow only numbers'
                                },
                            ],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Input,
                            label: "Make and Model",
                            name: "makeAndModel",
                            validations: [
                                {
                                    name: 'required',
                                    validator: Validators.required,
                                    message: 'This field is required!'
                                },
                            ],
                            value: this._authService.userVehicleDefaults?.makeAndModel
                        },
                        {
                            type: DynamicFieldType.Select,
                            label: "Vehicle Type",
                            name: "type",
                            options: [...this.vehicleTypes],
                            validations: [
                                {
                                    name: 'required',
                                    validator: Validators.required,
                                    message: 'This field is required!'
                                },
                            ],
                            value: this._authService.userVehicleDefaults?.type
                        },
                        {
                            type: DynamicFieldType.Select,
                            label: "Do you have a current driver licence?",
                            name: "isLicenseConsist",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [
                                {
                                    name: 'required',
                                    validator: Validators.required,
                                    message: 'This field is required!'
                                },
                            ],
                            value: ''
                        },
                        {
                            type: DynamicFieldType.Date,
                            label: "Drivers Licence Expiry Date",
                            name: "driversLicenceExpiryDate",
                            validations: [
                                {
                                    name: 'required',
                                    validator: Validators.required,
                                    message: 'This field is required!'
                                },
                            ],
                            value: this._authService.userVehicleDefaults?.licenceExpiry?.toDate()
                        },
                        {
                            type: DynamicFieldType.Input,
                            label: "Vehicle Registration",
                            name: "registration",
                            validations: [
                                {
                                    name: 'required',
                                    validator: Validators.required,
                                    message: 'This field is required!'
                                },
                                {
                                    name: 'pattern',
                                    validator: Validators.pattern(/^[a-z0-9]+$/i),
                                    message: 'Registration can only consist from letters or numbers'
                                },
                            ],
                            value: this._authService.userVehicleDefaults?.registration
                        },
                        {
                            type: DynamicFieldType.Date,
                            label: "WOF/COF Due",
                            name: "wofCofDue",
                            validations: [
                                {
                                    name: 'required',
                                    validator: Validators.required,
                                    message: 'This field is required!'
                                },
                            ],
                            value: ''
                        },
                        {
                            type: DynamicFieldType.Date,
                            label: "Registration Due",
                            name: "regDue",
                            validations: [
                                {
                                    name: 'required',
                                    validator: Validators.required,
                                    message: 'Registrtion Due is required!'
                                },
                            ],
                            value: ''
                        },
                        {
                            type: DynamicFieldType.Input,
                            label: "RUC/Road User Charges",
                            name: "rucRoadUserCharges",
                            validations: [
                                {
                                    name: 'required',
                                    validator: Validators.required,
                                    message: 'Road user charges is required!'
                                },
                                {
                                    name: 'pattern',
                                    validator: Validators.pattern(/^[a-z0-9]+$/i),
                                    message: 'Road user charges allow only numbers from 1 to 6 digits'
                                },
                            ],
                            value: ''
                        },
                    ]
                },
                {
                    category: 'Tyres',
                    values: [
                        {
                            type: DynamicFieldType.Select,
                            label: "Tyre pressure",
                            name: "tyrePressure",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            label: "Tread depth",
                            name: "treadDepth",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "goodCondition",
                            label: "Tyres in good condition",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "spareServiceable",
                            label: "Spare serviceable",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                    ]
                },
                {
                    category: 'Brakes',
                    values: [
                        {
                            type: DynamicFieldType.Select,
                            name: "brakingPower",
                            label: "Braking Power",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "handBrake",
                            label: "Hand Brake",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        }
                    ]
                },
                {
                    category: 'Seat Belts',
                    values: [
                        {
                            type: DynamicFieldType.Select,
                            name: "tensionerFunctions",
                            label: "Tensioner Functions",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "beltsWork",
                            label: "Belts work and are in good condition",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        }
                    ]
                },
                {
                    category: 'Wipers',
                    values: [
                        {
                            type: DynamicFieldType.Select,
                            name: "wipersFunction",
                            label: "Wipers function",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "washerFluidLevel",
                            label: "Washer fluid level",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        }
                    ]
                },
                {
                    category: 'Windows & Windscreen',
                    values: [
                        {
                            type: DynamicFieldType.Select,
                            name: "windscreen",
                            label: "Windscreen",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "windows",
                            label: "Windows",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                    ]
                },
                {
                    category: 'Lights',
                    values: [
                        {
                            type: DynamicFieldType.Select,
                            name: "numberPlate",
                            label: "Number Plate",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "highBeam",
                            label: "High Beam",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "lowBeam",
                            label: "Low Beam",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "parking",
                            label: "Parking",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "reverse",
                            label: "Reverse",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "brakes",
                            label: "Brakes",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                    ]
                },
                {
                    category: 'Indicators',
                    values: [
                        {
                            type: DynamicFieldType.Select,
                            name: "indicatorFront",
                            label: "Front",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "indicatorSide",
                            label: "Side",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "indicatorRear",
                            label: "Rear",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "indicatorHazards",
                            label: "Hazards",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                    ]
                },
                {
                    category: 'Mirrors',
                    values: [
                        {
                            type: DynamicFieldType.Select,
                            name: "rearSideMirror",
                            label: "Rear/Side Mirrors",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "fishEyeSpotter",
                            label: 'Fish Eye Spotter',
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        }
                    ]
                },
                {
                    category: 'Maintenance',
                    values: [
                        {
                            type: DynamicFieldType.Select,
                            name: "engineStarts",
                            label: 'Engine Starts 1st Time',
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "oilLevel",
                            label: 'Oil Levels',
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "waterLevel",
                            label: 'Water Levels',
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "brakeFluidLevels",
                            label: 'Brake Fluid Levels',
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "airConditioning",
                            label: 'Air Conditioning',
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "gpsSticker",
                            label: 'GPS Sticker',
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "heightLevel",
                            label: 'Height Label',
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "doorFunction",
                            label: 'Door Functioning',
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "strutsRams",
                            label: 'Struts and Rams',
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "reverseBeeper",
                            label: 'Reverse Beeper',
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "reverseCameraScreen",
                            label: 'Reverse Camera Screen',
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "reverseCameraCameraLens",
                            label: 'Reverse Camera Lens',
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "horn",
                            label: 'Horn',
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "isVehicleClean",
                            label: 'Vehicle Clean',
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "accidentDamage",
                            label: 'Is the vehicle free from damage?',
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "decals",
                            label: 'Decals',
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        }
                    ]
                },
                {
                    category: 'Other',
                    values: [
                        {
                            type: DynamicFieldType.Select,
                            name: "tailgate",
                            label: 'Tailgate',
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                    ]
                },
                {
                    category: 'General Items',
                    values: [
                        {
                            type: DynamicFieldType.Select,
                            name: "stickers",
                            label: "Take 5 Stickers",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "manualHandbook",
                            label: "Manual Handbook",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "fireExtinguisher",
                            label: "Fire Extinguisher",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "extinguisherServiced",
                            label: "Extinguisher Serviced",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "extinguisherAccessible",
                            label: "Extinguisher Accessible",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "firstAidKit",
                            label: "First Aid Kit (Missing Items Listed in Comments)",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "handSanitiser",
                            label: "Hand Sanitiser",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "sdsFolder",
                            label: "SDS Folder",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        }
                    ]
                },
                {
                    category: 'PPE',
                    values: [
                        {
                            type: DynamicFieldType.Select,
                            name: "disposableGloves",
                            label: "Disposable Gloves",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "dustMask",
                            label: "Dust Mask",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "earMuffs",
                            label: "Ear Muffs",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "safetyBoots",
                            label: "Safety Boots",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "safetyVest",
                            label: "Safety Vest",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "hardHat",
                            label: "Hardhat",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "spareContainer",
                            label: "SpareContainer",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "resistantGloves",
                            label: "Needle Resistant Gloves",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "safetyGlasses",
                            label: "Safety Glasses / Goggles",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "Kneepads",
                            label: "Kneepads",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "spillKit",
                            label: "Spill Kit Stocked",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                    ]
                },
                {
                    category: 'Equipment',
                    values: [
                        {
                            type: DynamicFieldType.Select,
                            name: "serviceTrolley",
                            label: "Service Trolley",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "largeLadder",
                            label: "Large Ladder",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "cordlessHammerdrill",
                            label: "Cordless Hammerdrill",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "extensionCord",
                            label: "Extension Cord",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "currentSaferySwitch",
                            label: "Current Safety Switch and RCD Unit",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "toolTray",
                            label: "Tool Tray / Tool Box",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "studFinder",
                            label: "Stud Finder Or Metal Detector",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "serviceKeys",
                            label: "Service Keys",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "vehicleRamps",
                            label: "Vehicle Ramps",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "binSupportBrace",
                            label: "Bin Support Brace",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "smallServiceTrolleyBag",
                            label: "Small Service Trolley Bag",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "whelieAndSulo",
                            label: "Wheelie And Sulo Bin",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        }
                    ]
                },
            ]
        },

        {
            formType: 'Non Service Form',
            formClass: FormClass.VehicleForm,
            formId: 'v-ns-form',
            countryName: CountryName.NewZealand,
            config: [
                {
                    category: 'Vehicle Defaults',
                    values: [
                        {
                            type: DynamicFieldType.Input,
                            label: "Odometer Reading",
                            name: "odometer",
                            validations: [
                                {
                                    name: 'required',
                                    validator: Validators.required,
                                    message: 'This field is required!'
                                },
                                {
                                    name: 'pattern',
                                    validator: Validators.pattern(/^[0-9]+$/),
                                    message: 'Allow only numbers'
                                },
                            ],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Input,
                            label: "Service Due (KM)",
                            name: "serviceDue",
                            validations: [
                                {
                                    name: 'required',
                                    validator: Validators.required,
                                    message: 'This field is required!'
                                },
                                {
                                    name: 'pattern',
                                    validator: Validators.pattern(/^[0-9]+$/),
                                    message: 'Allow only numbers'
                                },
                            ],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Input,
                            label: "Make and Model",
                            name: "makeAndModel",
                            validations: [
                                {
                                    name: 'required',
                                    validator: Validators.required,
                                    message: 'This field is required!'
                                },
                            ],
                            value: this._authService.userVehicleDefaults?.makeAndModel
                        },
                        {
                            type: DynamicFieldType.Select,
                            label: "Vehicle Type",
                            name: "type",
                            options: [...this.vehicleTypes],
                            validations: [
                                {
                                    name: 'required',
                                    validator: Validators.required,
                                    message: 'This field is required!'
                                },
                            ],
                            value: this._authService.userVehicleDefaults?.type
                        },
                        {
                            type: DynamicFieldType.Select,
                            label: "Do you have a current driver licence?",
                            name: "isLicenseConsist",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [
                                {
                                    name: 'required',
                                    validator: Validators.required,
                                    message: 'This field is required!'
                                },
                            ],
                            value: ''
                        },
                        {
                            type: DynamicFieldType.Date,
                            label: "Drivers Licence Expiry Date",
                            name: "driversLicenceExpiryDate",
                            validations: [
                                {
                                    name: 'required',
                                    validator: Validators.required,
                                    message: 'This field is required!'
                                },
                            ],
                            value: this._authService.userVehicleDefaults?.licenceExpiry?.toDate()
                        },
                        {
                            type: DynamicFieldType.Input,
                            label: "Vehicle Registration",
                            name: "registration",
                            validations: [
                                {
                                    name: 'required',
                                    validator: Validators.required,
                                    message: 'This field is required!'
                                },
                                {
                                    name: 'pattern',
                                    validator: Validators.pattern(/^[a-z0-9]+$/i),
                                    message: 'Registration can only consist from letters or numbers'
                                },
                            ],
                            value: this._authService.userVehicleDefaults?.registration
                        },
                        {
                            type: DynamicFieldType.Date,
                            label: "WOF/COF Due",
                            name: "wofCofDue",
                            validations: [
                                {
                                    name: 'required',
                                    validator: Validators.required,
                                    message: 'This field is required!'
                                },
                            ],
                            value: ''
                        },
                        {
                            type: DynamicFieldType.Date,
                            label: "Registration Due",
                            name: "regDue",
                            validations: [
                                {
                                    name: 'required',
                                    validator: Validators.required,
                                    message: 'Registrtion Due is required!'
                                },
                            ],
                            value: ''
                        },
                        {
                            type: DynamicFieldType.Input,
                            label: "RUC/Road User Charges",
                            name: "rucRoadUserCharges",
                            validations: [
                                {
                                    name: 'required',
                                    validator: Validators.required,
                                    message: 'Road user charges is required!'
                                },
                                {
                                    name: 'pattern',
                                    validator: Validators.pattern(/^[a-z0-9]+$/i),
                                    message: 'Road user charges allow only numbers from 1 to 6 digits'
                                },
                            ],
                            value: ''
                        },
                    ]
                },
                {
                    category: 'Tyres',
                    values: [
                        {
                            type: DynamicFieldType.Select,
                            label: "Tyre pressure",
                            name: "tyrePressure",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            label: "Tread depth",
                            name: "treadDepth",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "goodCondition",
                            label: "Tyres in good condition",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "spareServiceable",
                            label: "Spare serviceable",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                    ]
                },
                {
                    category: 'Brakes',
                    values: [
                        {
                            type: DynamicFieldType.Select,
                            name: "brakingPower",
                            label: "Braking Power",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "handBrake",
                            label: "Hand Brake",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        }
                    ]
                },
                {
                    category: 'Seat Belts',
                    values: [
                        {
                            type: DynamicFieldType.Select,
                            name: "tensionerFunctions",
                            label: "Tensioner Functions",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "beltsWork",
                            label: "Belts work and are in good condition",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        }
                    ]
                },
                {
                    category: 'Wipers',
                    values: [
                        {
                            type: DynamicFieldType.Select,
                            name: "wipersFunction",
                            label: "Wipers function",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "washerFluidLevel",
                            label: "Washer fluid level",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        }
                    ]
                },
                {
                    category: 'Windows & Windscreen',
                    values: [
                        {
                            type: DynamicFieldType.Select,
                            name: "windscreen",
                            label: "Windscreen",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "windows",
                            label: "Windows",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                    ]
                },
                {
                    category: 'Lights',
                    values: [
                        {
                            type: DynamicFieldType.Select,
                            name: "numberPlate",
                            label: "Number Plate",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "highBeam",
                            label: "High Beam",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "lowBeam",
                            label: "Low Beam",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "parking",
                            label: "Parking",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "reverse",
                            label: "Reverse",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "brakes",
                            label: "Brakes",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                    ]
                },
                {
                    category: 'Indicators',
                    values: [
                        {
                            type: DynamicFieldType.Select,
                            name: "indicatorFront",
                            label: "Front",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "indicatorSide",
                            label: "Side",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "indicatorRear",
                            label: "Rear",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "indicatorHazards",
                            label: "Hazards",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                    ]
                },
                {
                    category: 'Mirrors',
                    values: [
                        {
                            type: DynamicFieldType.Select,
                            name: "rearSideMirror",
                            label: "Rear/Side Mirrors",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        }
                    ]
                },
                {
                    category: 'Maintenance',
                    values: [
                        {
                            type: DynamicFieldType.Select,
                            name: "engineStarts",
                            label: 'Engine Starts 1st Time',
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "oilLevel",
                            label: 'Oil Levels',
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "waterLevel",
                            label: 'Water Levels',
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "brakeFluidLevels",
                            label: 'Brake Fluid Levels',
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "airConditioning",
                            label: 'Air Conditioning',
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "doorFunction",
                            label: 'Door Functioning',
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "strutsRams",
                            label: 'Struts and Rams',
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "reverseBeeper",
                            label: 'Reverse Beeper',
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "reverseCameraScreen",
                            label: 'Reverse Camera Screen',
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "reverseCameraCameraLens",
                            label: 'Reverse Camera Lens',
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "horn",
                            label: 'Horn',
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "isVehicleClean",
                            label: 'Vehicle Clean',
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "accidentDamage",
                            label: 'Is the vehicle free from damage?',
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "decals",
                            label: 'Decals',
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No, FormAnswerClassic.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        }
                    ]
                },
                {
                    category: 'General Items',
                    values: [
                        {
                            type: DynamicFieldType.Select,
                            name: "stickers",
                            label: "Take 5 Stickers",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "manualHandbook",
                            label: "Manual Handbook",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "firstAidKit",
                            label: "First Aid Kit (Missing Items Listed in Comments)",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "handSanitiser",
                            label: "Hand Sanitiser",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "sdsFolder",
                            label: "SDS Folder",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                    ]
                }
            ]
        },

        {
            formType: 'Pest Service Form',
            formClass: FormClass.VehicleForm,
            formId: 'v-pest-form',
            countryName: CountryName.NewZealand,
            config: [
                {
                    category: 'Vehicle Defaults',
                    values: [
                        {
                            type: DynamicFieldType.Input,
                            label: "Odometer Reading",
                            name: "odometer",
                            validations: [
                                {
                                    name: 'required',
                                    validator: Validators.required,
                                    message: 'This field is required!'
                                },
                                {
                                    name: 'pattern',
                                    validator: Validators.pattern(/^[0-9]+$/),
                                    message: 'Allow only numbers'
                                },
                            ],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Input,
                            label: "Service Due (KM)",
                            name: "serviceDue",
                            validations: [
                                {
                                    name: 'required',
                                    validator: Validators.required,
                                    message: 'This field is required!'
                                },
                                {
                                    name: 'pattern',
                                    validator: Validators.pattern(/^[0-9]+$/),
                                    message: 'Allow only numbers'
                                },
                            ],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Input,
                            label: "Make and Model",
                            name: "makeAndModel",
                            validations: [
                                {
                                    name: 'required',
                                    validator: Validators.required,
                                    message: 'This field is required!'
                                },
                            ],
                            value: this._authService.userVehicleDefaults?.makeAndModel
                        },
                        {
                            type: DynamicFieldType.Select,
                            label: "Vehicle Type",
                            name: "type",
                            options: [...this.vehicleTypes],
                            validations: [
                                {
                                    name: 'required',
                                    validator: Validators.required,
                                    message: 'This field is required!'
                                },
                            ],
                            value: this._authService.userVehicleDefaults?.type
                        },
                        {
                            type: DynamicFieldType.Select,
                            label: "Do you have a current driver licence?",
                            name: "isLicenseConsist",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [
                                {
                                    name: 'required',
                                    validator: Validators.required,
                                    message: 'This field is required!'
                                },
                            ],
                            value: ''
                        },
                        {
                            type: DynamicFieldType.Date,
                            label: "Drivers Licence Expiry Date",
                            name: "driversLicenceExpiryDate",
                            validations: [
                                {
                                    name: 'required',
                                    validator: Validators.required,
                                    message: 'This field is required!'
                                },
                            ],
                            value: this._authService.userVehicleDefaults?.licenceExpiry?.toDate()
                        },
                        {
                            type: DynamicFieldType.Input,
                            label: "Vehicle Registration",
                            name: "registration",
                            validations: [
                                {
                                    name: 'required',
                                    validator: Validators.required,
                                    message: 'This field is required!'
                                },
                                {
                                    name: 'pattern',
                                    validator: Validators.pattern(/^[a-z0-9]+$/i),
                                    message: 'Registration can only consist from letters or numbers'
                                },
                            ],
                            value: this._authService.userVehicleDefaults?.registration
                        },
                        {
                            type: DynamicFieldType.Date,
                            label: "WOF/COF Due",
                            name: "wofCofDue",
                            validations: [
                                {
                                    name: 'required',
                                    validator: Validators.required,
                                    message: 'This field is required!'
                                },
                            ],
                            value: ''
                        },
                        {
                            type: DynamicFieldType.Date,
                            label: "Registration Due",
                            name: "regDue",
                            validations: [
                                {
                                    name: 'required',
                                    validator: Validators.required,
                                    message: 'Registrtion Due is required!'
                                },
                            ],
                            value: ''
                        },
                        {
                            type: DynamicFieldType.Input,
                            label: "RUC/Road User Charges",
                            name: "rucRoadUserCharges",
                            validations: [
                                {
                                    name: 'required',
                                    validator: Validators.required,
                                    message: 'Road user charges is required!'
                                },
                                {
                                    name: 'pattern',
                                    validator: Validators.pattern(/^[a-z0-9]+$/i),
                                    message: 'Road user charges allow only numbers from 1 to 6 digits'
                                },
                            ],
                            value: ''
                        },
                    ]
                },
                {
                    category: 'Tyres',
                    values: [
                        {
                            type: DynamicFieldType.Select,
                            label: "Tyre pressure",
                            name: "tyrePressure",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            label: "Tread depth",
                            name: "treadDepth",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "goodCondition",
                            label: "Tyres in good condition",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "spareServiceable",
                            label: "Spare serviceable",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                    ]
                },
                {
                    category: 'Brakes',
                    values: [
                        {
                            type: DynamicFieldType.Select,
                            name: "brakingPower",
                            label: "Braking Power",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "handBrake",
                            label: "Hand Brake",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        }
                    ]
                },
                {
                    category: 'Seat Belts',
                    values: [
                        {
                            type: DynamicFieldType.Select,
                            name: "tensionerFunctions",
                            label: "Tensioner Functions",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "beltsWork",
                            label: "Belts work and are in good condition",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        }
                    ]
                },
                {
                    category: 'Wipers',
                    values: [
                        {
                            type: DynamicFieldType.Select,
                            name: "wipersFunction",
                            label: "Wipers function",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "washerFluidLevel",
                            label: "Washer fluid level",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        }
                    ]
                },
                {
                    category: 'Windows & Windscreen',
                    values: [
                        {
                            type: DynamicFieldType.Select,
                            name: "windscreen",
                            label: "Windscreen",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "windows",
                            label: "Windows",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                    ]
                },
                {
                    category: 'Lights',
                    values: [
                        {
                            type: DynamicFieldType.Select,
                            name: "numberPlate",
                            label: "Number Plate",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "highBeam",
                            label: "High Beam",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "lowBeam",
                            label: "Low Beam",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "parking",
                            label: "Parking",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "reverse",
                            label: "Reverse",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "brakes",
                            label: "Brakes",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                    ]
                },
                {
                    category: 'Indicators',
                    values: [
                        {
                            type: DynamicFieldType.Select,
                            name: "indicatorFront",
                            label: "Front",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "indicatorSide",
                            label: "Side",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "indicatorRear",
                            label: "Rear",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "indicatorHazards",
                            label: "Hazards",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                    ]
                },
                {
                    category: 'Mirrors',
                    values: [
                        {
                            type: DynamicFieldType.Select,
                            name: "rearSideMirror",
                            label: "Rear/Side Mirrors",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        }
                    ]
                },
                {
                    category: 'Maintenance',
                    values: [
                        {
                            type: DynamicFieldType.Select,
                            name: "engineStarts",
                            label: 'Engine Starts 1st Time',
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "oilLevel",
                            label: 'Oil Levels',
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "waterLevel",
                            label: 'Water Levels',
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "brakeFluidLevels",
                            label: 'Brake Fluid Levels',
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "airConditioning",
                            label: 'Air Conditioning',
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "gpsSticker",
                            label: 'GPS Sticker',
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "heightLevel",
                            label: 'Height Label',
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "doorFunction",
                            label: 'Door Functioning',
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "strutsRams",
                            label: 'Struts and Rams',
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "reverseBeeper",
                            label: 'Reverse Beeper',
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "reverseCameraScreen",
                            label: 'Reverse Camera Screen',
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "reverseCameraCameraLens",
                            label: 'Reverse Camera Lens',
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "horn",
                            label: 'Horn',
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "isVehicleClean",
                            label: 'Vehicle Clean',
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "accidentDamage",
                            label: 'Is the vehicle free from damage?',
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "decals",
                            label: 'Decals',
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        }
                    ]
                },
                {
                    category: 'Other',
                    values: [
                        {
                            type: DynamicFieldType.Select,
                            name: "tankUnitFixed",
                            label: 'Tank Unit Fixed',
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "spillTrayFitted",
                            label: 'Spill Tray Fitted',
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "hoseConnectorsSecure",
                            label: 'Hose Connectors Secure',
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                    ]
                },
                {
                    category: 'General Items',
                    values: [
                        {
                            type: DynamicFieldType.Select,
                            name: "stickers",
                            label: "Take 5 Stickers",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "manualHandbook",
                            label: "Manual Handbook",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "fireExtinguisher",
                            label: "Fire Extinguisher",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "extinguisherServiced",
                            label: "Extinguisher Serviced",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "extinguisherAccessible",
                            label: "Extinguisher Accessible",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "firstAidKit",
                            label: "First Aid Kit (Missing Items Listed in Comments)",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "handSanitiser",
                            label: "Hand Sanitiser",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "sdsFolder",
                            label: "SDS Folder",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        }
                    ]
                },
                {
                    category: 'PPE',
                    values: [
                        {
                            type: DynamicFieldType.Select,
                            name: "pvcGloves",
                            label: "PVC Gloves",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "latexGloves",
                            label: "Latex Gloves",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "earMuffs",
                            label: "Ear Muffs ",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "safetyGlasses",
                            label: "Safety Glasses",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "faceShield",
                            label: "Face Shield",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "safetyBoots",
                            label: "Safety Boots",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "safetyVest",
                            label: "Safety Vest",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "hardHat",
                            label: "Hard Hat / Bump Cap",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "spareUniform",
                            label: "Spare Uniform",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "beeHood",
                            label: "Bee Hood",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "insectRepellent",
                            label: "Insect Repellent",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "overalls",
                            label: "Overalls (Two Pairs)",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "kneePads",
                            label: "Knee Pads",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "subfloorEntrySign",
                            label: "Subfloor Entry Sign",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "paperTowelDispensor",
                            label: "Paper Towel Dispensor ",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "sunscreen",
                            label: "Sunscreen 30+",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "spareSafetyBoots",
                            label: "Spare Safety Boots",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "faceMaskAndRespirator",
                            label: "Face Mask And Respirator",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "faceMaskCartridges",
                            label: "Face Mask Cartridges",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        }
                    ]
                },
                {
                    category: 'Spill Kit',
                    values: [
                        {
                            type: DynamicFieldType.Select,
                            name: "spillKitStocked",
                            label: 'Spill Kit Stocked',
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        }
                    ]
                },
                {
                    category: 'Dangerous Goods',
                    values: [
                        {
                            type: DynamicFieldType.Select,
                            name: "dangerousGoodsStoredCorrectly",
                            label: 'Dangerous Goods Stored Correctly',
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "chemicalTreatmentSigns",
                            label: 'Chemical Treatment Signs',
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                    ]
                },
                {
                    category: 'Other',
                    values: [
                        {
                            type: DynamicFieldType.Select,
                            name: "extensionLadder",
                            label: 'Extension Ladder 2.4m',
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "ladder",
                            label: 'Ladder 1.8m',
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "rcdSafetySwitch",
                            label: 'RCD Safety Switch',
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "extensionCord",
                            label: 'Extension Cord',
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "waterBottle",
                            label: '20L Water Bottle',
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "soapDispenser",
                            label: 'Soap Dispenser',
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                    ]
                },
            ]
        },

        // Fiji Forms
        {
            formType: 'Ambius Service Form',
            formClass: FormClass.VehicleForm,
            formId: 'v-ambi-form',
            countryName: CountryName.Fiji,
            config: [
                {
                    category: 'Vehicle Defaults',
                    values: [
                        {
                            type: DynamicFieldType.Input,
                            label: "Odometer Reading",
                            name: "odometer",
                            validations: [
                                {
                                    name: 'required',
                                    validator: Validators.required,
                                    message: 'This field is required!'
                                },
                                {
                                    name: 'pattern',
                                    validator: Validators.pattern(/^[0-9]+$/),
                                    message: 'Allow only numbers'
                                },
                            ],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Input,
                            label: "Service Due (KM)",
                            name: "serviceDue",
                            validations: [
                                {
                                    name: 'required',
                                    validator: Validators.required,
                                    message: 'This field is required!'
                                },
                                {
                                    name: 'pattern',
                                    validator: Validators.pattern(/^[0-9]+$/),
                                    message: 'Allow only numbers'
                                },
                            ],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Input,
                            label: "Make and Model",
                            name: "makeAndModel",
                            validations: [
                                {
                                    name: 'required',
                                    validator: Validators.required,
                                    message: 'This field is required!'
                                },
                            ],
                            value: this._authService.userVehicleDefaults?.makeAndModel
                        },
                        {
                            type: DynamicFieldType.Select,
                            label: "Vehicle Type",
                            name: "type",
                            options: [...this.vehicleTypes],
                            validations: [
                                {
                                    name: 'required',
                                    validator: Validators.required,
                                    message: 'This field is required!'
                                },
                            ],
                            value: this._authService.userVehicleDefaults?.type
                        },
                        {
                            type: DynamicFieldType.Select,
                            label: "Do you have a current driver licence?",
                            name: "isLicenseConsist",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [
                                {
                                    name: 'required',
                                    validator: Validators.required,
                                    message: 'This field is required!'
                                },
                            ],
                            value: ''
                        },
                        {
                            type: DynamicFieldType.Date,
                            label: "Drivers Licence Expiry Date",
                            name: "driversLicenceExpiryDate",
                            validations: [
                                {
                                    name: 'required',
                                    validator: Validators.required,
                                    message: 'This field is required!'
                                },
                            ],
                            value: this._authService.userVehicleDefaults?.licenceExpiry?.toDate()
                        },
                        {
                            type: DynamicFieldType.Input,
                            label: "Vehicle Registration",
                            name: "registration",
                            validations: [
                                {
                                    name: 'required',
                                    validator: Validators.required,
                                    message: 'This field is required!'
                                },
                                {
                                    name: 'pattern',
                                    validator: Validators.pattern(/^[a-z0-9]+$/i),
                                    message: 'Registration can only consist from letters or numbers'
                                },
                            ],
                            value: this._authService.userVehicleDefaults?.registration
                        },
                    ]
                },
                {
                    category: 'Tyres',
                    values: [
                        {
                            type: DynamicFieldType.Select,
                            label: "Tyre Pressure",
                            name: "tyrePressure",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            label: "Tread Depth",
                            name: "treadDepth",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "goodCondition",
                            label: "Tyres in Good Condition",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "spareServiceable",
                            label: "Spare Serviceable",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                    ]
                },
                {
                    category: 'Brakes',
                    values: [
                        {
                            type: DynamicFieldType.Select,
                            name: "brakingPower",
                            label: "Braking Power",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "handBrake",
                            label: "Hand Brake",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        }
                    ]
                },
                {
                    category: 'Seat Belts',
                    values: [
                        {
                            type: DynamicFieldType.Select,
                            name: "tensionerFunctions",
                            label: "Belt Tensioner Functions",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "beltsWork",
                            label: "Belts work and are in good condition",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        }
                    ]
                },
                {
                    category: 'Wipers',
                    values: [
                        {
                            type: DynamicFieldType.Select,
                            name: "wipersFunction",
                            label: "Wipers Function",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "washerFluidLevel",
                            label: "Washer Fluid Level is High",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        }
                    ]
                },
                {
                    category: 'Windows & Windscreen',
                    values: [
                        {
                            type: DynamicFieldType.Select,
                            name: "windscreen",
                            label: "Windscreen",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "windows",
                            label: "Windows",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                    ]
                },
                {
                    category: 'Lights',
                    values: [
                        {
                            type: DynamicFieldType.Select,
                            name: "numberPlate",
                            label: "Number Plate",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "highBeam",
                            label: "High Beam",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "lowBeam",
                            label: "Low Beam",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "parking",
                            label: "Parking",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "reverse",
                            label: "Reverse",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "brakes",
                            label: "Brakes",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                    ]
                },
                {
                    category: 'Indicators',
                    values: [
                        {
                            type: DynamicFieldType.Select,
                            name: "indicatorFront",
                            label: "Front",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "indicatorSide",
                            label: "Side",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "indicatorRear",
                            label: "Rear",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "indicatorHazards",
                            label: "Hazards",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                    ]
                },
                {
                    category: 'Mirrors',
                    values: [
                        {
                            type: DynamicFieldType.Select,
                            name: "rearSideMirror",
                            label: "Rear/Side Mirrors",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "fishEyeSpotter",
                            label: 'Fish Eye Spotter',
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        }
                    ]
                },
                {
                    category: 'Maintenance',
                    values: [
                        {
                            type: DynamicFieldType.Select,
                            name: "engineStarts",
                            label: 'Engine Starts 1st Time',
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "oilLevel",
                            label: 'Oil Levels',
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "waterLevel",
                            label: 'Water Levels',
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "brakeFluidLevels",
                            label: 'Brake Fluid Levels',
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "airConditioning",
                            label: 'Air Conditioning',
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "gpsSticker",
                            label: 'GPS Sticker',
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "heightLevel",
                            label: 'Height Label',
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "doorFunction",
                            label: 'Doors Function Properly',
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "strutsRams",
                            label: 'Struts and Rams',
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "reverseBeeper",
                            label: 'Reverse Beeper',
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "reverseCameraScreen",
                            label: 'Reverse Camera Screen',
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "reverseCameraCameraLens",
                            label: 'Reverse Camera Lens',
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "horn",
                            label: 'Horn',
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "isVehicleClean",
                            label: 'Vehicle Clean',
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "accidentDamage",
                            label: 'Is the vehicle free from damage?',
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "decals",
                            label: 'Decals',
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        }
                    ]
                },
                {
                    category: 'Other',
                    values: [
                        {
                            type: DynamicFieldType.Select,
                            name: "tailgate",
                            label: 'Tailgate',
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                    ]
                },
                {
                    category: 'General Items',
                    values: [
                        {
                            type: DynamicFieldType.Select,
                            name: "stickers",
                            label: "Have Take 5 Stickers",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "manualHandbook",
                            label: "Manual Handbook",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "fireExtinguisher",
                            label: "Fire Extinguisher",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "extinguisherServiced",
                            label: "Extinguisher Serviced",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "extinguisherAccessible",
                            label: "Extinguisher Accessible",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "firstAidKit",
                            label: "First Aid Kit",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "handSanitiser",
                            label: "Hand Sanitiser",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "sdsFolder",
                            label: "SDS Folder",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        }
                    ]
                },
                {
                    category: 'Spill Kit',
                    values: [
                        {
                            type: DynamicFieldType.Select,
                            name: "absorbentGranules",
                            label: "Absorbent Granules",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "glovesAndGlassesAndMask",
                            label: "PPE - Gloves, Safety Glasses, Dust Mask",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "dustPanAndBrush",
                            label: "Dust Pan & Brush",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "tongs",
                            label: "Tongs",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "bucketWithLid",
                            label: "Bucket with lid and heavy duty plastic bag",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "sharpsCleanUpKit",
                            label: "Sharps clean up kit",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "mop",
                            label: "Mop",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        }
                    ]
                },
                {
                    category: 'Other Equipment',
                    values: [
                        {
                            type: DynamicFieldType.Select,
                            name: "trolley",
                            label: "Trolley",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "stepLadder",
                            label: "Step Ladder (1.8m Industrial Grade)",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "technicalKitBag",
                            label: "Technician Kit Bag",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "bucketAndWateringCan",
                            label: "Bucket and Watering Can",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "secateursAndLeatherSheaf",
                            label: "Secateurs and Leather Sheaf",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "cleaningCloths",
                            label: "Cleaning Cloths (Sponges and Chux)",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "sprayBottles",
                            label: "Spray Bottles (2 x 1L)",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "chemicalsLabelled",
                            label: "All Chemicals Labelled with contents",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "soilMoisture",
                            label: "Soil Moisture Testing Probe",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "waterPipette",
                            label: "Water Measuring Pipette",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "wireTie",
                            label: "Wire Tie",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "heavyDutyPlasticBug",
                            label: "Heavy Duty Plastic Bag",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "yellowWarningSun",
                            label: "Yellow Warning Sign",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        }
                    ]
                },
            ]
        },

        {
            formType: 'Fumigation Service Form',
            formClass: FormClass.VehicleForm,
            formId: 'v-fumi-form',
            countryName: CountryName.Fiji,
            config: [
                {
                    category: 'Vehicle Defaults',
                    values: [
                        {
                            type: DynamicFieldType.Input,
                            label: "Odometer Reading",
                            name: "odometer",
                            validations: [
                                {
                                    name: 'required',
                                    validator: Validators.required,
                                    message: 'This field is required!'
                                },
                                {
                                    name: 'pattern',
                                    validator: Validators.pattern(/^[0-9]+$/),
                                    message: 'Allow only numbers'
                                },
                            ],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Input,
                            label: "Service Due (KM)",
                            name: "serviceDue",
                            validations: [
                                {
                                    name: 'required',
                                    validator: Validators.required,
                                    message: 'This field is required!'
                                },
                                {
                                    name: 'pattern',
                                    validator: Validators.pattern(/^[0-9]+$/),
                                    message: 'Allow only numbers'
                                },
                            ],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Input,
                            label: "Make and Model",
                            name: "makeAndModel",
                            validations: [
                                {
                                    name: 'required',
                                    validator: Validators.required,
                                    message: 'This field is required!'
                                },
                            ],
                            value: this._authService.userVehicleDefaults?.makeAndModel
                        },
                        {
                            type: DynamicFieldType.Select,
                            label: "Vehicle Type",
                            name: "type",
                            options: [...this.vehicleTypes],
                            validations: [
                                {
                                    name: 'required',
                                    validator: Validators.required,
                                    message: 'This field is required!'
                                },
                            ],
                            value: this._authService.userVehicleDefaults?.type
                        },
                        {
                            type: DynamicFieldType.Select,
                            label: "Do you have a current driver licence?",
                            name: "isLicenseConsist",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [
                                {
                                    name: 'required',
                                    validator: Validators.required,
                                    message: 'This field is required!'
                                },
                            ],
                            value: ''
                        },
                        {
                            type: DynamicFieldType.Date,
                            label: "Drivers Licence Expiry Date",
                            name: "driversLicenceExpiryDate",
                            validations: [
                                {
                                    name: 'required',
                                    validator: Validators.required,
                                    message: 'This field is required!'
                                },
                            ],
                            value: this._authService.userVehicleDefaults?.licenceExpiry?.toDate()
                        },
                        {
                            type: DynamicFieldType.Input,
                            label: "Vehicle Registration",
                            name: "registration",
                            validations: [
                                {
                                    name: 'required',
                                    validator: Validators.required,
                                    message: 'This field is required!'
                                },
                                {
                                    name: 'pattern',
                                    validator: Validators.pattern(/^[a-z0-9]+$/i),
                                    message: 'Registration can only consist from letters or numbers'
                                },
                            ],
                            value: this._authService.userVehicleDefaults?.registration
                        },
                    ]
                },
                {
                    category: 'Tyres',
                    values: [
                        {
                            type: DynamicFieldType.Select,
                            label: "Tyre pressure",
                            name: "tyrePressure",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            label: "Tread depth",
                            name: "treadDepth",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "goodCondition",
                            label: "Tyres in good condition",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "spareServiceable",
                            label: "Spare serviceable",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                    ]
                },
                {
                    category: 'Brakes',
                    values: [
                        {
                            type: DynamicFieldType.Select,
                            name: "brakingPower",
                            label: "Braking Power",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "handBrake",
                            label: "Hand Brake",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        }
                    ]
                },
                {
                    category: 'Seat Belts',
                    values: [
                        {
                            type: DynamicFieldType.Select,
                            name: "tensionerFunctions",
                            label: "Tensioner Functions",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "beltsWork",
                            label: "Belts work and are in good condition",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        }
                    ]
                },
                {
                    category: 'Wipers',
                    values: [
                        {
                            type: DynamicFieldType.Select,
                            name: "wipersFunction",
                            label: "Wipers function",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "washerFluidLevel",
                            label: "Washer fluid level",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        }
                    ]
                },
                {
                    category: 'Windows & Windscreen',
                    values: [
                        {
                            type: DynamicFieldType.Select,
                            name: "windscreen",
                            label: "Windscreen",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "windows",
                            label: "Windows",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                    ]
                },
                {
                    category: 'Lights',
                    values: [
                        {
                            type: DynamicFieldType.Select,
                            name: "numberPlate",
                            label: "Number Plate",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "highBeam",
                            label: "High Beam",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "lowBeam",
                            label: "Low Beam",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "parking",
                            label: "Parking",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "reverse",
                            label: "Reverse",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "brakes",
                            label: "Brakes",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                    ]
                },
                {
                    category: 'Indicators',
                    values: [
                        {
                            type: DynamicFieldType.Select,
                            name: "indicatorFront",
                            label: "Front",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "indicatorSide",
                            label: "Side",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "indicatorRear",
                            label: "Rear",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "indicatorHazards",
                            label: "Hazards",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                    ]
                },
                {
                    category: 'Mirrors',
                    values: [
                        {
                            type: DynamicFieldType.Select,
                            name: "rearSideMirror",
                            label: "Rear/Side Mirrors",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "fishEyeSpotter",
                            label: 'Fish Eye Spotter',
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        }
                    ]
                },
                {
                    category: 'Maintenance',
                    values: [
                        {
                            type: DynamicFieldType.Select,
                            name: "engineStarts",
                            label: 'Engine Starts 1st Time',
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "oilLevel",
                            label: 'Oil Levels',
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "waterLevel",
                            label: 'Water Levels',
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "brakeFluidLevels",
                            label: 'Brake Fluid Levels',
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "airConditioning",
                            label: 'Air Conditioning',
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "gpsSticker",
                            label: 'GPS Sticker',
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "heightLevel",
                            label: 'Height Label',
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "doorFunction",
                            label: 'Door Functioning',
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "strutsRams",
                            label: 'Struts and Rams',
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "horn",
                            label: 'Horn',
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "reverseCameraScreen",
                            label: 'Reverse Camera Screen',
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "reverseCameraCameraLens",
                            label: 'Reverse Camera Lens',
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "reverseBeeper",
                            label: 'Reverse Beeper',
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "isVehicleClean",
                            label: 'Vehicle Clean',
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "vehicleBodyCondition",
                            label: 'Vehicle Body Condition',
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "rearCompartment",
                            label: 'Rear Compartment',
                            options: ["Safe", "Not Safe"],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "decals",
                            label: 'Decals',
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "trayCover",
                            label: 'Tray Cover',
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "chassisRail",
                            label: 'Chassis Rail',
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        }
                    ]
                },
                {
                    category: 'General Items',
                    values: [
                        {
                            type: DynamicFieldType.Select,
                            name: "stickers",
                            label: "Take 5 Stickers",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "fireExtinguisher",
                            label: "Fire Extinguisher",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "extinguisherServiced",
                            label: "Extinguisher Serviced",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "extinguisherAccessible",
                            label: "Extinguisher Accessible",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "firstAidKit",
                            label: "First Aid Kit (Missing Items Listed in Comments)",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "sdsFolder",
                            label: "Safety Data Sheet (SDS) Folder",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "manualHandbook",
                            label: "Vehicle Manual & Drivers Handbook",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "handSanitiser",
                            label: "Hand Sanitiser",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "soapDispenser",
                            label: "Soap Dispenser (complete with spare soap)",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "fanHeaters",
                            label: "Fan & Heaters",
                            options: ["Yes", "No", "N/A"],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "extensionCord",
                            label: "Extension Cord",
                            options: ["Safe", "Not Safe"],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        }


                    ]
                },
                {
                    category: 'Manuals/Required Information',
                    values: [
                        {
                            type: DynamicFieldType.Select,
                            name: "fumigationTechnicalFolder",
                            label: "Fumigation Technical Folder",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "emergencyResponseInformation",
                            label: "Emergency Response Information (TeamCards) for chemicals being carried",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        }
                    ]
                },
                {
                    category: 'General Vehicle Equipment',
                    values: [
                        {
                            type: DynamicFieldType.Select,
                            name: "insectRepellent",
                            label: "Insect Repellent",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "approvedFumigationTape",
                            label: "Approved Fumigation Tape",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "fumigationWarningSigns",
                            label: "Fumigation Warning Signs",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "bollardsOrBunting",
                            label: "Bollards / Bunting",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "cableTies",
                            label: "Cable Ties",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "sunscreen",
                            label: "Sunscreen",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "waterBottle",
                            label: "20L Water Bottle (Isuzu Models only)",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                    ]
                },
                {
                    category: 'Dangerous Goods',
                    values: [
                        {
                            type: DynamicFieldType.Select,
                            name: "dangerousGoods",
                            label: "Dangerous Goods stored correctly",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "spillKit",
                            label: "Spill Kit",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        }
                    ]
                },
                {
                    category: 'Equipment',
                    values: [
                        {
                            type: DynamicFieldType.Select,
                            name: "marineHotWaterSystem",
                            label: "Marine Hot Water System",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "bottleRacks",
                            label: "Bottle Racks",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "tempRecordingDevices",
                            label: "Temp. Recording Devices",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "shiftingSpanner",
                            label: "Shifting Spanner",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "profumeSpanner",
                            label: "Profume Spanner",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "gasMonitoringLines",
                            label: "Gas / Monitoring Lines",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "safetyGlasses",
                            label: "SafetyGlasses",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "pvsMonitorExtensionTubes",
                            label: "PVC Monitor Extension Tubes",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "methylBromideDispenser",
                            label: "Methyl Bromide Dispenser",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "ispmStamps",
                            label: "ISPM Stamps",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "personalDetectionDevice",
                            label: "Personal Detection Device",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "ladder",
                            label: "Ladder",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        }
                    ]
                },
                {
                    category: 'Digital Equipment Case',
                    values: [
                        {
                            type: DynamicFieldType.Select,
                            name: "digitalMonitoringUnit",
                            label: "Digital Monitoring Unit",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "digitalLeakCheckingUnit",
                            label: "Digital Leak Checking Unit",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "digitalClearanceDevice",
                            label: "Digital Clearance Device",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "dryWipeBrush",
                            label: "Dry Wipe Brush",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "alcoholFreeWipes",
                            label: "Alcohol Free Wipes",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        }
                    ]
                },
                {
                    category: 'PPE',
                    values: [
                        {
                            type: DynamicFieldType.Select,
                            name: "steelCapBoots",
                            label: "Steel Cap Boots",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "hiVisShirt",
                            label: "Hi-Vis Shirt & Pants",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "gloves",
                            label: "Gloves",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "fullFaceRespirator",
                            label: "Full Face Respirator",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "respiratorCartridges",
                            label: "Respirator Cartridges",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "hat",
                            label: "Hat",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "wetWeatherRaincoat",
                            label: "Wet Weather Raincoat and Pants",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        }
                    ]
                },
            ]
        },

        {
            formType: 'Hygiene Service Form',
            formClass: FormClass.VehicleForm,
            formId: 'v-init-form',
            countryName: CountryName.Fiji,
            config: [
                {
                    category: 'Vehicle Defaults',
                    values: [
                        {
                            type: DynamicFieldType.Input,
                            label: "Odometer Reading",
                            name: "odometer",
                            validations: [
                                {
                                    name: 'required',
                                    validator: Validators.required,
                                    message: 'This field is required!'
                                },
                                {
                                    name: 'pattern',
                                    validator: Validators.pattern(/^[0-9]+$/),
                                    message: 'Allow only numbers'
                                },
                            ],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Input,
                            label: "Service Due (KM)",
                            name: "serviceDue",
                            validations: [
                                {
                                    name: 'required',
                                    validator: Validators.required,
                                    message: 'This field is required!'
                                },
                                {
                                    name: 'pattern',
                                    validator: Validators.pattern(/^[0-9]+$/),
                                    message: 'Allow only numbers'
                                },
                            ],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Input,
                            label: "Make and Model",
                            name: "makeAndModel",
                            validations: [
                                {
                                    name: 'required',
                                    validator: Validators.required,
                                    message: 'This field is required!'
                                },
                            ],
                            value: this._authService.userVehicleDefaults?.makeAndModel
                        },
                        {
                            type: DynamicFieldType.Select,
                            label: "Vehicle Type",
                            name: "type",
                            options: [...this.vehicleTypes],
                            validations: [
                                {
                                    name: 'required',
                                    validator: Validators.required,
                                    message: 'This field is required!'
                                },
                            ],
                            value: this._authService.userVehicleDefaults?.type
                        },
                        {
                            type: DynamicFieldType.Select,
                            label: "Do you have a current driver licence?",
                            name: "isLicenseConsist",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [
                                {
                                    name: 'required',
                                    validator: Validators.required,
                                    message: 'This field is required!'
                                },
                            ],
                            value: ''
                        },
                        {
                            type: DynamicFieldType.Date,
                            label: "Drivers Licence Expiry Date",
                            name: "driversLicenceExpiryDate",
                            validations: [
                                {
                                    name: 'required',
                                    validator: Validators.required,
                                    message: 'This field is required!'
                                },
                            ],
                            value: this._authService.userVehicleDefaults?.licenceExpiry?.toDate()
                        },
                        {
                            type: DynamicFieldType.Input,
                            label: "Vehicle Registration",
                            name: "registration",
                            validations: [
                                {
                                    name: 'required',
                                    validator: Validators.required,
                                    message: 'This field is required!'
                                },
                                {
                                    name: 'pattern',
                                    validator: Validators.pattern(/^[a-z0-9]+$/i),
                                    message: 'Registration can only consist from letters or numbers'
                                },
                            ],
                            value: this._authService.userVehicleDefaults?.registration
                        },
                    ]
                },
                {
                    category: 'Tyres',
                    values: [
                        {
                            type: DynamicFieldType.Select,
                            label: "Tyre pressure",
                            name: "tyrePressure",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            label: "Tread depth",
                            name: "treadDepth",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "goodCondition",
                            label: "Tyres in good condition",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "spareServiceable",
                            label: "Spare serviceable",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                    ]
                },
                {
                    category: 'Brakes',
                    values: [
                        {
                            type: DynamicFieldType.Select,
                            name: "brakingPower",
                            label: "Braking Power",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "handBrake",
                            label: "Hand Brake",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        }
                    ]
                },
                {
                    category: 'Seat Belts',
                    values: [
                        {
                            type: DynamicFieldType.Select,
                            name: "tensionerFunctions",
                            label: "Tensioner Functions",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "beltsWork",
                            label: "Belts work and are in good condition",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        }
                    ]
                },
                {
                    category: 'Wipers',
                    values: [
                        {
                            type: DynamicFieldType.Select,
                            name: "wipersFunction",
                            label: "Wipers function",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "washerFluidLevel",
                            label: "Washer fluid level",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        }
                    ]
                },
                {
                    category: 'Windows & Windscreen',
                    values: [
                        {
                            type: DynamicFieldType.Select,
                            name: "windscreen",
                            label: "Windscreen",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "windows",
                            label: "Windows",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                    ]
                },
                {
                    category: 'Lights',
                    values: [
                        {
                            type: DynamicFieldType.Select,
                            name: "numberPlate",
                            label: "Number Plate",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "highBeam",
                            label: "High Beam",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "lowBeam",
                            label: "Low Beam",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "parking",
                            label: "Parking",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "reverse",
                            label: "Reverse",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "brakes",
                            label: "Brakes",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                    ]
                },
                {
                    category: 'Indicators',
                    values: [
                        {
                            type: DynamicFieldType.Select,
                            name: "indicatorFront",
                            label: "Front",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "indicatorSide",
                            label: "Side",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "indicatorRear",
                            label: "Rear",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "indicatorHazards",
                            label: "Hazards",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                    ]
                },
                {
                    category: 'Mirrors',
                    values: [
                        {
                            type: DynamicFieldType.Select,
                            name: "rearSideMirror",
                            label: "Rear/Side Mirrors",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "fishEyeSpotter",
                            label: 'Fish Eye Spotter',
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        }
                    ]
                },
                {
                    category: 'Maintenance',
                    values: [
                        {
                            type: DynamicFieldType.Select,
                            name: "engineStarts",
                            label: 'Engine Starts 1st Time',
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "oilLevel",
                            label: 'Oil Levels',
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "waterLevel",
                            label: 'Water Levels',
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "brakeFluidLevels",
                            label: 'Brake Fluid Levels',
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "airConditioning",
                            label: 'Air Conditioning',
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "gpsSticker",
                            label: 'GPS Sticker',
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "heightLevel",
                            label: 'Height Label',
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "doorFunction",
                            label: 'Door Functioning',
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "strutsRams",
                            label: 'Struts and Rams',
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "reverseBeeper",
                            label: 'Reverse Beeper',
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "reverseCameraScreen",
                            label: 'Reverse Camera Screen',
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "reverseCameraCameraLens",
                            label: 'Reverse Camera Lens',
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "horn",
                            label: 'Horn',
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "isVehicleClean",
                            label: 'Vehicle Clean',
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "accidentDamage",
                            label: 'Is the vehicle free from damage?',
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "decals",
                            label: 'Decals',
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        }
                    ]
                },
                {
                    category: 'Other',
                    values: [
                        {
                            type: DynamicFieldType.Select,
                            name: "tailgate",
                            label: 'Tailgate',
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                    ]
                },
                {
                    category: 'General Items',
                    values: [
                        {
                            type: DynamicFieldType.Select,
                            name: "stickers",
                            label: "Take 5 Stickers",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "manualHandbook",
                            label: "Manual Handbook",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "fireExtinguisher",
                            label: "Fire Extinguisher",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "extinguisherServiced",
                            label: "Extinguisher Serviced",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "extinguisherAccessible",
                            label: "Extinguisher Accessible",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "firstAidKit",
                            label: "First Aid Kit (Missing Items Listed in Comments)",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "handSanitiser",
                            label: "Hand Sanitiser",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "sdsFolder",
                            label: "SDS Folder",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        }
                    ]
                },
                {
                    category: 'PPE',
                    values: [
                        {
                            type: DynamicFieldType.Select,
                            name: "disposableGloves",
                            label: "Disposable Gloves",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "dustMask",
                            label: "Dust Mask",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "earMuffs",
                            label: "Ear Muffs",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "safetyBoots",
                            label: "Safety Boots",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "safetyVest",
                            label: "Safety Vest",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "hardHat",
                            label: "Hardhat",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "spareContainer",
                            label: "SpareContainer",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "resistantGloves",
                            label: "Needle Resistant Gloves",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "safetyGlasses",
                            label: "Safety Glasses / Goggles",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "Kneepads",
                            label: "Kneepads",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "spillKit",
                            label: "Spill Kit Stocked",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                    ]
                },
                {
                    category: 'Equipment',
                    values: [
                        {
                            type: DynamicFieldType.Select,
                            name: "serviceTrolley",
                            label: "Service Trolley",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "largeLadder",
                            label: "Large Ladder",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "cordlessHammerdrill",
                            label: "Cordless Hammerdrill",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "extensionCord",
                            label: "Extension Cord",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "currentSaferySwitch",
                            label: "Current Safety Switch and RCD Unit",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "toolTray",
                            label: "Tool Tray / Tool Box",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "studFinder",
                            label: "Stud Finder Or Metal Detector",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "serviceKeys",
                            label: "Service Keys",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "vehicleRamps",
                            label: "Vehicle Ramps",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "binSupportBrace",
                            label: "Bin Support Brace",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "smallServiceTrolleyBag",
                            label: "Small Service Trolley Bag",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "whelieAndSulo",
                            label: "Wheelie And Sulo Bin",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        }
                    ]
                },
            ]
        },

        {
            formType: 'Non Service Form',
            formClass: FormClass.VehicleForm,
            formId: 'v-ns-form',
            countryName: CountryName.Fiji,
            config: [
                {
                    category: 'Vehicle Defaults',
                    values: [
                        {
                            type: DynamicFieldType.Input,
                            label: "Odometer Reading",
                            name: "odometer",
                            validations: [
                                {
                                    name: 'required',
                                    validator: Validators.required,
                                    message: 'This field is required!'
                                },
                                {
                                    name: 'pattern',
                                    validator: Validators.pattern(/^[0-9]+$/),
                                    message: 'Allow only numbers'
                                },
                            ],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Input,
                            label: "Service Due (KM)",
                            name: "serviceDue",
                            validations: [
                                {
                                    name: 'required',
                                    validator: Validators.required,
                                    message: 'This field is required!'
                                },
                                {
                                    name: 'pattern',
                                    validator: Validators.pattern(/^[0-9]+$/),
                                    message: 'Allow only numbers'
                                },
                            ],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Input,
                            label: "Make and Model",
                            name: "makeAndModel",
                            validations: [
                                {
                                    name: 'required',
                                    validator: Validators.required,
                                    message: 'This field is required!'
                                },
                            ],
                            value: this._authService.userVehicleDefaults?.makeAndModel
                        },
                        {
                            type: DynamicFieldType.Select,
                            label: "Vehicle Type",
                            name: "type",
                            options: [...this.vehicleTypes],
                            validations: [
                                {
                                    name: 'required',
                                    validator: Validators.required,
                                    message: 'This field is required!'
                                },
                            ],
                            value: this._authService.userVehicleDefaults?.type
                        },
                        {
                            type: DynamicFieldType.Select,
                            label: "Do you have a current driver licence?",
                            name: "isLicenseConsist",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [
                                {
                                    name: 'required',
                                    validator: Validators.required,
                                    message: 'This field is required!'
                                },
                            ],
                            value: ''
                        },
                        {
                            type: DynamicFieldType.Date,
                            label: "Drivers Licence Expiry Date",
                            name: "driversLicenceExpiryDate",
                            validations: [
                                {
                                    name: 'required',
                                    validator: Validators.required,
                                    message: 'This field is required!'
                                },
                            ],
                            value: this._authService.userVehicleDefaults?.licenceExpiry?.toDate()
                        },
                        {
                            type: DynamicFieldType.Input,
                            label: "Vehicle Registration",
                            name: "registration",
                            validations: [
                                {
                                    name: 'required',
                                    validator: Validators.required,
                                    message: 'This field is required!'
                                },
                                {
                                    name: 'pattern',
                                    validator: Validators.pattern(/^[a-z0-9]+$/i),
                                    message: 'Registration can only consist from letters or numbers'
                                },
                            ],
                            value: this._authService.userVehicleDefaults?.registration
                        },
                    ]
                },
                {
                    category: 'Tyres',
                    values: [
                        {
                            type: DynamicFieldType.Select,
                            label: "Tyre pressure",
                            name: "tyrePressure",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            label: "Tread depth",
                            name: "treadDepth",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "goodCondition",
                            label: "Tyres in good condition",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "spareServiceable",
                            label: "Spare serviceable",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                    ]
                },
                {
                    category: 'Brakes',
                    values: [
                        {
                            type: DynamicFieldType.Select,
                            name: "brakingPower",
                            label: "Braking Power",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "handBrake",
                            label: "Hand Brake",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        }
                    ]
                },
                {
                    category: 'Seat Belts',
                    values: [
                        {
                            type: DynamicFieldType.Select,
                            name: "tensionerFunctions",
                            label: "Tensioner Functions",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "beltsWork",
                            label: "Belts work and are in good condition",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        }
                    ]
                },
                {
                    category: 'Wipers',
                    values: [
                        {
                            type: DynamicFieldType.Select,
                            name: "wipersFunction",
                            label: "Wipers function",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "washerFluidLevel",
                            label: "Washer fluid level",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        }
                    ]
                },
                {
                    category: 'Windows & Windscreen',
                    values: [
                        {
                            type: DynamicFieldType.Select,
                            name: "windscreen",
                            label: "Windscreen",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "windows",
                            label: "Windows",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                    ]
                },
                {
                    category: 'Lights',
                    values: [
                        {
                            type: DynamicFieldType.Select,
                            name: "numberPlate",
                            label: "Number Plate",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "highBeam",
                            label: "High Beam",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "lowBeam",
                            label: "Low Beam",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "parking",
                            label: "Parking",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "reverse",
                            label: "Reverse",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "brakes",
                            label: "Brakes",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                    ]
                },
                {
                    category: 'Indicators',
                    values: [
                        {
                            type: DynamicFieldType.Select,
                            name: "indicatorFront",
                            label: "Front",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "indicatorSide",
                            label: "Side",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "indicatorRear",
                            label: "Rear",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "indicatorHazards",
                            label: "Hazards",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                    ]
                },
                {
                    category: 'Mirrors',
                    values: [
                        {
                            type: DynamicFieldType.Select,
                            name: "rearSideMirror",
                            label: "Rear/Side Mirrors",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        }
                    ]
                },
                {
                    category: 'Maintenance',
                    values: [
                        {
                            type: DynamicFieldType.Select,
                            name: "engineStarts",
                            label: 'Engine Starts 1st Time',
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "oilLevel",
                            label: 'Oil Levels',
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "waterLevel",
                            label: 'Water Levels',
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "brakeFluidLevels",
                            label: 'Brake Fluid Levels',
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "airConditioning",
                            label: 'Air Conditioning',
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "doorFunction",
                            label: 'Door Functioning',
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "strutsRams",
                            label: 'Struts and Rams',
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "reverseBeeper",
                            label: 'Reverse Beeper',
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "reverseCameraScreen",
                            label: 'Reverse Camera Screen',
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "reverseCameraCameraLens",
                            label: 'Reverse Camera Lens',
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "horn",
                            label: 'Horn',
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "isVehicleClean",
                            label: 'Vehicle Clean',
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "accidentDamage",
                            label: 'Is the vehicle free from damage?',
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "decals",
                            label: 'Decals',
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No, FormAnswerClassic.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        }
                    ]
                },
                {
                    category: 'General Items',
                    values: [
                        {
                            type: DynamicFieldType.Select,
                            name: "stickers",
                            label: "Take 5 Stickers",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "manualHandbook",
                            label: "Manual Handbook",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "firstAidKit",
                            label: "First Aid Kit (Missing Items Listed in Comments)",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "handSanitiser",
                            label: "Hand Sanitiser",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "sdsFolder",
                            label: "SDS Folder",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                    ]
                }
            ]
        },

        {
            formType: 'Pest Service Form',
            formClass: FormClass.VehicleForm,
            formId: 'v-pest-form',
            countryName: CountryName.Fiji,
            config: [
                {
                    category: 'Vehicle Defaults',
                    values: [
                        {
                            type: DynamicFieldType.Input,
                            label: "Odometer Reading",
                            name: "odometer",
                            validations: [
                                {
                                    name: 'required',
                                    validator: Validators.required,
                                    message: 'This field is required!'
                                },
                                {
                                    name: 'pattern',
                                    validator: Validators.pattern(/^[0-9]+$/),
                                    message: 'Allow only numbers'
                                },
                            ],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Input,
                            label: "Service Due (KM)",
                            name: "serviceDue",
                            validations: [
                                {
                                    name: 'required',
                                    validator: Validators.required,
                                    message: 'This field is required!'
                                },
                                {
                                    name: 'pattern',
                                    validator: Validators.pattern(/^[0-9]+$/),
                                    message: 'Allow only numbers'
                                },
                            ],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Input,
                            label: "Make and Model",
                            name: "makeAndModel",
                            validations: [
                                {
                                    name: 'required',
                                    validator: Validators.required,
                                    message: 'This field is required!'
                                },
                            ],
                            value: this._authService.userVehicleDefaults?.makeAndModel
                        },
                        {
                            type: DynamicFieldType.Select,
                            label: "Vehicle Type",
                            name: "type",
                            options: [...this.vehicleTypes],
                            validations: [
                                {
                                    name: 'required',
                                    validator: Validators.required,
                                    message: 'This field is required!'
                                },
                            ],
                            value: this._authService.userVehicleDefaults?.type
                        },
                        {
                            type: DynamicFieldType.Select,
                            label: "Do you have a current driver licence?",
                            name: "isLicenseConsist",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [
                                {
                                    name: 'required',
                                    validator: Validators.required,
                                    message: 'This field is required!'
                                },
                            ],
                            value: ''
                        },
                        {
                            type: DynamicFieldType.Date,
                            label: "Drivers Licence Expiry Date",
                            name: "driversLicenceExpiryDate",
                            validations: [
                                {
                                    name: 'required',
                                    validator: Validators.required,
                                    message: 'This field is required!'
                                },
                            ],
                            value: this._authService.userVehicleDefaults?.licenceExpiry?.toDate()
                        },
                        {
                            type: DynamicFieldType.Input,
                            label: "Vehicle Registration",
                            name: "registration",
                            validations: [
                                {
                                    name: 'required',
                                    validator: Validators.required,
                                    message: 'This field is required!'
                                },
                                {
                                    name: 'pattern',
                                    validator: Validators.pattern(/^[a-z0-9]+$/i),
                                    message: 'Registration can only consist from letters or numbers'
                                },
                            ],
                            value: this._authService.userVehicleDefaults?.registration
                        },
                    ]
                },
                {
                    category: 'Tyres',
                    values: [
                        {
                            type: DynamicFieldType.Select,
                            label: "Tyre pressure",
                            name: "tyrePressure",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            label: "Tread depth",
                            name: "treadDepth",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "goodCondition",
                            label: "Tyres in good condition",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "spareServiceable",
                            label: "Spare serviceable",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                    ]
                },
                {
                    category: 'Brakes',
                    values: [
                        {
                            type: DynamicFieldType.Select,
                            name: "brakingPower",
                            label: "Braking Power",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "handBrake",
                            label: "Hand Brake",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        }
                    ]
                },
                {
                    category: 'Seat Belts',
                    values: [
                        {
                            type: DynamicFieldType.Select,
                            name: "tensionerFunctions",
                            label: "Tensioner Functions",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "beltsWork",
                            label: "Belts work and are in good condition",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        }
                    ]
                },
                {
                    category: 'Wipers',
                    values: [
                        {
                            type: DynamicFieldType.Select,
                            name: "wipersFunction",
                            label: "Wipers function",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "washerFluidLevel",
                            label: "Washer fluid level",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        }
                    ]
                },
                {
                    category: 'Windows & Windscreen',
                    values: [
                        {
                            type: DynamicFieldType.Select,
                            name: "windscreen",
                            label: "Windscreen",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "windows",
                            label: "Windows",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                    ]
                },
                {
                    category: 'Lights',
                    values: [
                        {
                            type: DynamicFieldType.Select,
                            name: "numberPlate",
                            label: "Number Plate",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "highBeam",
                            label: "High Beam",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "lowBeam",
                            label: "Low Beam",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "parking",
                            label: "Parking",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "reverse",
                            label: "Reverse",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "brakes",
                            label: "Brakes",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                    ]
                },
                {
                    category: 'Indicators',
                    values: [
                        {
                            type: DynamicFieldType.Select,
                            name: "indicatorFront",
                            label: "Front",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "indicatorSide",
                            label: "Side",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "indicatorRear",
                            label: "Rear",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "indicatorHazards",
                            label: "Hazards",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                    ]
                },
                {
                    category: 'Mirrors',
                    values: [
                        {
                            type: DynamicFieldType.Select,
                            name: "rearSideMirror",
                            label: "Rear/Side Mirrors",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        }
                    ]
                },
                {
                    category: 'Maintenance',
                    values: [
                        {
                            type: DynamicFieldType.Select,
                            name: "engineStarts",
                            label: 'Engine Starts 1st Time',
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "oilLevel",
                            label: 'Oil Levels',
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "waterLevel",
                            label: 'Water Levels',
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "brakeFluidLevels",
                            label: 'Brake Fluid Levels',
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "airConditioning",
                            label: 'Air Conditioning',
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "gpsSticker",
                            label: 'GPS Sticker',
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "heightLevel",
                            label: 'Height Label',
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "doorFunction",
                            label: 'Door Functioning',
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "strutsRams",
                            label: 'Struts and Rams',
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "reverseBeeper",
                            label: 'Reverse Beeper',
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "reverseCameraScreen",
                            label: 'Reverse Camera Screen',
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "reverseCameraCameraLens",
                            label: 'Reverse Camera Lens',
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "horn",
                            label: 'Horn',
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "isVehicleClean",
                            label: 'Vehicle Clean',
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "accidentDamage",
                            label: 'Is the vehicle free from damage?',
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "decals",
                            label: 'Decals',
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        }
                    ]
                },
                {
                    category: 'Other',
                    values: [
                        {
                            type: DynamicFieldType.Select,
                            name: "tankUnitFixed",
                            label: 'Tank Unit Fixed',
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "spillTrayFitted",
                            label: 'Spill Tray Fitted',
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "hoseConnectorsSecure",
                            label: 'Hose Connectors Secure',
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                    ]
                },
                {
                    category: 'General Items',
                    values: [
                        {
                            type: DynamicFieldType.Select,
                            name: "stickers",
                            label: "Take 5 Stickers",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "manualHandbook",
                            label: "Manual Handbook",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "fireExtinguisher",
                            label: "Fire Extinguisher",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "extinguisherServiced",
                            label: "Extinguisher Serviced",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "extinguisherAccessible",
                            label: "Extinguisher Accessible",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "firstAidKit",
                            label: "First Aid Kit (Missing Items Listed in Comments)",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "handSanitiser",
                            label: "Hand Sanitiser",
                            options: [FormAnswerClassic.Yes, FormAnswerClassic.No],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "sdsFolder",
                            label: "SDS Folder",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        }
                    ]
                },
                {
                    category: 'PPE',
                    values: [
                        {
                            type: DynamicFieldType.Select,
                            name: "pvcGloves",
                            label: "PVC Gloves",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "latexGloves",
                            label: "Latex Gloves",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "earMuffs",
                            label: "Ear Muffs ",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "safetyGlasses",
                            label: "Safety Glasses",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "faceShield",
                            label: "Face Shield",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "safetyBoots",
                            label: "Safety Boots",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "safetyVest",
                            label: "Safety Vest",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "hardHat",
                            label: "Hard Hat / Bump Cap",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "spareUniform",
                            label: "Spare Uniform",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "beeHood",
                            label: "Bee Hood",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "insectRepellent",
                            label: "Insect Repellent",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "overalls",
                            label: "Overalls (Two Pairs)",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "kneePads",
                            label: "Knee Pads",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "subfloorEntrySign",
                            label: "Subfloor Entry Sign",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "paperTowelDispensor",
                            label: "Paper Towel Dispensor ",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "sunscreen",
                            label: "Sunscreen 30+",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "spareSafetyBoots",
                            label: "Spare Safety Boots",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "faceMaskAndRespirator",
                            label: "Face Mask And Respirator",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "faceMaskCartridges",
                            label: "Face Mask Cartridges",
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        }
                    ]
                },
                {
                    category: 'Spill Kit',
                    values: [
                        {
                            type: DynamicFieldType.Select,
                            name: "spillKitStocked",
                            label: 'Spill Kit Stocked',
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        }
                    ]
                },
                {
                    category: 'Dangerous Goods',
                    values: [
                        {
                            type: DynamicFieldType.Select,
                            name: "dangerousGoodsStoredCorrectly",
                            label: 'Dangerous Goods Stored Correctly',
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "chemicalTreatmentSigns",
                            label: 'Chemical Treatment Signs',
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                    ]
                },
                {
                    category: 'Other',
                    values: [
                        {
                            type: DynamicFieldType.Select,
                            name: "extensionLadder",
                            label: 'Extension Ladder 2.4m',
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "ladder",
                            label: 'Ladder 1.8m',
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "rcdSafetySwitch",
                            label: 'RCD Safety Switch',
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "extensionCord",
                            label: 'Extension Cord',
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "waterBottle",
                            label: '20L Water Bottle',
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                        {
                            type: DynamicFieldType.Select,
                            name: "soapDispenser",
                            label: 'Soap Dispenser',
                            options: [FormAnswerSecondary.Safe, FormAnswerSecondary.NotSafe, FormAnswerSecondary.NA],
                            validations: [{
                                name: 'required',
                                validator: Validators.required,
                                message: 'This field is required!'
                            }],
                            value: ""
                        },
                    ]
                },
            ]
        },
    ];

    constructor(
        private _authService: AuthService
    ) {
        this.formTypes = Array.from(new Set(this.setFormTypes()));
    }

    /**
     * Set form types
     */
    setFormTypes() {
        return this.formsConfig.map(form => form.formType)
    }

    /**
     * Get form config by id
     * @param id
     */
    getFormConfigById(id: string): formDynamic {
        return this.formsConfig.find(form => form.formId === id);
    }

    /**
     * Get form config by id and country
     * @param id
     * @param countryName
     */
    getFormConfigByIdAndCountry(id: string, countryName: CountryName): formDynamic {
        return this.formsConfig.filter(form => form.countryName.toLowerCase() === countryName).find(form => form.formId === id);
    }

    /**
     * Get form type by id
     * @param id {string}
     */
    getFormTypeById(id: string): string {
        return this.formsConfig.find(form => form.formId === id).formType;
    }

    /**
     * Get form class by id
     * @param id {string}
     */
    getFormClassById(id: string): FormClass {
        return this.formsConfig.find(form => form.formId === id).formClass;
    }

    /**
     * Filter forms config by country
     * @param countryName
     */
    getFormsByCountry(countryName: string) {
        return this.formsConfig.filter(form => form.countryName.toLowerCase() === countryName);
    }

    /**
     * Filter branches by country
     * @param countryName
     */
    getBranchesByCountry(countryName: string) {
        return this.branchGroups.find(branchGroup=> branchGroup.branchCategory.toLowerCase() === countryName).branches;
    }

}
