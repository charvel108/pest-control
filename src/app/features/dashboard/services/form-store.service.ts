import { Injectable } from '@angular/core';
import { BehaviorSubject } from "rxjs";
import { FieldGroup } from "../models/form.interface";
import { ActionDto } from "../models/action.interface";
import {GeneralDetailsNewDto} from "../models/general-details-new.model";

@Injectable()
export class FormStoreService {

  /**@type {BehaviorSubject<GeneralDetailsNewDto>} */
  generalDetails$: BehaviorSubject<GeneralDetailsNewDto> = new BehaviorSubject<GeneralDetailsNewDto>(null);

  /**@type {BehaviorSubject<FieldGroup[]>} */
  mainFields$: BehaviorSubject<FieldGroup[]> = new BehaviorSubject<FieldGroup[]>(null);

  /**@type {BehaviorSubject<ActionDto[]>} */
  actionsList$: BehaviorSubject<ActionDto[]> = new BehaviorSubject<ActionDto[]>(null);


  constructor() { }

  /**
   * Get value from general details stream
   */
  get generalDetailsValue() {
    return this.generalDetails$.getValue();
  }

  /**
   * Emit new value to general details stream
   * @param generalDetails {GeneralDetailsNewDto}
   */
  setGeneralDetails(generalDetails: GeneralDetailsNewDto) {
    this.generalDetails$.next(generalDetails);
  }

  /**
   * Get value from main fields stream
   */
  get mainFieldsValue () {
    return this.mainFields$.getValue();
  }

  /**
   * Emit new value to main fields stream
   * @param mainFields
   */
  setMainFields(mainFields: FieldGroup[]) {
    this.mainFields$.next(mainFields);
  }

  /**
   * Emit new value to actions list stream
   * @param actionsList {ActionDto[]}
   */
  setActionsList(actionsList: ActionDto[]) {
    this.actionsList$.next(actionsList)
  }

  /**
   * Get actions list value
   */
  get actionsListValue() {
    return this.actionsList$.getValue();
  }

}
