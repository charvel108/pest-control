import {Injectable} from '@angular/core';
import {DataStorageService} from "../../../shared/services/data-storage.service";
import {map} from "rxjs/operators";
import {Comment} from "../models/comment.model";
import {NotificationService} from "../../../shared/services/notification.service";
import {RequestDto, RequestTableDto} from "../models/request.model";
import {Router} from "@angular/router";
import {ActionRequestDto, CriticalActionDto, CriticalActionStatus} from "../models/action.interface";
import {Observable} from "rxjs";

@Injectable()
export class RequestsService {
    constructor(
        private _dataStorageService: DataStorageService,
        private _router: Router,
        private _notificationService: NotificationService,
    ) {}

    /**
     * Get table requests from database
     */
    getTableRequests() {
        return this._dataStorageService.getTableRequests().pipe(
            map(tableRequests => tableRequests.map(tableRequest => tableRequest.payload.doc.data() as RequestTableDto))
        );
    }

    /**
     * Get requests from database
     */
    getRequests() {
        return this._dataStorageService.getRequests().pipe(
            map(requests => requests.map(request => {
                const data = request.payload.doc.data() as RequestDto;
                const id = request.payload.doc.id;
                return  {id, ...data};
            }))
        );
    }

    /**
     * Get request by id from database
     * @param id {string}
     */
    getRequest(id: string) {
        return this._dataStorageService.getRequest(id)
            .pipe(
                map(request => {
                    const data = request.payload.data() as RequestDto;
                    const id = request.payload.id;
                    if (data) {
                        return {id, ...data};
                    }
                })
            )
    }

    /**
     * Approve request by id
     * @param id {string}
     */
    async approveRequest(id: string) {
        try {
            await this._dataStorageService.approveRequest(id);
            await this._dataStorageService.approveTableRequest(id);
            this._notificationService.openCustomMessageSnackbar('Request successfully approved.', 'Close');
        } catch (e) {
            this._notificationService.openCustomMessageSnackbar('Something went wrong', 'Close');
        }
    }

    /**
     * Decline request by id. Change status in requests and table requests
     * @param id {string}
     */
    async declineRequest(id: string) {
        try {
            await this._dataStorageService.declineRequest(id);
            await this._dataStorageService.declineTableRequest(id);
            this._notificationService.openCustomMessageSnackbar('Request successfully declined.', 'Close');
        } catch (e) {
            this._notificationService.openCustomMessageSnackbar('Something went wrong', 'Close');
        }
    }

    /**
     * Create request in database
     * @param request {RequestDto}
     */
    async createRequest(request?: RequestDto) {
        const reqResponse = await this._dataStorageService.storeRequest(request);
        await this._notificationService.openCustomMessageSnackbar('Request successfully created.', 'Close');
        this._notificationService.isLoading = false;
        return reqResponse;
    }

    /**
     * Create table request
     * @param tableRequest {RequestTableDto}
     * @param id {string}
     */
    async createTableRequest(tableRequest: RequestTableDto, id: string) {
        await this._dataStorageService.storeTableRequest(tableRequest, id);
    }

    /**
     * Add comment to database
     * @param id {string}
     * @param comments {Comment[]}
     */
    addComment(id: string, comments: Comment[]) {
        this._dataStorageService.addComment(id, comments)
            .then(() => {
                this._dataStorageService.getRequest(id);
                this._notificationService.openCustomMessageSnackbar('Comment successfully created.', 'Close');
            })
            .catch((err) => console.log(err));
    }

    /**
     * Get critical list from database
     */
    getCriticalList(): Observable<CriticalActionDto[]> {
        return this._dataStorageService.getCriticalList().pipe(
            map(criticalList => criticalList.map(criticalAction => criticalAction.payload.doc.data() as CriticalActionDto))
        );
    }

    /**
     * Add critical action to database
     * @param id {string}
     * @param criticalAction {CriticalActionDto}
     */
    async addCriticalAction(id: string, criticalAction: CriticalActionDto) {
        try {
            await this._dataStorageService.addToCriticalList(id, criticalAction);
        } catch (error) {
            this._notificationService.openCustomMessageSnackbar('Something went wrong', 'Close')
        }
    }

    /**
     * Update Actions List in database
     * @param requestId {string}
     * @param actionId {string}
     * @param actionsList {ActionRequestDto[]}
     */
    async updateActionsList(requestId: string, actionId: string, actionsList: ActionRequestDto[]) {
        try {
            await this._dataStorageService.updateActionsList(requestId, actionId, actionsList);
            this._notificationService.openCustomMessageSnackbar('Answer was successfully updated!', 'Close');
        } catch (error) {
            this._notificationService.openCustomMessageSnackbar('Something went wrong', 'Close')
        }
    }

    async updateRequestId(id: string) {
        try {
            await this._dataStorageService.updateRequestId(id);
        } catch (error) {
            this._notificationService.openCustomMessageSnackbar('Something went wrong', 'Close')
        }
    }

    /**
     * Update critical action in database
     * @param requestId {string}
     * @param comment {Comment}
     * @param criticalActionId {string}
     * @param criticalActionStatus {CriticalActionStatus}
     */
    async updateCriticalAction(requestId: string, comment: Comment, criticalActionId: string, criticalActionStatus: CriticalActionStatus) {
        try {
            await this._dataStorageService.updateCriticalActionStatus(criticalActionId, criticalActionStatus);
            await this._dataStorageService.addNewComment(requestId, comment);
            this._notificationService.openCustomMessageSnackbar('Critical Action was successfully updated', 'Close');
        } catch (e) {
            this._notificationService.openCustomMessageSnackbar('Something went wrong', 'Close');
        }
    }

    async updateRequestApprover(requestId: string, newApprover: string) {
        await this._dataStorageService.updateTableRequestApprover(requestId, newApprover);
        await this._dataStorageService.updateRequestApprover(requestId, newApprover)
            .then(() => this._notificationService.openCustomMessageSnackbar('Approver was successfully updated', 'Close'))
            .catch((err) => this._notificationService.openCustomMessageSnackbar('Something went wrong', 'Close'))
        ;
    }

    async addNewComment(requestId: string, comment: Comment) {
        await this._dataStorageService.addNewComment(requestId, comment);
    }

    async updateCriticalListItemApprover(criticalActionId: string, newApprover: string) {
        try {
            await this._dataStorageService.updateCriticalActionApprover(criticalActionId, newApprover);
            this._notificationService.openCustomMessageSnackbar('Approver was successfully updated', 'Close');
        } catch (error) {
            this._notificationService.openCustomMessageSnackbar('Something went wrong', 'Close')
        }
    }
}
