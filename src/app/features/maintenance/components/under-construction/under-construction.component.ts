import {Component} from '@angular/core';

@Component({
    selector: 'app-under-construction',
    template: `
        <div class="under-construction">
            <img class="under-construction__img" src="assets/under-construction.svg" alt="Under construction">
            <div class="under-construction__content">
                <div class="container container--lg">
                    <div class="row justify-content-center justify-content-xl-end">
                        <div class="col-lg-10 col-xl-7">
                            <h1 class="under-construction__title">Sorry! We are under scheduled <span
                                    class="text--orange">maintenance</span></h1>
                            <p class="under-construction__text">Our website is currently undergoing scheduled
                                maintenance, will be right back soon. Thank for your patience.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    `,
    styleUrls: ['./under-construction.component.scss']
})
export class UnderConstructionComponent {
}
