import {RouterModule, Routes} from "@angular/router";
import {NgModule} from "@angular/core";
import {UnderConstructionComponent} from "./components/under-construction/under-construction.component";

const routes: Routes = [
    {
        path: '',
        component: UnderConstructionComponent,
    },
    {
        path: '**', redirectTo: ''
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})

export class MaintenanceRoutingModule {
}
