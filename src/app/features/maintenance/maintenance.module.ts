import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {UnderConstructionComponent} from "./components/under-construction/under-construction.component";
import {MaintenanceRoutingModule} from "./maintenance-routing.module";

@NgModule({
  declarations: [
    UnderConstructionComponent
  ],
  imports: [
    CommonModule,
    MaintenanceRoutingModule
  ]
})
export class MaintenanceModule { }
