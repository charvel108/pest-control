import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {AuthService} from "../../../core/services/auth.service";
import { Observable } from "rxjs";
import { User } from "../../../features/dashboard/models/user.model";

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})

export class HeaderComponent implements OnInit {

  /**@type {EventEmitter<boolean>} */
  @Output() updateMenu = new EventEmitter<boolean>();

  /**@type {Observable<User>} */
  currentUser$: Observable<User>;


  constructor(public authService: AuthService) {}

  ngOnInit() {
    this.currentUser$ = this.authService.currentUser$;
  }

  /**
   * On menu change trigger
   */
  onMenuChange() {
    this.updateMenu.emit(true);
  }

  /**
   * Method for trigger auth service method signOut
   */
  async onLogout() {
    await this.authService.signOut();
  }

}
