import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {AbstractControl, FormBuilder, FormGroup, ValidatorFn, Validators} from "@angular/forms";
import {ConstantsService} from "../../services/constants.service";
import {AuthService} from "../../../core/services/auth.service";

@Component({
    selector: 'app-modal-change-approver',
    templateUrl: './modal-change-approver.component.html',
    styleUrls: ['./modal-change-approver.component.css']
})
export class ModalChangeApproverComponent implements OnInit {

    approverForm: FormGroup;
    approver: string;

    constructor(
        private _fb: FormBuilder,
        public dialogRef: MatDialogRef<ModalChangeApproverComponent>,
        private _constantsService: ConstantsService,
        private _authService: AuthService,
        @Inject(MAT_DIALOG_DATA) public data: ModalChangeApproverComponent
    ) {
        this.approver = data.approver;
    }

    ngOnInit(): void {
        this.approverForm = this._fb.group({
            approver: [this.approver, [
                Validators.required,
                Validators.email,
                Validators.pattern(new RegExp(`(^[A-Za-z0-9._%+-]+${this._constantsService.DOMAIN_ADDRESS})|(^[A-Za-z0-9._%+-]+${this._constantsService.DOMAIN_ADDRESS_ADDITIONAL})`)),
                this.restrictPersonalUserEmailValidator()
            ]],
        });
    }

    /**
     * Method for close modal with value "true"
     */
    onSubmit(): void {
        if (this.approverForm.invalid) {
            return;
        } else {
            this.dialogRef.close(this.approverForm.value?.approver.trim().toLowerCase());
        }
    }

    /**
     * Method for close modal with value "false"
     */
    onDismiss(): void {
        this.dialogRef.close(false);
    }

    // Custom validator for exclude user email as approver
    private restrictPersonalUserEmailValidator(): ValidatorFn {
        return (control: AbstractControl): { [key: string]: any } | null => {
            return control.value.toLowerCase() === this._authService?.currentUser?.email ? { restrictPersonalUserEmailValidator: control.value } : null;
        }
    }


}
