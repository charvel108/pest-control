import { Component, OnDestroy, OnInit } from '@angular/core';

interface Link {
  title;
  route;
  icon;
}

@Component({
  selector: 'app-nav-menu',
  templateUrl: './nav-menu.component.html',
  styleUrls: ['./nav-menu.component.scss']
})
export class NavMenuComponent {

  /**@type {Link[]} */
  links: Link[] = [
    { title: 'Create Form', route: 'forms', icon: 'dashboard' },
    { title: 'Requests', route: 'requests', icon: 'folder' },
    { title: 'Critical List', route: 'critical-list', icon: 'list' }
  ];

}
