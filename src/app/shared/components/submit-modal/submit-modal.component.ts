import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from "@angular/material/dialog";

@Component({
  selector: 'app-submit-modal',
  templateUrl: './submit-modal.component.html',
  styleUrls: ['./submit-modal.component.scss']
})
export class SubmitModalComponent {

  constructor(
    public dialogRef: MatDialogRef<SubmitModalComponent>,
    @Inject(MAT_DIALOG_DATA) public data: SubmitModalComponent
  ) { }

  /**
   * Method for close modal with value "true"
   */
  onSubmit(): void {
    this.dialogRef.close(true);
  }

  /**
   * Method for close modal with value "false"
   */
  onDismiss(): void {
    this.dialogRef.close(false);
  }

}
