import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Observable } from "rxjs";
import { User } from "../../../features/dashboard/models/user.model";

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']
})
export class UserComponent {

  /**@type {Observable<User>} */
  @Input() currentUser: Observable<User>;

  /**@type {EventEmitter} */
  @Output() onClick = new EventEmitter();

  /**@type {boolean} */
  isOpen:boolean = false;


  /**
   * Close menu method
   */
  closeMenu() {
    this.isOpen = false;
  }

  /**
   * Toggle menu method
   */
  toggleMenu() {
    this.isOpen = !this.isOpen;
  }

}
