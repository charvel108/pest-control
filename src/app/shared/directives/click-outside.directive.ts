import { Directive, ElementRef, EventEmitter, HostListener, Output } from '@angular/core';

@Directive({
  selector: '[clickOutside]'
})
export class ClickOutsideDirective {

  constructor(
      private _elementRef: ElementRef
  ) {
  }

  /**@type {EventEmitter} */
  @Output()
  public clickOutside = new EventEmitter();

  /**
   * Check if we click to outside of element
   * @param targetElement
   */
  @HostListener('document:click', ['$event.target'])
  public onClick(targetElement) {
    const clickedInside = this._elementRef.nativeElement.contains(targetElement);
    if (!clickedInside) {
      this.clickOutside.emit(null);
    }
  }

}
