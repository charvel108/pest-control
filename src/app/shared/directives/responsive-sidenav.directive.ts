import { Directive, Input, OnDestroy, OnInit } from '@angular/core';
import { Subject } from "rxjs";
import { NavigationEnd, Router } from "@angular/router";
import { BreakpointObserver } from "@angular/cdk/layout";
import { MatSidenav } from "@angular/material/sidenav";
import { filter, map, takeUntil } from "rxjs/operators";

@Directive({
  selector: '[permanentAt]'
})
export class ResponsiveSidenavDirective implements OnInit, OnDestroy {

  /**@type {Subject<boolean>} */
  private readonly _destroy$: Subject<boolean> = new Subject<boolean>();

  /**@type {boolean} */
  @Input() canOpen = () => true;

  /**@type {number} */
  @Input() permanentAt: number;


  constructor(
      private readonly _router: Router,
      private readonly _breakpoint: BreakpointObserver,
      private readonly _sidenav: MatSidenav
  ) {
  }

  ngOnInit(): void {
    // Stream with matching media
    const permanent$ = this._breakpoint
        .observe(`(min-width: ${this.permanentAt}px)`)
        .pipe(
            takeUntil(this._destroy$),
            map(({ matches }) => {
                  return matches
                }
            )
        );

    // Change side nav state
    permanent$.subscribe(permanent => {
      this._sidenav.mode = permanent ? "side" : "over";
      this._sidenav.opened = permanent && this.canOpen();
    });

    this._router.events
        .pipe(
            takeUntil(this._destroy$),
            filter(() => this._sidenav.mode === "over"),
            filter(event => event instanceof NavigationEnd)
        )
        .subscribe(() => {
          this._sidenav.opened = false;
        });

  }

  ngOnDestroy() {
    this._destroy$.next();
    this._destroy$.complete();
  }

}
