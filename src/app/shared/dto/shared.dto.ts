// Here we leave some shared DTO's (data transfer object)

import {CountryName} from "../enums/shared.enums";

export interface CountryDto {
    countryName: CountryName;
    countryImageLink: string;
    countryRouteLink: string;
}