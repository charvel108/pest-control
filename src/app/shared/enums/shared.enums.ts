export enum VehicleType {
    Van = "Van",
    Truck = "Truck",
    Ute = "Ute",
    Car = "Car",
    Other = "Other"
}

export enum BranchName {
    Albany = 'Albany',
    Auckland = 'Auckland',
    Blenheim = 'Blenheim',
    Christchurch = 'Christchurch',
    Cromwell = 'Cromwell',
    Dunedin = 'Dunedin',
    Hamilton = 'Hamilton',
    Queenstown ='Queenstown',
    Napier = 'Napier',
    Nelson = 'Nelson',
    NewPlymouth = 'New Plymouth',
    PalmerstonNorth = 'Palmerston North',
    Rotorua = 'Rotorua',
    Timaru = 'Timaru',
    Wellington = 'Wellington',
    Whangarei = 'Whangarei',
    Adelaide = "Adelaide",
    Albury = "Albury",
    Brisbane = "Brisbane",
    Canberra = "Canberra",
    Marulan = "Marulan",
    Melbourne = "Melbourne",
    Newcastle = "Newcastle",
    NorthQueensland = "North Queensland",
    Perth = "Perth",
    Sydney = "Sydney",
    Tasmania = "Tasmania",
    DarlingDowns = "Darling Downs",
    Suva = "Suva",
    Lautoka = "Lautoka",
    Darwin = "Darwin",
}

export enum RequestStatus {
    New = "New",
    Approved = "Approved",
    Declined = "Declined"
}

export enum FormClass {
    VehicleForm = "VehicleForm"
}

export enum CountryName {
    Australia = 'Australia',
    NewZealand = 'New Zealand',
    Fiji = "Fiji"
}

export enum FormAnswerClassic {
    Yes = 'Yes',
    No = 'No',
    NA = 'N/A'
}

export enum FormAnswerSecondary {
    Safe = "Safe",
    NotSafe = "Not Safe",
    NA = "N/A"
}

export enum StorageKeys {
    RequestFilters = 'requestFilters',
    CriticalListFilters = 'criticalListFilters'
}
