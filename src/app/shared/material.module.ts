import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatToolbarModule } from "@angular/material/toolbar";
import { MatFormFieldModule } from "@angular/material/form-field";
import { MatInputModule } from "@angular/material/input";
import { MatDatepickerModule } from "@angular/material/datepicker";
import { MatNativeDateModule } from "@angular/material/core";
import { MatStepperModule } from "@angular/material/stepper";
import { MatButtonModule } from "@angular/material/button";
import { MatButtonToggleModule } from "@angular/material/button-toggle";
import { MatCardModule } from "@angular/material/card";
import { MatSidenavModule } from "@angular/material/sidenav";
import { MatListModule } from "@angular/material/list";
import { MatIconModule } from "@angular/material/icon";
import { MatTableModule } from "@angular/material/table";
import { MatSortModule } from "@angular/material/sort";
import { MatMenuModule } from "@angular/material/menu";
import { MatProgressBarModule } from "@angular/material/progress-bar";
import { MatSnackBarModule } from "@angular/material/snack-bar";
import { MatTabsModule } from "@angular/material/tabs";
import { MatSelectModule } from "@angular/material/select";
import { MatProgressSpinnerModule } from "@angular/material/progress-spinner";
import { MatRadioModule } from "@angular/material/radio";
import { MatPaginatorModule } from "@angular/material/paginator";
import { MatCheckboxModule } from "@angular/material/checkbox";
import { MatExpansionModule } from "@angular/material/expansion";
import { FileInputConfig, MaterialFileInputModule, NGX_MAT_FILE_INPUT_CONFIG } from "ngx-material-file-input";
import { MatDialogModule } from "@angular/material/dialog";
import { MatChipsModule } from '@angular/material/chips';

const modules: any[] = [
  CommonModule,
  MatToolbarModule,
  MatFormFieldModule,
  MatInputModule,
  MatDatepickerModule,
  MatNativeDateModule,
  MatStepperModule,
  MatButtonModule,
  MatButtonToggleModule,
  MatCardModule,
  MatSidenavModule,
  MatListModule,
  MatIconModule,
  MatTableModule,
  MatSortModule,
  MatMenuModule,
  MatProgressBarModule,
  MatSnackBarModule,
  MatSelectModule,
  MatProgressSpinnerModule,
  MatTabsModule,
  MatRadioModule,
  MatCardModule,
  MatPaginatorModule,
  MatCheckboxModule,
  MatExpansionModule,
  MaterialFileInputModule,
  MatDialogModule,
  MatChipsModule,
];

export const config: FileInputConfig = {
  sizeUnit: 'Octet'
};


@NgModule({
  declarations: [],
  imports: [...modules],
  exports: modules,
  providers: [
    {
      provide: NGX_MAT_FILE_INPUT_CONFIG,
      useValue: config
    }
  ]
})
export class MaterialModule {
}
