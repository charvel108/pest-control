import { Pipe, PipeTransform } from "@angular/core";

@Pipe({
  name: 'localDate'
})
export class LocalDatePipe implements PipeTransform {
  transform(value: any, args?: any): string {
    return value?.toDate().toLocaleDateString();
  }
}
