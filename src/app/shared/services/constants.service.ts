import {Injectable} from '@angular/core';
import {CountryName, VehicleType} from "../enums/shared.enums";

@Injectable({
  providedIn: 'root'
})
export class ConstantsService {

  /**@type { CountryName[] } */
  readonly countriesList: CountryName[] = [
    CountryName.Australia,
    CountryName.NewZealand,
    CountryName.Fiji
  ];

  /**@type { VehicleType[] } */
  readonly vehicleTypes: VehicleType[] = [
    VehicleType.Van,
    VehicleType.Truck,
    VehicleType.Ute,
    VehicleType.Car,
    VehicleType.Other
  ];

  /**
   * There are domain constants allowed to signIn and setting approver
   * @type {string}
   */
  readonly DOMAIN_ADDRESS: string = '@rentokil.com';
  readonly DOMAIN_ADDRESS_ADDITIONAL: string = '@rentokil-initial.com';
  readonly DEVELOPMENT_EMAIL: string = 'heuristicer@gmail.com';

  constructor() { }
}
