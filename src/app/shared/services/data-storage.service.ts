import {Injectable} from '@angular/core';
import {AngularFirestore} from "@angular/fire/firestore";
import {RequestDto, RequestTableDto} from "../../features/dashboard/models/request.model";
import {Comment} from "../../features/dashboard/models/comment.model";
import {User} from "../../features/dashboard/models/user.model";
import {map} from "rxjs/operators";
import {Observable} from "rxjs";
import {
    ActionRequestDto,
    CriticalActionDto,
    CriticalActionStatus
} from "../../features/dashboard/models/action.interface";
import firebase from "firebase/app";
import firestore = firebase.firestore;

@Injectable({
    providedIn: 'root'
})
export class DataStorageService {

    constructor(private readonly _firestore: AngularFirestore) {}

    // Requests

    /**
     * Store request in database
     * @param request {RequestDto}
     */
    storeRequest(request: RequestDto) {
        return this._firestore
            .collection("requests")
            .add(request);
    }

    updateRequestId(id: string) {
        return this._firestore.doc('requests/' + id)
            .update({
                id
            })
    }

    /**
     * Store table request in database
     * @param tableRequest {RequestTableDto}
     * @param id {string}
     */
    storeTableRequest(tableRequest: RequestTableDto, id: string) {
        return this._firestore
            .collection("table-requests")
            .doc(id)
            .set(tableRequest)
    }

    /**
     * Get table requests form database
     */
    getTableRequests() {
        return this._firestore
            .collection('table-requests')
            .snapshotChanges()
    }

    /**
     * Get requests form database
     */
    getRequests() {
        return this._firestore
            .collection('requests')
            .snapshotChanges()
    }

    /**
     * Get request by id
     * @param id {string}
     */
    getRequest(id: string) {
        return this._firestore
            .collection('requests')
            .doc(id)
            .snapshotChanges()
    }

    /**
     * Get critical list from database
     */
    getCriticalList() {
        return this._firestore
            .collection('critical-list')
            .snapshotChanges()
    }

    /**
     * Get the users list
     */
    getUsersList() {
        return this._firestore
            .collection('users')
            .snapshotChanges()
    }

    /**
     * Approve request in database
     * @param id {string}
     */
    approveRequest(id: string) {
        return this._firestore.doc('requests/' + id)
            .update({status: "Approved"});
    }

    /**
     * Approve table request by id
     * @param id {string}
     */
    approveTableRequest(id: string) {
        return this._firestore.doc('table-requests/' + id)
            .update({status: "Approved"});
    }

    /**
     * Decline request by id
     * @param id {string}
     */
    declineRequest(id: string) {
        return this._firestore.doc('requests/' + id)
            .update({status: "Declined"});
    }

    /**
     * Decline table request by id
     * @param id {string}
     */
    declineTableRequest(id: string) {
        return this._firestore.doc('table-requests/' + id)
            .update({status: "Declined"});
    }

    /**
     * Update actions list in database
     * @param requestId {string}
     * @param actionId {string}
     * @param actionsList {ActionRequestDto[]}
     */
    updateActionsList(requestId: string, actionId: string, actionsList: ActionRequestDto[]) {
        return this._firestore.doc('requests/' + requestId)
            .update({
                "criticalActions": actionsList
            })
    }


    /**
     * Add item to critical list
     * @param id {string}
     * @param criticalAction {CriticalActionDto}
     */
    addToCriticalList(id: string, criticalAction: CriticalActionDto) {
        return this._firestore.doc('critical-list/' + id).set(criticalAction);
    }

    /**
     * Update critical action status
     * @param criticalItemId {string}
     * @param newStatus {CriticalActionStatus}
     */
    updateCriticalActionStatus(criticalItemId: string, newStatus: CriticalActionStatus) {
        return this._firestore.doc('critical-list/' + criticalItemId)
            .update({
                "status": newStatus
            })
    }

    /**
     * Update critical action approver
     * @param criticalItemId {string}
     * @param newApprover {string}
     */
    updateCriticalActionApprover(criticalItemId: string, newApprover: string) {
        return this._firestore.doc('critical-list/' + criticalItemId)
            .update({
                "approver": newApprover
            })
    }

    // Comments

    /**
     * Add comment to database from request
     * @param id {string}
     * @param comments {Comment[]}
     */
    addComment(id: string, comments: Comment[]) {
        return this._firestore.doc('requests/' + id)
            .set({comments: comments}, {merge: true});
    }

    /**
     * Add new comment to database from critical item
     * @param requestId {string}
     * @param comment {Comment}
     */
    addNewComment(requestId: string, comment: Comment) {
        return this._firestore.doc('requests/' + requestId)
            .update({
                "comments": firestore.FieldValue.arrayUnion(comment)
            })
    }

    // Users

    /**
     * Add new user to database
     * @param newUser {User}
     */
    addUser(newUser: User) {
        return this._firestore.collection('users')
            .doc(newUser.uid)
            .set(newUser);
    }

    /**
     * Get user by id
     * @param uid {string}
     */
    getUser(uid: string): Observable<User> {
        return this._firestore.collection<User>('users')
            .doc(uid)
            .snapshotChanges()
            .pipe(map(response => response.payload.data() as User))
    }

    /**
     * Update user
     * @param updatedUser
     */
    updateUser(updatedUser) {
        return this._firestore.collection('users')
            .doc(updatedUser.uid)
            .set(updatedUser, {merge: true})
    }

    /**
     * Update google data from provider field
     * @param uid {string}
     * @param displayName {string}
     * @param email {string}
     * @param phoneNumber {string}
     * @param photoURL {string}
     */
    updateGoogleData(uid: string, displayName: string, email: string, phoneNumber: string, photoURL: string) {
        return this._firestore.doc('users/' + uid)
            .update({
                displayName,
                email,
                phoneNumber,
                photoURL
            })
    }

    /**
     *
     * @param requestId {string}
     * @param newApprover {string}
     */
    updateRequestApprover(requestId: string, newApprover: string) {
        return this._firestore.doc('requests/' + requestId)
            .update({'requestDetails.approver': `${newApprover}`});
    }

    /**
     *
     * @param requestId {string}
     * @param newApprover {string}
     */
    updateTableRequestApprover(requestId: string, newApprover: string) {
        return this._firestore.doc('table-requests/' + requestId)
            .update({approver: newApprover});
    }

    /**
     * Check if document exists in collection
     * @param collection
     * @param documentId
     */
    async isDocumentExists(collection: string, documentId: string) {
        return await this._firestore.doc(`${collection}/${documentId}`).get().toPromise()
            .then(doc => {
               return doc.exists;
            });
    }

}

