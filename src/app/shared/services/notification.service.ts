import { Injectable } from '@angular/core';
import { MatSnackBar } from "@angular/material/snack-bar";

@Injectable({
  providedIn: 'root'
})
export class NotificationService {

  /**@type {boolean} */
  isLoading: boolean = false;

  /**@type {boolean} */
  isLoadingLocal: boolean = false;

  constructor(public _snackBar: MatSnackBar) {
  }

  /**
   * Open notification snackbar with message, action and duration, by default duration 3seconds
   * @param message
   * @param action
   * @param duration
   */
  openCustomMessageSnackbar(message: string, action: string, duration?: number) {
    this._snackBar.open(message, action, { duration: duration ? duration : 3000 });
  }

}
