import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from "./components/header/header.component";
import { MaterialModule } from "./material.module";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule } from "@angular/router";
import { HttpClientModule } from "@angular/common/http";
import { NavMenuComponent } from "./components/nav-menu/nav-menu.component";
import { LogoComponent } from "./components/logo/logo.component";
import { UserComponent } from "./components/user/user.component";
import { ClickOutsideDirective } from "./directives/click-outside.directive";
import { ResponsiveSidenavDirective } from "./directives/responsive-sidenav.directive";
import { LocalDatePipe } from "./pipes/local-date.pipe";
import { CopyrightComponent } from "./components/copyright/copyright.component";
import { SubmitModalComponent } from "./components/submit-modal/submit-modal.component";
import {ModalChangeApproverComponent} from "./components/modal-change-approver/modal-change-approver.component";


@NgModule({
  declarations: [
    HeaderComponent,
    NavMenuComponent,
    LogoComponent,
    UserComponent,
    ClickOutsideDirective,
    ResponsiveSidenavDirective,
    CopyrightComponent,
    LocalDatePipe,
    SubmitModalComponent,
    ModalChangeApproverComponent

  ],
  imports: [
    CommonModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule,
    HttpClientModule,
  ],
  exports: [
    HeaderComponent,
    NavMenuComponent,
    LogoComponent,
    ClickOutsideDirective,
    ReactiveFormsModule,
    MaterialModule,
    FormsModule,
    RouterModule,
    ReactiveFormsModule,
    HttpClientModule,
    LogoComponent,
    ResponsiveSidenavDirective,
    CopyrightComponent,
    SubmitModalComponent,
    ModalChangeApproverComponent,
    LocalDatePipe
  ]
})
export class SharedModule {
}
