export const environment = {
  production: true,
  firebaseConfig: {
    apiKey: "AIzaSyAptZJS069zIKeMpGVNBwgViymH3L40suc",
    authDomain: "she-forms.firebaseapp.com",
    databaseURL: "https://she-forms.firebaseio.com",
    projectId: "she-forms",
    storageBucket: "she-forms.appspot.com",
    messagingSenderId: "312483852045",
    appId: "1:312483852045:web:d0851b069109f4fd34ca44",
    measurementId: "G-2R5WEW3M82"
  },
  additionalFields: {
    mainDomain: 'http://pacific-she-app.rentokil-initial.com'
  }
};
