// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyAptZJS069zIKeMpGVNBwgViymH3L40suc",
    authDomain: "she-forms.firebaseapp.com",
    projectId: "she-forms",
    storageBucket: "she-forms.appspot.com",
    messagingSenderId: "312483852045",
    appId: "1:312483852045:web:d0851b069109f4fd34ca44",
    measurementId: "G-2R5WEW3M82"
  },
  additionalFields: {
    mainDomain: 'http://pacific-she-app.rentokil-initial.com'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
